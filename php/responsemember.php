
<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

//include( "logchange.php" );

function postEdit ( $db, $action,$id,$values) {
  /*print "str 1";*/
  $originalDate= strval($values["Date"]);
  $newDate = date("Y-m-d H:i", strtotime($originalDate));
  /*print $newDate;
  print "str 2";*/
  /*print(strtotime($originalDate));*/
  if (isset($values["POOP1"])) {
    $POOP=$values["POOP1"];
  }elseif (isset($values["POOP2"])) {
    $POOP=$values["POOP2"];
  }elseif (isset($values["POOP3"])) {
    $POOP=$values["POOP3"];
  }
$values= array(
    'InviteID' => $POOP,
    'Date'   => $newDate,
    'Medium' => $values["Invitations2"],
    'Note' => $values["Invitations3"],
    'Response' => $values["Invitations4"],
  ) ;
$id =$db->insert( 'Responses', $values)->insertId();
$values=array('Responses'=> $values);

//logChange( $db, 'insert', $id,$values,NULL,'Responses');
};


// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

  //Editor instance and process the data coming from _POST
Editor::inst( $db, 'AllMembers' , 'Member')
	->field(
        Field::inst( 'AllMembers.Member' ),
        //Field::inst( 'ID' )
        Field::inst( 'MP.GMB' ),
          Field::inst( 'MP.WPO1' ),
          Field::inst( 'MP.WPO2' ),
          Field::inst( 'MP.HS1' ),
          Field::inst( 'MP.HS2' ),
          Field::inst( 'MP.ELC' ),
          Field::inst( 'MP.EQU' ),
          //Field::inst( 'LI.ID' ),
          Field::inst( 'R.ID' )
      )
      ->leftJoin( 'AttendanceConcat as AC' , 'AllMembers.Member' , '=' , 'AC.Member')
      ->leftJoin( 'PreferredVenues as PV' , 'AllMembers.Member' , '=' , 'PV.Member')
      ->leftJoin( 'member_pivot as MP' , 'AllMembers.Member' , '=' , 'MP.Member')
      ->leftJoin( 'LiveInvites as LI' , 'AllMembers.Member' , '=' , 'LI.Member') 
      ->leftJoin( 'ResponsesMember as R' , 'LI.ID' , '=' , 'R.InviteID') 
      ->on( 'postEdit', function ( $editor, $id, $values, $row ) {
          postEdit ( $editor->db(), 'edit' ,$id,$values) ;
        } )
	->process( $_POST )
	->json();

