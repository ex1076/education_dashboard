<?php
  session_start();
  require('config.php');

  $mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);
//  $myArray = array();

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  }

  $query = 'SELECT Abbreviation FROM Courses WHERE CoreCourse=1 ORDER By CoreCourseOrder;';

  if (!($res = $mysqli->query($query))) {
     echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
     exit;
  } else {
	/* store first result set */
	  while ($row = $res->fetch_array()) {
	    $myArray[] = $row;
	  }
  };

?>
