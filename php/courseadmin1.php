<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );
include( "courseadmin_update.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'Courses' , 'Abbreviation')
	->fields(
        Field::inst('Abbreviation'),
        Field::inst('Title'),
        Field::inst('Prerequisite')
        ->getFormatter( 'Format::nullEmpty',"asda")
        ->setFormatter( 'Format::nullEmpty'),
        Field::inst('Duration'),
        Field::inst('CoreCourse'),
        Field::inst('CoreCourseOrder')
	)
      ->where( 'CoreCourse', 1, "=")
      ->on( 'postCreate', function() {courseadmin_update();} )
      ->on( 'postEdit', function() {courseadmin_update();} )
      ->on( 'postRemove', function() {courseadmin_update();} )
	->process( $_POST )
	->json();
