<?php
require('config.php');


$mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

if ($mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

$temparray=[];
$grandarray = array();

if (!($res = $mysqli->query(
"create temporary table POPPY as SELECT distinct Convert(Activist using ASCII) POSTS FROM MembershipDetails;"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

if (!($res = $mysqli->query(
  "SELECT value  FROM 
  (SELECT distinct left(SUBSTRING_INDEX(SUBSTRING_INDEX(t.POSTS, '?', n.n), '?', -1),3) value FROM POPPY t CROSS JOIN (    SELECT a.N + b.N * 10 + 1 n      FROM     (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a    ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b     ORDER BY n ) n  WHERE n.n <= 1 + (LENGTH(t.POSTS) - LENGTH(REPLACE(t.POSTS, '?', ' ')))  ORDER BY value) port
where value regexp '^[A-Z][A-Z][A-Z]\(\r|$\)'
  ;"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }


while ($row = mysqli_fetch_assoc($res)){ 
  
//  var_dump($row);
  //$temparray[label] = $row["CourseAbb"] . " " . $row["VenueAbb"]  . " " . $row["StartDate"] ;
  $temparray['name'] = $row["value"] ;
  $temparray['value'] = $row["value"] ;
  $grandarray[] = $temparray ;
};
$superarray["success"]=true;
$superarray["results"]=$grandarray;

echo json_encode($superarray);
?>
