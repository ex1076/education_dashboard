<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DataTables PHP library
include( "DataTables.php" );

if (isset($_GET['archived']))    
{  
$archived=$_GET['archived'];
};
// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'TrainingProgrammeCourseVenue AS TP' , 'ID')
	->fields(
        Field::inst( 'TP.ID' ),
        Field::inst( 'TP.CourseAbb' ),
        Field::inst( 'TP.VenueAbb' ),
        Field::inst( 'Venues.Name'),
        Field::inst( 'Courses.Title'),
        Field::inst( 'TP.StartDate' ),
        //Field::inst( 'jfhGetEligibleCount(CourseAbb, VenueAbb) as Eligible' ),
        Field::inst( 'ET.EligibleCount' ),
	  Field::inst( 'TP.InvitesDone' ),
	  Field::inst( 'TP.MaterialsDone' ),
	  Field::inst( 'TP.ExpensesDone' ),
	  Field::inst( 'CIC.InviteCount' ),
	  Field::inst( 'CIC.ResponseCount' ),
	  Field::inst( 'CIC.ConfirmCount' ),
	  Field::inst( 'CAC.AttendanceCount' )
	  )
      ->leftJoin( 'EligibilityTable as ET ', 'ET.ID', '=', 'TP.ID')
      ->leftJoin( 'CourseInviteCount as CIC ', 'CIC.ID', '=', 'TP.ID')
      ->leftJoin( 'CourseAttendanceCount as CAC ', 'CAC.ID', '=', 'TP.ID')
      ->leftJoin( 'Venues' , 'TP.VenueAbb' , '=' , 'Venues.Abbreviation')
      ->leftJoin( 'Courses' , 'TP.CourseAbb' , '=' , 'Courses.Abbreviation')
      ->where( 'TP.Archived', $archived , '=')
	->process( $_POST )
	->json();
