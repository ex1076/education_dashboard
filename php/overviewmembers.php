
<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

$prereq=$_GET["prereq"];
$current=$_GET["current"];
$venue=$_GET["venue"];
$courseID=$_GET["courseID"];


// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
      //DataTables\Editor\Join,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'AllMembers' , 'Member')
	->field(
        Field::inst( 'AllMembers.Member as Member' ),
        Field::inst( 'AllMembers.FirstName' ),
        Field::inst( 'AllMembers.Surname' ),
	  Field::inst( 'AllMembers.Employer' ),
        Field::inst( 'AllMembers.EmployerName' ),
	  Field::inst( 'AllMembers.Workplace' ),
        Field::inst( 'AllMembers.WorkplaceName' ),
        Field::inst( 'AllMembers.OfficerName' ),
        Field::inst( 'AllMembers.BranchName' ),
	  Field::inst( 'AllMembers.BranchCode' ),
	  Field::inst( 'AllMembers.BSMailName' ),
        Field::inst( 'AllMembers.Add1' ),
	  Field::inst( 'AllMembers.Add2' ),
	  Field::inst( 'AllMembers.Add3' ),
	  Field::inst( 'AllMembers.Add4' ),
	  Field::inst( 'AllMembers.Postcode' ),
	  Field::inst( 'AllMembers.TelNoHome' ),
	  Field::inst( 'AllMembers.TelNoWork' ),
	  Field::inst( 'AllMembers.TelNoMobile' ),
	  Field::inst( 'AllMembers.EmailAddress1' ),
	  Field::inst( 'AllMembers.InvalidHomeAddress' ),
	  Field::inst( 'AllMembers.ApparentLeaverDate' ),
	  Field::inst( 'AllMembers.SuspendedMandate' ),
        //Field::inst( 'ID' )
        Field::inst( 'MP.GMB' ),
          Field::inst( 'MP.WPO1' ),
          Field::inst( 'MP.WPO2' ),
          Field::inst( 'MP.HS1' ),
          Field::inst( 'MP.HS2' ),
          Field::inst( 'MP.ELC' ),
          Field::inst( 'MP.EQU' )
          //Field::inst( 'LI.ID' ),
          //Field::inst( 'R.ID' )
      )
      ->leftJoin( 'AttendanceConcat as AC' , 'AllMembers.Member' , '=' , 'AC.Member')
      ->leftJoin( 'PreferredVenuesConCat as PV' , 'AllMembers.Member' , '=' , 'PV.Member')
      ->leftJoin( 'member_pivot3 as MP' , 'AllMembers.Member' , '=' , 'MP.Member')
      ->leftJoin( 'InvitationsConCat as LI' , 'AllMembers.Member' , '=' , 'LI.Member') 
      //->leftJoin( 'ResponsesMember as R' , 'LI.ID' , '=' , 'R.InviteID') 
          ->join(
              Mjoin::inst( 'Invitations')
                  ->link( 'Invitations.Member', 'AllMembers.Member' )
                  ->fields(
                      Field::inst( 'ID' ),
                      Field::inst( 'Date' )
                    )
                    ->where('TrainingID', $_GET['courseID'] ,'=')
            )
          ->join(
              Mjoin::inst( 'ResponsesMember')
                  ->link( 'ResponsesMember.Member', 'AllMembers.Member' )
                  ->fields(
                      Field::inst( 'ID' ),
                      Field::inst( 'Date' ),
                      Field::inst( 'Response' )
                    )
                    ->where('TrainingID', $_GET['courseID'] ,'=')
            )
            ->where( function ( $q ) {
              $q
                  ->where( 'PV.Venue' , '%|'.$_GET['venue'].'|%' ,'LIKE')
                  ->where( 'PV.Archived' , '0',"=")
                  ->where( function ( $r ) {
                      if ($_GET['prereq']) {
                        $r
                        ->where( 'AC.CourseList' , '%'.$_GET['prereq'].'%' ,'LIKE');
                      }
                      else {
                        $r
                        ->where( 'PV.Archived' , '0',"=");
                      }
                      ;
                    })
                  ->where( function ( $s ) {
                    $s
                        ->where( 'AC.CourseList' , '%'.$_GET['current'].'%' ,'NOT LIKE')
                        ->or_where( 'AC.CourseList' , null, '=');
                  })
                  ->or_where( 'LI.shit' , '%, ' .$_GET['courseID'] .'.%','LIKE');
            })
	->process( $_POST )
	->json();

