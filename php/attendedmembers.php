
<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

include("logchange.php");

$prereq=$_GET["prereq"];
$current=$_GET["current"];
$venue=$_GET["venue"];
$courseID=$_GET["courseID"];

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
      //DataTables\Editor\Join,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'Attendance' , 'Member')
	->field(
        Field::inst( 'Attendance.Member as Member' ),
        Field::inst( 'MD.FirstName' ),
        Field::inst( 'MD.Surname' ),
        Field::inst( 'MD.EmployerName' ),
        Field::inst( 'MD.WorkplaceName' ),
        Field::inst( 'MD.OfficerName' ),
        Field::inst( 'MD.BranchName' ),
        Field::inst( 'MP.GMB' ),
          Field::inst( 'MP.WPO1' ),
          Field::inst( 'MP.WPO2' ),
          Field::inst( 'MP.HS1' ),
          Field::inst( 'MP.HS2' ),
          Field::inst( 'MP.ELC' ),
          Field::inst( 'MP.EQU' )
      )
      ->leftJoin( 'AllMembers as MD' , 'Attendance.Member' , '=', 'MD.Member')
      ->leftJoin( 'member_pivot3 as MP' , 'MD.Member' , '=' , 'MP.Member')
      ->where( "Attendance.TrainingID" , $courseID, "=")
      ->on( 'postCreate', function ( $editor, $id, $values, $row ) {
            logChange( $editor->db(), 'create', $id, $values, $row, 'Attendance' );
        } )
      ->on( 'postEdit', function ( $editor, $id, $values, $row ) {
            logChange( $editor->db(), 'edit', $id, $values, $row, 'Attendance' );
        } )
      ->on( 'postRemove', function ( $editor, $id, $values, $row ) {
            logChange( $editor->db(), 'remove', $id, $values, NULL, 'Attendance' );
        } )
	->process( $_POST )
	->json();

