<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//
//
// DataTables PHP library
include( "DataTables.php" );

$courseID = $_POST['courseID'];

function postEdit ( $db, $action,$id,$values) {
$values= array(
    'TrainingID' => $_POST["courseID"],
    'Member' => $values['Member'],
    'Notes' => $values['Notes']
  ) ;
$id =$db->insert( 'Attendance', $values)->insertId();
$values=array('Invitations'=> $values);
};

// Alias Editor classes so they are easy to use
//
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

Editor::inst( $db, 'AllMembers' , 'Member')
	->field(
        Field::inst( 'AllMembers.Member' ),
        Field::inst( 'AllMembers.FirstName' ),
        Field::inst( 'AllMembers.Surname' ),
        Field::inst( 'MP.GMB' ),
        Field::inst( 'MP.WPO1' ),
        Field::inst( 'MP.WPO2' ),
        Field::inst( 'MP.HS1' ),
        Field::inst( 'MP.HS2' ),
        Field::inst( 'MP.ELC' ),
        Field::inst( 'MP.EQU' )
      )
      ->leftJoin( 'member_pivot2 as MP' , 'AllMembers.Member' , '=' , 'MP.Member')
      ->on( 'postEdit', function ( $editor, $id, $values, $row ) {
          postEdit ( $editor->db(), 'edit' ,$id,$values) ;
        } )
      ->process( $_POST )
      ->json();
?>
