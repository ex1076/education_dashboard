
<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

$prereq=$_GET["prereq"];
$current=$_GET["current"];
$venue=$_GET["venue"];
$courseID=$_GET["courseID"];


// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
      //DataTables\Editor\Join,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
//Editor::inst( $db, 'MD' , 'Member')
$editor = Editor::inst( $db, 'Invitations' , 'Member')
	->field(
        Field::inst( 'Invitations.Member as Member' ),
        Field::inst( 'Invitations.ID AS POOP1' ),
        Field::inst( 'Invitations.Date' ),
        Field::inst( 'MD.FirstName' ),
        Field::inst( 'MD.Surname' ),
	  Field::inst( 'MD.Employer' ),
        Field::inst( 'MD.EmployerName' ),
	  Field::inst( 'MD.Workplace' ),
        Field::inst( 'MD.WorkplaceName' ),
        Field::inst( 'MD.OfficerName' ),
        Field::inst( 'MD.BranchName' ),
	  Field::inst( 'MD.BranchCode' ),
	  Field::inst( 'MD.BSMailName' ),
        Field::inst( 'MD.Add1' ),
	  Field::inst( 'MD.Add2' ),
	  Field::inst( 'MD.Add3' ),
	  Field::inst( 'MD.Add4' ),
	  Field::inst( 'MD.Postcode' ),
	  Field::inst( 'MD.TelNoHome' ),
	  Field::inst( 'MD.TelNoWork' ),
	  Field::inst( 'MD.TelNoMobile' ),
	  Field::inst( 'MD.EmailAddress1' ),
	  Field::inst( 'MD.InvalidHomeAddress' ),
	  Field::inst( 'MD.ApparentLeaverDate' ),
	  Field::inst( 'MD.SuspendedMandate' )
        //Field::inst( 'ID' )
        //Field::inst( 'MP.GMB' ),
          //Field::inst( 'MP.WPO1' ),
          //Field::inst( 'MP.WPO2' ),
          //Field::inst( 'MP.HS1' ),
          //Field::inst( 'MP.HS2' ),
          //Field::inst( 'MP.ELC' ),
          //Field::inst( 'MP.EQU' )
          //Field::inst( 'LI.ID' ),
          //Field::inst( 'R.ID' )
      )
      ->leftJoin( 'member_pivot3 as MP' , 'Invitations.Member' , '=' , 'MP.Member')
      ->leftJoin( 'AllMembers as MD' , 'Invitations.Member' , '=' , 'MD.Member')
      ->where( 'Invitations.TrainingID' , $_GET['courseID'] ,'=');
      //->leftJoin( 'ResponsesMember as R' , 'LI.ID' , '=' , 'R.InviteID') 
      //

$editor->field( new Field('MP.HS1'));
$editor->field( new Field('MP.HS2'));
$editor->field( new Field('MP.GMB'));
$editor->field( new Field('MP.WPO1'));
$editor->field( new Field('MP.WPO2'));
$editor->field( new Field('MP.EQU'));
$editor->field( new Field('MP.ELC'));

$editor
  ->process( $_POST )
  ->json();

?>
