
<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

//$archived=$_GET["archived"];
$prereq=$_GET["prereq"];
$current=$_GET["current"];
$venue=$_GET["venue"];
$courseID=$_GET["courseID"];


//ONLY MATCH MEMBERS FOR LIVE COURSES
//
//if($archived == 0){
  // Alias Editor classes so they are easy to use
  use
        DataTables\Editor,
        DataTables\Editor\Field,
        DataTables\Editor\Format,
        //DataTables\Editor\Join,
        DataTables\Editor\Join,
        DataTables\Editor\Mjoin,
        DataTables\Editor\Upload,
        DataTables\Editor\Validate;

  // Build our Editor instance and process the data coming from _POST
  Editor::inst( $db, 'AllMembers' , 'Member')
        ->field(
          Field::inst( 'MD.Member as Member' ),
          Field::inst( 'MD.FirstName' ),
          Field::inst( 'MD.Surname' ),
          Field::inst( 'MD.Employer' ),
          Field::inst( 'MD.EmployerName' ),
          Field::inst( 'MD.Workplace' ),
          Field::inst( 'MD.WorkplaceName' ),
          Field::inst( 'MD.OfficerName' ),
          Field::inst( 'MD.BranchName' ),
          Field::inst( 'MD.BranchCode' ),
          Field::inst( 'MD.BSMailName' ),
          Field::inst( 'MD.Add1' ),
          Field::inst( 'MD.Add2' ),
          Field::inst( 'MD.Add3' ),
          Field::inst( 'MD.Add4' ),
          Field::inst( 'MD.Postcode' ),
          Field::inst( 'MD.TelNoHome' ),
          Field::inst( 'MD.TelNoWork' ),
          Field::inst( 'MD.TelNoMobile' ),
          Field::inst( 'MD.EmailAddress1' ),
          Field::inst( 'MD.InvalidHomeAddress' ),
          Field::inst( 'MD.ApparentLeaverDate' ),
          Field::inst( 'MD.SuspendedMandate' ),
          //Field::inst( 'ID' )
          Field::inst( 'MP.GMB' ),
            Field::inst( 'MP.WPO1' ),
            Field::inst( 'MP.WPO2' ),
            Field::inst( 'MP.HS1' ),
            Field::inst( 'MP.HS2' ),
            Field::inst( 'MP.ELC' ),
            Field::inst( 'MP.EQU' )
            //Field::inst( 'ICC.ID' ),
            //Field::inst( 'R.ID' )
        )
        ->leftJoin( 'AllMembers as MD' , 'AllMembers.Member' , '=' , 'MD.Member')
        ->leftJoin( 'AttendanceConcat as AC' , 'AllMembers.Member' , '=' , 'AC.Member')
        ->leftJoin( 'PreferredVenuesConCat as PV' , 'AllMembers.Member' , '=' , 'PV.Member')
        ->leftJoin( 'member_pivot3 as MP' , 'AllMembers.Member' , '=' , 'MP.Member')
        ->leftJoin( 'InvitationsConCat as ICC' , 'AllMembers.Member' , '=' , 'ICC.Member') 
        ->leftJoin( 'LiveInvites as LI' , 'AllMembers.Member' , '=' , 'LI.Member') 
        //->leftJoin( 'ResponsesMember as R' , 'ICC.ID' , '=' , 'R.InviteID') 
        ->join(
            Mjoin::inst( 'Invitations')
              ->link( 'Invitations.Member', 'AllMembers.Member' )
              ->fields(
                Field::inst( 'ID' ),
                Field::inst( 'Date' )
              )
              ->where('TrainingID', $_GET['courseID'] ,'=')
          )
        ->join(
            Mjoin::inst( 'ResponsesMember')
              ->link( 'ResponsesMember.Member', 'AllMembers.Member' )
              ->fields(
                Field::inst( 'ID' ),
                Field::inst( 'Date' ),
                Field::inst( 'Response' )
              )
              ->where('TrainingID', $_GET['courseID'] ,'=')
          )
        ->where( function ( $q ) {
          $q
            //ONLY MATCH MEMBERS WITH saME PREFFERRED VENUE
            ->where( 'PV.Venue' , '%|'.$_GET['venue'].'|%' ,'LIKE')
            //ONLY MATCH MEMBERS WHO HAVE DONE THE PREREQUISITE COURSES
            ->where( function ( $r ) {
                if (isset($_GET['prereq'])) {
                  $r
                  ->where( 'AC.CourseList' , '%'.$_GET['prereq'].'%' ,'LIKE');
                }
                //else {
                //$r
                //->where( 'PV.Archived' , '0',"=");
                //}
                ;
              })
            //NOT GOT A LIVE INVITE
            ->where( 'LI.ID' , null, '=')
            //NOT ALREADY INVITED TO THIS COURSE
            ->where( 'ICC.shit' , '%, ' .$_GET['courseID'] .'.%','NOT LIKE')
            //ONLY MATCH MEMBERS WHO HAVE NOT DONE THIS COURSES
            ->where( function ( $s ) {
              $s
                ->where( 'AC.CourseList' , '%'.$_GET['current'].'%' ,'NOT LIKE')
                ->or_where( 'AC.CourseList' , null, '=');
            });
            //->or_where( 'ICC.shit' , '%, ' .$_GET['courseID'] .'.%','LIKE');
        })
        ->process( $_POST )
        ->json();
//}
?>

