<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

require('config.php');

$mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

if ($mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

if (!($res = $mysqli->query(
  " SELECT 
	AM.Member,
	AM.Title,
	AM.FirstName,
	AM.Surname,
	AM.OfficerName,
	AM.WorkplaceName,
	AM.Workplace,
	AM.EmployerName,
	AM.Employer,
	AM.BranchName,
	AM.BranchCode,
	AM.BSMailName,
	replace(Convert(AM.Activist using ASCII),'?',' '),
	AM.Add1,
	AM.Add2,
	AM.Add3,
	AM.Add4,
	AM.Postcode,
	AM.TelNoHome,
	AM.TelNoWork,
	AM.TelNoMobile,
	AM.EmailAddress1,
	AM.InvalidHomeAddress,
	AM.ApparentLeaverDate,
      AM.SuspendedMandate,
      IF(OOR.Member is null,0,1) OOR
FROM AllMembers AM 
left join OutOfRegionMembershipDetails OOR on AM.Member=OOR.Member 
WHERE AM.Member='" .  $_GET['member'] . 
"' AND (OOR.Archived is null or OOR.Archived =0)"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
while ($row = mysqli_fetch_assoc($res)){ foreach ($row as $current){$orla[] = $current;}};

$data = [
"Member" => $orla[0],
"Title" => $orla[1],
"FirstName" => $orla[2],
"Surname" => $orla[3],
"OfficerName" => $orla[4],
"WorkplaceName" => $orla[5],
"Workplace" => $orla[6],
"EmployerName" => $orla[7],
"Employer" => $orla[8],
"BranchName" => $orla[9],
"BranchCode" => $orla[10],
"BSMailName" => $orla[11],
"Activist" => $orla[12],
"Add1" => $orla[13],
"Add2" => $orla[14],
"Add3" => $orla[15],
"Add4" => $orla[16],
"Postcode" => $orla[17],
"TelNoHome" => $orla[18],
"TelNoWork" => $orla[19],
"TelNoMobile" => $orla[20],
"EmailAddress1" => $orla[21],
"OOR" => $orla[25]
];



if (!($res = $mysqli->query(
  "SELECT GROUP_CONCAT(DISTINCT CourseAbb ORDER BY CoreCourseOrder)  
  FROM AttMemberCourse  
  LEFT JOIN Courses on AttMemberCourse.CourseAbb=Courses.Abbreviation  
  WHERE Member='" .  $rMember  . "'"
  //WHERE Member='" .  $_GET['member'] . "'"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
while ($row = mysqli_fetch_assoc($res)){ foreach ($row as $current){$orla[] = $current;}};
$row = mysqli_fetch_assoc($res);
//print_r ($row);

//$rAttendances=$orla[21];
$rAttendances=$orla[26];
//echo $rAttendances;
?>
