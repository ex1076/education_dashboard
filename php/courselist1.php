<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DataTables PHP library
include( "DataTables.php" );

include("logchange.php");
// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'TrainingProgramme' , 'ID')
	->fields(
        //Field::inst( 'TP.ID' ),
        Field::inst( 'Series' ),
        Field::inst( 'CourseAbb' ),
        Field::inst( 'VenueAbb' ),
        Field::inst( 'StartDate' ),
        Field::inst( 'InvitesDone' ),
        Field::inst( 'MaterialsDone' ),
        Field::inst( 'ExpensesDone' ),
        Field::inst( 'Archived' )
        //->on( 'postCreate',  ajax.reload())
        //->on( 'postCreate', function ( $editor, $id, $values, $row ) {
        //logChange( $editor->db(), 'create', $id, $values, $row, 'TrainingProgramme' );
//} )
        //->on( 'postEdit', function ( $editor, $id, $values, $row ) {
        //logChange( $editor->db(), 'edit', $id, $values, $row, 'TrainingProgramme' );
        //} )
        //->on( 'postRemove', function ( $editor, $id, $values, $row ) {
        //logChange( $editor->db(), 'remove', $id, $values, NULL, 'TrainingProgramme' );
        //} )
        //)
        //Field::inst( 'jfhGetEligibleCount(CourseAbb, VenueAbb) as Eligible' ),
        //Field::inst( 'ET.EligibleCount' ),
        //Field::inst( 'TP.InvitesDone' ),
        //Field::inst( 'TP.MaterialsDone' ),
        //Field::inst( 'TP.ExpensesDone' ),
	  )
	->process( $_POST )
	->json();
