<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

// Alias Editor classes so they are easy to use
use
  DataTables\Editor,
  DataTables\Editor\Field,
  DataTables\Editor\Format,
  DataTables\Editor\Join,
  DataTables\Editor\Upload,
  DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
$editor = Editor::inst( $db, 'AllMembers as MD' , 'Member')
  ->fields(
    Field::inst( 'MD.FirstName' )->validator( 'Validate::notEmpty' ),
    Field::inst( 'MD.Surname' )->validator( 'Validate::notEmpty' ),
    Field::inst( 'MD.Member' ),
    Field::inst( 'MD.OfficerName' ),
    Field::inst( 'MD.WorkplaceName' ),
    Field::inst( 'MD.Workplace' ),
    Field::inst( 'MD.EmployerName' ),
    Field::inst( 'MD.Employer' ),
    Field::inst( 'MD.BranchName' ),
    Field::inst( 'MD.BranchCode' ),
    Field::inst( 'MD.BSMailName' ),
    Field::inst( 'CONVERT(MD.Activist using ASCII) as MD.Activist' ),
    Field::inst( 'MD.Add1' ),
    Field::inst( 'MD.Add2' ),
    Field::inst( 'MD.Add3' ),
    Field::inst( 'MD.Add4' ),
    Field::inst( 'MD.Postcode' ),
    Field::inst( 'MD.TelNoHome' ),
    Field::inst( 'MD.TelNoWork' ),
    Field::inst( 'MD.TelNoMobile' ),
    Field::inst( 'MD.EmailAddress1' ),
    Field::inst( 'MD.InvalidHomeAddress' ),
    Field::inst( 'MD.ApparentLeaverDate' ),
    Field::inst( 'MD.SuspendedMandate' ),
    Field::inst( 'MD.Archived' )
  )
  ->leftJoin( 'member_pivot3 as ATT', 'ATT.member', '=', 'MD.Member')
  ->where( 'MD.Archived', '0', '=');

$editor->field( new Field('ATT.HS1'));
$editor->field( new Field('ATT.HS2'));
$editor->field( new Field('ATT.GMB'));
$editor->field( new Field('ATT.WPO1'));
$editor->field( new Field('ATT.WPO2'));
$editor->field( new Field('ATT.EQU'));
$editor->field( new Field('ATT.ELC'));

$editor
  ->process( $_POST )
  ->json();

?>
