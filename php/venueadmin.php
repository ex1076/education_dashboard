<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'Venues' , 'Abbreviation')
	->fields(
        Field::inst('Abbreviation'),
//      Field::inst('Abbreviation')
//            ->validator( 'Validate::numeric' )
//            ->setFormatter( 'Format::ifEmpty', null ),
//JFH Must format non-null values to be in specified formats
//JFH Must also let user know if they are attempting to duplicate
        Field::inst('Name'),
        Field::inst('Add1'),
        Field::inst('Add2'),
        Field::inst('Add3'),
        Field::inst('Add4'),
        Field::inst('Add5'),
        Field::inst('CoreVenue')
	)
	->process( $_POST )
	->json();
