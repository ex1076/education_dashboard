<?php
session_start();

function courseadmin_update () {
require('config.php');

$mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

if ($mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

$query = '';
$query .= 'SET @rownumber = 0;';
$query .= 'update Courses set CoreCourseOrder = (@rownumber:=@rownumber+1) order by CoreCourseOrder asc;';

if (!($res = $mysqli->multi_query($query))) {
   echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
   exit;
}

};

?>
