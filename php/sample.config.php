<?php
$db_username 	= '';
$db_password 	= '';
$db_name 		= '';
$db_host 		= '';
?>

<?php 

if (defined('DATATABLES')) {  // Ensure being used in DataTables env.

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL);
ini_set('display_errors', '1');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Database user / pass
 */
$sql_details = array(
      "type" => "Mysql",  // Database type: "Mysql", "Postgres", "Sqlite" or "Sqlserver"
      "user" => $db_username,       // Database user name
      "pass" => $db_password,       // Database password
      "host" => $db_host,           // Database host
      "port" => "",                 // Database connection port (can be left empty for default)
      "db"   => $db_name,           // Database name
      "dsn"  => ""                  // PHP DSN extra information. Set as `charset=utf8` if you are using MySQL
);
}
