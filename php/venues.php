<?php
require('config.php');


$mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

if ($mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

$temparray=[];

if (!($res = $mysqli->query(
  " SELECT * FROM Venues order by CoreVenue DESC"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

while ($row = mysqli_fetch_assoc($res)){ $temparray[] = array($row["Abbreviation"],$row["Name"]);};

echo json_encode($temparray);
?>
