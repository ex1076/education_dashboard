<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DataTables PHP library
include( "DataTables.php" );

if (isset($_GET['archived']))    
{  
$archived=$_GET['archived'];
};
// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'Users' , 'ID')
	->fields(
        Field::inst( 'ID' ),
        Field::inst( 'User' ),
        Field::inst( 'Level' ),
        Field::inst( 'LastSeen' )
	  )
	->process( $_POST )
	->json();
