<?php
require('config.php');


$mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

if ($mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

$temparray=[];

if (!($res = $mysqli->query(
  " SELECT * FROM Courses order by -CoreCourseOrder DESC"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

$temparray[] = array("","");
while ($row = mysqli_fetch_assoc($res)){ $temparray[] = array($row["Abbreviation"],$row["Title"]);};

echo json_encode($temparray);
?>
