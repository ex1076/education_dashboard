<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//
//
// DataTables PHP library
include( "DataTables.php" );

include( "logchange.php" );

function postRemove ( $db, $id,$values) {
  $invID=$values["Invitations"]["ID"];
  $resID=$values["Responses"]["ID"];
  $attID=$values["Attendance"]["ID"];
  
  if (!empty($invID)) {
    echo "one<br>";
    $values=$db->sql( 'SELECT * FROM Invitations WHERE ID="' . $invID . '"')->fetch();
    $db->sql( 'DELETE FROM Invitations WHERE ID="' . $invID . '"');
    $values=array('Invitations'=> $values);
    logChange( $db, 'remove', $invID,$values,NULL,'Invitations');
  };

  if (!empty($resID)) {
    echo "two<br>";
    $values=$db->sql( 'SELECT * FROM Responses WHERE ID="' . $resID . '"')->fetch();
    $db->sql( 'DELETE FROM Responses WHERE ID="' . $resID . '"');
    $values=array('Responses'=> $values);
    logChange( $db, 'remove', $resID,$values,NULL,'Responses');
  };

  if (!empty($attID)) {
    echo "three<br>";
    $values=$db->sql( 'SELECT * FROM Attendance WHERE ID="' . $attID . '"')->fetch();
    $db->sql( 'DELETE FROM Attendance WHERE ID="' . $attID . '"');
    $values=array('Attendance'=> $values);
    logChange( $db, 'remove', $attID,$values,NULL,'Attendance');
  };

};

function logChange2 ( $db, $id,$values) {
  var_dump($values);
  echo"HIT";
  $invID=$values["INV"]["ID"];
  $resID=$values["R"]["ID"];
  $attID=$values["ATT"]["ID"];
  //$db->sql( 'INSERT INTO (Training.ID Invitations WHERE ID="' . $invID . '"');
  //$db->sql( 'DELETE FROM Responses WHERE ID="' . $resID . '"');
  //$db->sql( 'DELETE FROM Attendance WHERE ID="' . $attID . '"');
};


// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

$member=$_GET['member'];
echo $member;

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'Invitations as INV' , 'INV.Member')
	->fields(
        Field::inst( 'INV.Member' ),
        Field::inst( 'TP.ID' )
	  )
      ->leftJoin( 'TrainingProgramme as TP' , 'TP.ID' , '=' , 'INV.TrainingID')
      ->on( 'postEdit', function ( $editor, $id, $values ) {
        postRemove ( $editor->db(), $id,$values) ;
      } )
      ->on( 'postRemove', function ( $editor, $id, $values ) {
        postRemove ( $editor->db(), $id,$values) ;
      } )
      ->on( 'postAdd', function ( $editor, $id, $values ) {
        logChange2 ( $editor->db(), $id,$values) ;
      } )
	->process( $_POST )
	->json();
?>
