<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//
//
// DataTables PHP library
include( "DataTables.php" );

//include("logchange.php");

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

Editor::inst ($db, 'Invitations')
  ->field(
    Field::inst('Invitations.ID'),
    Field::inst('Invitations.Member'),
    Field::inst('Invitations.TrainingID'),
    Field::inst('Invitations.Medium'),
    Field::inst('TrainingProgramme.ID'),
    Field::inst('AllMembers.EmployerName'),
    Field::inst('AllMembers.WorkplaceName'),
    Field::inst('AllMembers.OfficerName'),
    Field::inst('AllMembers.BranchName')
)
  ->leftJoin( 'TrainingProgramme' , 'TrainingProgramme.ID' , '=' , 'Invitations.TrainingID')
  ->leftJoin( 'AllMembers' , 'AllMembers.Member' , '=' , 'Invitations.Member')
  //->on( 'posreplyytCreate', function ( $editor, $id, $values, $row ) {
  //      logChange( $editor->db(), 'create', $id, $values,$row,'Invitations' );
  //  } )
  //->on( 'postEdit', function ( $editor, $id, $values, $row ) {
  //      logChange( $editor->db(), 'edit', $id, $values,$row,'Invitations' );
  //  } )
  //->on( 'postRemove', function ( $editor, $id, $values, $row ) {
  //      logChange( $editor->db(), 'remove', $id, $values, NULL,'Invitations' );
  //  } )
  ->process( $_POST )
  ->json();
?>
