
<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "DataTables.php" );

//include( "logchange.php" );

$courseID = $_POST['courseID'];

function postEdit ( $db, $action,$id,$values) {
$values= array(
    'TrainingID' => $_POST["courseID"],
    'Member' => $id,
    'Medium' => $values['Medium']
    //'Medium' => var_dump($values),
  ) ;
$id =$db->insert( 'Invitations', $values)->insertId();
$values=array('Invitations'=> $values);
};

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

//// Build our Editor instance and process the data coming from _POST
//Editor::inst( $db, 'AllMembers as MD' , 'MD.Member')
	//->fields(
        //Field::inst( 'MD.Member' ),
        //Field::inst( 'MP.GMB' ),
        //Field::inst( 'MP.WPO1' ),
        //Field::inst( 'MP.WPO2' ),
        //Field::inst( 'MP.HS1' ),
        //Field::inst( 'MP.HS2' ),
        //Field::inst( 'MP.ELC' ),
        //Field::inst( 'MP.EQU' ),
        //Field::inst( 'LI.ID' ),
        //Field::inst( 'R.ID' )
	//)
      //->leftJoin( 'AttendanceConcat as AC' , 'MD.Member' , '=' , 'AC.Member')
      //->leftJoin( 'PreferredVenues as PV' , 'MD.Member' , '=' , 'PV.Member')
      //->leftJoin( 'member_pivot as MP' , 'MD.Member' , '=' , 'MP.Member')
      //->leftJoin( 'LiveInvites as LI' , 'MD.Member' , '=' , 'LI.Member') 
      //->leftJoin( 'Responses as R' , 'LI.ID' , '=' , 'R.InviteID') 
      //->on( 'postEdit', function ( $editor, $id, $values, $row ) {
        //logChange ( $editor->db(), 'edit' ,$id,$values) ;
        //} )
	//->process( $_POST )
	//->json();

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'AllMembers' , 'Member')
	->field(
        Field::inst( 'AllMembers.Member' ),
        Field::inst( 'AllMembers.FirstName' ),
        Field::inst( 'AllMembers.Surname' ),
        //Field::inst( 'ID' )
        Field::inst( 'MP.GMB' ),
          Field::inst( 'MP.WPO1' ),
          Field::inst( 'MP.WPO2' ),
          Field::inst( 'MP.HS1' ),
          Field::inst( 'MP.HS2' ),
          Field::inst( 'MP.ELC' ),
          Field::inst( 'MP.EQU' )
          //Field::inst( 'LI.ID' ),
          //Field::inst( 'R.ID' )
      )
      ->leftJoin( 'AttendanceConcat as AC' , 'AllMembers.Member' , '=' , 'AC.Member')
      ->leftJoin( 'PreferredVenues as PV' , 'AllMembers.Member' , '=' , 'PV.Member')
      ->leftJoin( 'member_pivot2 as MP' , 'AllMembers.Member' , '=' , 'MP.Member')
      //->leftJoin( 'Invitations as LI' , 'AllMembers.Member' , '=' , 'LI.Member') 
      //->leftJoin( 'ResponsesMember as R' , 'LI.ID' , '=' , 'R.InviteID') 
          ->join(
              Mjoin::inst( 'Invitations')
                  ->link( 'Invitations.Member', 'AllMembers.Member' )
                  ->fields(
                      Field::inst( 'ID' )
                        ->validator( 'Validate::email', array( 'required' => true))
                    )
                    ->where('TrainingID', $courseID,'=')
            )
          ->join(
              Mjoin::inst( 'ResponsesMember')
                  ->link( 'ResponsesMember.Member', 'AllMembers.Member' )
                  ->fields(
                      Field::inst( 'ID' )
                    )
                    ->where('TrainingID', $courseID,'=')
            )
      ->on( 'postEdit', function ( $editor, $id, $values, $row ) {
          postEdit ( $editor->db(), 'edit' ,$id,$values) ;
        } )
	->process( $_POST )
	->json();

