<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//
//
// DataTables PHP library
include( "DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

$member=$_GET['member'];

Editor::inst ($db, 'AttendanceUInvitations', 'Member')
      ->field(
        Field::inst('AttendanceUInvitations.Member'),
        Field::inst('AttendanceUInvitations.TrainingID'),
        //->options( 'TrainingProgramme', 'ID', 'CourseAbb' ),
        Field::inst('Venues.Name'),
        Field::inst('Courses.Title'),
        //->options( 'TrainingProgramme', 'ID', 'VenueAbb' ),
        Field::inst('TrainingProgramme.CourseAbb'),
        Field::inst('TrainingProgramme.VenueAbb'),
        Field::inst('TrainingProgramme.ID')
        ->options( 'TrainingProgramme', 'ID', array('CourseAbb','VenueAbb','StartDate') ),
        Field::inst('TrainingProgramme.StartDate'),
        Field::inst('Invitations.ID'),
        Field::inst('Invitations.Member'),
        Field::inst('Invitations.TrainingID'),
        Field::inst('Invitations.Date'),
        Field::inst('Invitations.Medium'),
        Field::inst('Invitations.Notes'),
        Field::inst('Responses.ID'),
        Field::inst('Responses.ID_hist'),
        Field::inst('Responses.Date'),
        Field::inst('Responses.Date_hist'),
        Field::inst('Responses.Medium'),
        Field::inst('Responses.Medium_hist'),
        Field::inst('Responses.Note'),
        Field::inst('Responses.Note_hist'),
        Field::inst('Responses.Response'),
        Field::inst('Responses.Resp_hist'),
        Field::inst('Attendance.ID'),
        Field::inst('Attendance.TrainingID')
      )
      ///This join is a bit ofa hack (see https://www.datatables.net/forums/discussion/30273/leftouterjoin-complex)
      ->leftJoin( 'Attendance' , 'Attendance.TrainingID' , '=' , 'AttendanceUInvitations.TrainingID AND Attendance.Member = AttendanceUInvitations.Member')
      ->leftJoin( 'Invitations' , 'Invitations.TrainingID' , '=' , 'AttendanceUInvitations.TrainingID AND Invitations.Member = AttendanceUInvitations.Member')
      ->leftJoin( 'ResponsesMember as Responses' , 'Responses.InviteID' , '=' , 'Invitations.ID')
      ->leftJoin( 'TrainingProgramme' , 'TrainingProgramme.ID' , '=' , 'AttendanceUInvitations.TrainingID')
      ->leftJoin( 'Venues' , 'TrainingProgramme.VenueAbb' , '=' , 'Venues.Abbreviation')
      ->leftJoin( 'Courses' , 'TrainingProgramme.CourseAbb' , '=' , 'Courses.Abbreviation')
      ->where( 'AttendanceUInvitations.Member', $member,'=')
          //->join(
              //Mjoin::inst( 'Attendance')
                  //->link( 'Attendance.Member', 'AttendanceUInvitations.Member' )
                  //->fields(
                    //Field::inst( 'TrainingID' )
                  //)
                    //->where('Attendance.Member', '064465A','=')
                    //->where('AttendanceUInvitations.Member', '064465A','=')
            //)
            //->where( 'AttendanceUInvitations.Member', $member,'=')
      //->where( function ( $q ) {
      //$q
      //->where( 'AttendanceUInvitations.Member', '064465A','=')
                  //->where( function ( $r ) {
                    //$r
                        //->where( 'Attendance.TrainingID', null,'!=')
                        //->or_where( 'Attendance.Member', null,'!=')
                        //;
                  //})
                //->where( 'PV.Venue' , $_GET['venue'],"=")
                //->where( 'AC.CourseList' , '%'.$_GET['prereq'].'%' ,'LIKE')
                //->where( 'AC.CourseList' , '%'.$_GET['current'].'%' ,'NOT LIKE')
                //->where( 'LI.TrainingID' , $_GET['courseID'] ,'=')
                  //;
                  //})
      //->where( 'Attendance.Member', $member)
      //->where( 'Attendance.Member', $member)
      ->process( $_POST )
      ->json();
//// Table's primary key
//$primaryKey = 'ID';

//// Array of database columns which should be read and sent back to DataTables.
//// The `db` parameter represents the column name in the database, while the `dt`
//// parameter represents the DataTables column identifier. In this case simple
//// indexes
//$columns = array(
	//array( 'db' => '`Invitations`.`ID`'             ,'dt' => 0 , 'field' => 'ID'),
	//array( 'db' => '`Invitations`.`TrainingID`'     ,'dt' => 1 , 'field' => 'TrainingID'),
	//array( 'db' => '`Invitations`.`Member`'         ,'dt' => 2 , 'field' => 'Member'),
	//array( 'db' => '`Invitations`.`Date`'           ,'dt' => 3 , 'field' => 'Date'),
	//array( 'db' => '`Invitations`.`Medium`'         ,'dt' => 4 , 'field' => 'Medium')
//);


//// SQL server connection information
//require('config.php');
//$sql_details = array(
	//'user' => $db_username,
	//'pass' => $db_password,
	//'db'   => $db_name,
	//'host' => $db_host
//);

///* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 //* If you just want to use the basic configuration for DataTables with PHP
 //* server-side, there is no need to edit below this line.
 //*/
////$member=$_GET['member'];

//////require( 'ssp.class.php' );
//include_once('ssp.customized.class.php');
////$member='230694A';

//$joinQuery = "FROM `Invitations` AS `Invitations` 
  //WHERE `Invitations`.`Member`='" . $member . "'";

////echo "<br>";
////echo "<br>";
////echo "<br>";
////echo "<br>";
////echo "<br>";
////echo "<br>";
////echo "<br>";
////var_dump($joinQuery);
////echo "<br>";
////echo "<br>";

//echo  json_encode(
  //SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere )
//);
?>
