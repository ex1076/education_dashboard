<?php
require('config.php');


$mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

if ($mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

$temparray=[];

if (!($res = $mysqli->query(
  " SELECT * FROM Media order by ID ASC"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

$temparray[] = array(
  "value" => "",
  "label" => ""
);

while ($row = mysqli_fetch_assoc($res)){ 
  $temparray[] = array(
    "value" => $row["ID"],
    "label" => $row["Medium"]
  );
};

echo json_encode($temparray);
?>
