<?php 
  require('php/config.php');
  session_start();     
  $myusername= $_SESSION['user'];


  $mysqli = @new mysqli($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "<br>";
    exit;
  }

  if (!($res = $mysqli->query(
    "INSERT INTO Audit_Logins (Username, Hostname, Status) Values('" . $myusername . "','" . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "','logged out')"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
    exit;
  };
  session_unset();     // unset $_SESSION variable for the run-time
  session_destroy();   // destroy session data in storage
?>

<script type="text/javascript" language="javascript" class="init">
  $(document).ready(function() {
    window.setTimeout(function(){
      // Move to a new location or you can do something else
      window.location.href = "/education/";
    }, 3000);
  });
</script>


<body>
  <div class="ui main text center aligned container">
    <h1 class="ui header">Logout</h1>
    <div class="ui container">
      <div class="ui basic segment">
        Succesfully logged out
      </div>
    </div>
  </div>
</body>


