<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Editor example - Basic initialisation</title>
    <style type="text/css" class="init">
      div.DTE_Body div.DTE_Body_Content div.DTE_Field {
          float: left;
          width: 50%;
          padding: 5px 20px;
          clear: none;
          box-sizing: border-box;
      }

      div.DTE_Body div.DTE_Form_Content:after {
          content: ' ';
          display: block;
          clear: both;
      }

      select {
          width: 100% !important;
      }

      div.DTE_Field.DTE_Field_Type_select.DTE_Field_Name_Invitations\.TrainingID {
          width: 100% !important;
      }

      div.DTED_Lightbox_Wrapper {
          width: 600px;
          margin-left: -300px;
      }
      div.DTE div.DTE_Body div.DTE_Field {
          padding-top: 5px
      }
      div.DTE div.DTE_Body div.DTE_Field > label {
          float: none;
          clear: both;
          width: 100%;
      }
      div.DTE div.DTE_Body div.DTE_Field > div.DTE_Field_Input {
          float: none;
          clear: both;
          width: 100%;
      }
     table.dataTable tbody th, table.dataTable tbody td {
           padding: 0px 0px;
     }
    </style>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    function convertDate(i) {
      var m_names = new Array("January", "February", "March",
      "April", "May", "June", "July", "August", "September",
      "October", "November", "December");
      var m_names = new Array("Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul", "Aug", "Sep",
      "Oct", "Nov", "Dec");

      var d = new Date(i);
      var curr_date = d.getDate();
      var sup = "";
      if (curr_date == 1 || curr_date == 21 || curr_date ==31)
         {
         sup = "st";
         }
      else if (curr_date == 2 || curr_date == 22)
         {
         sup = "nd";
         }
      else if (curr_date == 3 || curr_date == 23)
         {
         sup = "rd";
         }
      else
         {
         sup = "th";
         }

      var curr_month = d.getMonth();
      var curr_year = d.getFullYear();
      var curr_year = curr_year.toString().substring(2);

      return curr_date + "<SUP>" + sup + "</SUP> " + m_names[curr_month] + " " + curr_year;
    }

    function ReturnIconInv(method) {
	var icon;
      switch (method) {
	  case '5':
	    icon='envelope outline';
	    break;
	  case '4':
	    icon='user outline';;
	    break;
	  case '1':   
	    icon='at';
	    break;
	  case '2':
	    icon='phone';
	    break;
	  case '3':
	    icon='mobile';
	    break;
	};
	return icon;
    }	 

    function ReturnIconRes(method) {
	var icon;
      switch (method) {
	  case '2':
	    icon='thumbs up outline';
	    break;
	  case '1':
	    icon='thumbs up';;
	    break;
	  case '0':   
	    icon='thumbs down';
	    break;
	};
	return icon;
    }	 

    function empty(data) {
      if(typeof(data) == 'number' || typeof(data) == 'boolean')
      {
        return false;
      }
      if(typeof(data) == 'undefined' || data === null)
      {
        return true;
      }
      if(typeof(data.length) != 'undefined')
      {
        return data.length == 0;
      }
      var count = 0;
      for(var i in data)
      {
        if(data.hasOwnProperty(i))
        {
          count ++;
        }
      }
      return count == 0;
    }

    var editor; // use a global for the submit and return data rendering in the examples

    $(document).ready(function() {
      //This keeps select2 properly aligned when embedded in modals
      $.fn.modal.constructor.prototype.enforceFocus = function() {};

      var memno = document.getElementById("memberno").innerHTML;
      editor_remove = new $.fn.dataTable.Editor( {
        ajax: "php/invite_remove.php?member=" + memno.trim(),
        table: "#tempm",
        fields: [ {
          label: "Training ID:",
          name: "TrainingProgramme.ID",
          type: "hidden"
        } , {
          label: "Invitation ID:",
          name: "Invitations.ID",
          type: "hidden"
        } , {
          label: "Response ID:",
          name: "Responses.ID",
          type: "hidden"
        } , {
          label: "Attendance ID:",
          name: "Attendance.ID",
          type: "hidden"
        } ]
      } );

      function loader(phpFile){
        var test= new Array({"label" : "a", "value" : "a"});

        test.splice(0,1);
        $.ajax({
          url: phpFile,
          async: false,
          dataType: 'json',
          success: function (json) {
            for(var a=0;a<json.length;a++){
              if (json[a]['label'] !== undefined){
                obj= { "label" : json[a]['label'], "value" : json[a]['value']};
              }else{
                obj= { "label" : json[a], "value" : json[a]};
              }
              test.push(obj);
            }
            }
        });
        return test;
      }

      editor_addinvite = new $.fn.dataTable.Editor( {
        //ajax: "php/invite_create2.php?member=" + memno.trim(),
        template: '#inviteForm',
        //table: "#tempm",
        fields: [ {
          label: "Member:",
          name: "Invitations.Member",
          def: memno.trim(),
          type: 'hidden'
        } , {
          label: "Date From:",
          name: "dateFrom",
          type: "datetime",
          format: "Do MMM YY"
        } , {
          label: "Date To:",
          name: "dateTo",
          type: "datetime",
          format: "Do MMM YY"
        } , {
          //label: "Method:",
          name: "method",
          type: "select",
          options: loader('php/media.php'),
          placeholder: "Choose method",
          //options: [{ label: "telephon",value: "telephone"}],
        } , {
          //label: "Notes:",
          name: "notes",
          type: "textarea"
        } , {
          //label: "Include Archived:",
          name: "archived",
          type: "checkbox",
          //opts: { id: function(data){ return data.id; }, },
          //placeholder: "Choose course",
          options: [{ label: "Include Archived",value: "archived"}],
          def: "0"
        } , {
          label: "Course:",
          name: "courseID",
          type: "select2",
          opts: { id: function(data){ return data.id; }, },
          placeholder: "Choose course",
          options: []
        } ]
      } );

      editor_addresponse = new $.fn.dataTable.Editor( {
        //ajax: "php/response_create.php",
        template: '#responseForm',
        fields: [ {
          label: "InviteID:",
          name: "inviteID",
          type: 'hidden'
        } , {
          label: "Response:",
          name: "response",
          type: 'select',
          placeholder: "Choose Response",
          options: loader('php/response.php'),
        } , {
          label: "Method:",
          name: "method",
          type: "select",
          placeholder: "Choose method",
          options: loader('php/media.php'),
          //options: [{ label: "telephon",value: "telephone"}],
        } , {
          label: "Notes:",
          name: "notes",
          type: 'textarea'
        } ] 
      } );

      editor_addattendance = new $.fn.dataTable.Editor( {
        template: '#attendanceForm',
        fields: [ {
          label: "Member:",
          name: "member",
          def: memno.trim(),
          type: 'hidden'
        } , {
          label: "Date From:",
          name: "dateFrom",
          type: "datetime",
          format: "Do MMM YY"
        } , {
          label: "Date To:",
          name: "dateTo",
          type: "datetime",
          format: "Do MMM YY"
        } , {
          name: "notes",
          type: "textarea"
        } , {
          name: "archived",
          type: "checkbox",
          options: [{ label: "Include Archived",value: "archived"}],
          def: "0"
        } , {
          label: "Course:",
          name: "courseID",
          type: "select2",
          opts: { id: function(data){ return data.id; }, },
          placeholder: "Choose course",
          options: []
        } ]
      } );

      //Create course dependencies
      function myJunkFunc(editor_instance,name){
	  console.log(editor_instance);
        editor_instance.dependent( name, function ( val,data, callback ) {
          $.ajax({
             url:'php/inviteRecordAdd.php',
             data: {
               "val" : val ,
               "dateFrom" : editor_instance.field('dateFrom').val(),
               "dateTo" : editor_instance.field('dateTo').val(),
               "archived" : editor_instance.field('archived').val(),
             },
             dataType: 'json',
             success: function (res) {
               editor_instance.field('courseID').update(res);
             }
          });
        } );
      }

      editor_addinvite.on( 'submitComplete', function (e, json, data){
        member=(editor_addinvite.field('Invitations.Member').val());
        trainingID=(editor_addinvite.field('courseID').val());
        method=(editor_addinvite.field('method').val());
        notes=(editor_addinvite.field('notes').val());
        $.ajax({
          url: "php/inviteRecordAdd2.php",
          data: {
            member: member,
            trainingID: trainingID,
            method: method,
            notes: notes
          },
          dataType: 'json',
          success: function (json) {
          },
          error: function (json) {
		alert("FAILURE");
          },
        });
      });

      editor_addresponse.on( 'submitComplete', function (e, json, data){
        inviteID=(editor_addresponse.field('inviteID').val());
        response=(editor_addresponse.field('response').val());
        method=(editor_addresponse.field('method').val());
        notes=(editor_addresponse.field('notes').val());	
        $.ajax({
          url: "php/responseRecordAdd2.php",
          data: {
            inviteID: inviteID,
            response: response,
            method: method,
            notes: notes
          },
          dataType: 'json',
          success: function (json) {
          },
          error: function (json) {
		alert("FAILURE");
          },
        });
      });

      editor_addattendance.on( 'submitComplete', function (e, json, data){
        courseID=(editor_addattendance.field('courseID').val());
        member=(editor_addattendance.field('member').val());
        notes=(editor_addattendance.field('notes').val());	
        $.ajax({
          url: "php/attendanceRecordAdd2.php",
          data: {
            courseID: courseID,
            member: member,
            notes: notes
          },
          dataType: 'json',
          success: function (json) {
          },
          error: function (json) {
		alert("FAILURE");
          },
        });
      });


      myJunkFunc(editor_addinvite,'dateFrom');
      myJunkFunc(editor_addinvite,'dateTo');
      myJunkFunc(editor_addinvite,'archived');

      myJunkFunc(editor_addattendance,'dateFrom');
      myJunkFunc(editor_addattendance,'dateTo');
      myJunkFunc(editor_addattendance,'archived');

	// Activate an inline edit on click of a table cell
	$('#tempm').on( 'click', 'tbody td:not(:first-child)', function (e) {
	    editor.bubble( this );
	} );

      table = new      $('#tempm').DataTable( {
	  "fnDrawCallback": function(){
          $('.MD_popup') .popup({
            position : 'bottom left',
            title    : 'TEST:',
            delay: { show: 900, hide: 0 }
          });
        },
        autoWidth: false,
        responsive: true,
        select: {
	    select: 'single',
	    style: 'os',
	    selector:  'td:first-child'
	  },
        dom: "BrtipTS",
        order: [2,"desc"],
        ajax: "php/invite_info.php?member=" + memno.trim(),
        columns: [
          {
            data:  null,
            render:  function(data,type,row){
              if (type === 'sort'){ return data.TrainingProgramme.StartDate};
              var popup="<div class=\'ui center aligned \'>" +
                  data.Invitations.Medium +
                  " invite sent on " +
                  data.Invitations.Date+ "</div>";
              return "<div style='white-space:normal;' data-html='" +
                       data.TrainingProgramme.Date +
                       "'>"
			     + "<a href= ./?page=course&courseID=" + data.TrainingProgramme.ID  + ">"
                       + data.Courses.Title + "</a><br>(<small>"
                       + data.Venues.Name + " - "
                       + convertDate(data.TrainingProgramme.StartDate) + "</small>)"
                     "</div>"+
                     "<div class='ui popup'>" +
                       popup +
                     "</div>";
            }
          }, {
            data:  null,
            visible:  false,
            render:  function(data,type,row){
              if (empty(data.Invitations.Date)){ return ""};
              var popup="<div class=\'ui center aligned \'>" +
                  data.Invitations.Medium +
                  " invite sent on " +
                  data.Invitations.Date+ "</div>";
              return "<div data-html='" + data.TrainingProgramme.Date + "'>" + data.TrainingProgramme.CourseAbb + "</div><div class='ui popup'>" + popup + "</div>";
            }
          }, {
            data:  null,
            visible:  false,
            render:  function(data,type,row){
              if (empty(data.Invitations.Date)){ return ""};
              var popup="<div class=\'ui center aligned \'>" +
                  data.Invitations.Medium +
                  " invite sent on " +
                  data.Invitations.Date+ "</div>";
              return "<div data-html='" + data.TrainingProgramme.Date + "'>" + data.TrainingProgramme.VenueAbb + "</div><div class='ui popup'>" + popup + "</div>";
            }
          }, {
            data:  null,
            render:  function(data,type,row){
              if (empty(data.Invitations.Date)){ return ""};
              var popup="<div class=\'ui center aligned \'>" +
		    data.Invitations.Medium +
		    " invite sent on " +
		    data.Invitations.Date+ "</div>";
              popup= "<h4 class=\'ui center aligned top attached header\'>Invited</h4>" +
                "<div>" + 
		    convertDate(data.Invitations.Date) + 
                "</div><div>" + 
		    (data.Invitations.Notes) + 
                "</div>";
              return "<div data-html=\"" + popup + "\" class='MD_popup ui icon'><i class='ui " + ReturnIconInv(data.Invitations.Medium) + " icon'></i><div class='ui popup'>" + popup + "</div></div>";
            }
          }, {
            data:  null,
              render:  function(data,type,row){
                if (empty(data.Responses.Date)){ return ""};
                var popup="<div class=\'ui center aligned \'>" +
                  "Received " +
                  data.Responses.Medium +
                  " on " +
                  data.Responses.Date+
                  " stating that " +
                  data.Responses.response+ "</div>";
                  "<br>Notes: " +
                  data.Responses.Note+ "</div>";

			var ID_str = data.Responses.ID_hist;
   		      var ID_array = ID_str.split('|');
			var Resp_str = data.Responses.Resp_hist;
   		      var Resp_array = Resp_str.split('|');
			var Date_str = data.Responses.Date_hist;
   		      var Date_array = Date_str.split('|');
			var Medium_str = data.Responses.Medium_hist;
   		      var Medium_array = Medium_str.split('|');
			var Note_str = data.Responses.Note_hist;
   		      var Note_array = Note_str.split('|');
			var popup= "<h4 class=\'ui center aligned top attached header\'>Response</h4><table>"
   		      for(var i = 0; i < ID_array.length; i++) {
   			  // Trim the excess whitespace.
   			  ID_array[i] = ID_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
   			  // Add additional code here, such as:
                    popup=popup + "<tr><td>" + 
			  convertDate(Date_array[i]) + 
			  "</td><td>" + 
			  (Medium_array[i]) + 
			  "</td><td>" + 
			  (Note_array[i]) + 
			  "</tr></td>";
   		      };
                    popup=popup + "</table>";
                return "<div data-html=\"" + popup + "\" class='MD_popup ui icon'><i class='ui " + ReturnIconRes(data.Responses.Response) + " icon'><div class='ui popup'>" + popup + "</div></div>";
              }
          }, {
            data:  null,
              render:  function(data,type,row){
                if (empty(data.Attendance.TrainingID)){ return ""};
                var popup="<div class=\'ui center aligned \'>" +
                  "TrainingID: " +
                  data.Attendance.TrainingID+ "</div>";
                popup=data.Attendance.TrainingID;
                return "<div data-html='" + popup + "' class='ui icon'><i class='ui checkmark icon'></i><div class='ui popup'>" + popup + "</div></div>";
              }
          },
        ],
        buttons: [
          {
		enabled: false,
            formMessage: "Invite to a course.",
            className: 'ui circular icon basic button',
            text:      '<i class="ui icons ">' +
                         '<i class="send mail icon "></i>' +
                         '<i class="top right corner plus icon "></i>' +
                         '</i>',
            //text: 'Add Invite',
            action: function ( e, dt, node, config ) {
              editor_addinvite.title('Record Invitation');
              editor_addinvite .buttons( {
                label: "Save",
                fn: function () {
                  this.submit();
                  setTimeout(function(){ table.ajax.reload(null , false)}, 0750);
                }
              } ).create();
            }
          } , {
		enabled: false,
            //text: 'Add Response',
            extend: "edit",
            formMessage: "Respond to an invitation.",
            className: 'ui circular icon basic button',
            text:      '<i class="ui icons ">' +
                         '<i class="reply icon "></i>' +
                         '<i class="top right corner plus icon "></i>' +
                         '</i>',
            action: function ( e, dt, node, config ) {
              var rowData = dt.row({selected:true}).data();
		  var inviteID=(rowData.Invitations.ID);
		  var courseTitle=(rowData.Courses.Title);
		  var venueName=(rowData.Venues.Name);
		  var startDate=(rowData.TrainingProgramme.StartDate);
		    editor_addresponse.on('open', function () {
		    editor_addresponse.field('inviteID').set(inviteID);
		    $('#responseTitle').html(
                  "<div style='text-align:center;'>" + 
			  courseTitle + "<br>" +
			  venueName + "<br>" + 
			  startDate + "<br>" + 
			"</div>"
		    );
              });

              editor_addresponse.title('Record Response');
              editor_addresponse.buttons( {
                label: "Save",
                fn: function () {
                  this.submit();
                  setTimeout(function(){ table.ajax.reload(null , false); }, 0750);
                }
              } ).create();
            }
          } , {
		enabled: false,
            //text: 'Add Attendance',
            //formTitle: "Record Attendance",
            formMessage: "Confirm attendance on a course.",
            className: 'ui circular icon basic button',
            text:      '<i class="ui icons ">' +
                         '<i class="student icon "></i>' +
                         '<i class="top right corner plus icon "></i>' +
                         '</i>',
            action: function ( e, dt, node, config ) {
              editor_addattendance.title('Record Course Attendance');
              editor_addattendance.buttons( {
                label: "Save",
                fn: function () {
                  this.submit();
                  setTimeout(function(){ table.ajax.reload(null , false)}, 0750);
                }
              } ).create();
            }
          } , {
		enabled: false,
            extend: "edit",
            //text: "Delete",
            className: 'ui circular icon basic button',
            text:      '<i class="ui icons ">' +
                         '<i class="trash icon "></i>' +
                         '</i>',
            editor:editor_remove,
            formTitle: "Delete Record",
            formMessage: "Would you like to delete this record?",
            formButtons: [
              {
                label: 'No',
                fn: function () { this.close(); }
              } , {
                label: 'Yes',
                fn: function (e, dt, node, config) {
                  this.submit();
                  setTimeout(function(){ table.ajax.reload(null , false)}, 1000);
                } 
              } 
            ]
          }
        ] 
      } );

      table.buttons().container()
        .appendTo($('#btnInvite'));

      var admin = "<?php print $_SESSION['level']; ?>";
      /*if ( admin <= 3 ){ table.buttons().enable(); };*/
      table.buttons().disable();
      table.select.style('api');
    } );
  </script>

  <body class="dt-tempm">
    <!--Used to pass variable to javascript-->
    <div hidden id="memberno" value=42>
        <?php echo $_GET['member']; ?>
    </div>
    <div id='InviteTable' class="ui center aligned container">
      <table id="tempm" class="ui display responsive nowrap" width="100%" cellspacing="0">
      <!--table id="tempm" class="ui small sortable very compact table" -->
        <thead>
          <tr>
            <!--th class="ID">ID</th-->
            <!--th class="snnever">TrainingID</th>
            <th class="Member">Member</th-->
            <th class="Member">Course</th>
            <th class="Member">Course</th>
            <th class="Member">Venue</th>
            <th class="TURD">Inv</th>
            <!--th class="TURD">Response ID</th-->
            <th class="TURD">Res</th>
            <th class="TURD">Att</th>
            <!--th class="Date">Date</th>
            <th class="Medium">Medium</th-->
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </body>
</html>

<style>
  div.DTE_Body div.DTE_Body_Content div.DTE_Field {
    width: 100%;
  }
  div#inviteForm.ui.form div.DTE_Field_Name_dateFrom ,
  div#inviteForm.ui.form div.DTE_Field_Name_dateTo {
    width: 50%;
  }
  div#inviteForm.ui.form div.DTE_Field_Name_archived {
    width: 40%;
  }
  div#attendanceForm.ui.form div.DTE_Field_Name_dateFrom ,
  div#attendanceForm.ui.form div.DTE_Field_Name_dateTo {
    width: 50%;
  }
  div#attendanceForm.ui.form div.DTE_Field_Name_archived {
    width: 40%;
  }
</style>

<div id='inviteForm' class="ui form">
  <fieldset>
    <legend>Select Course</legend>
    <editor-field name='courseID'></editor-field>
    <editor-field name='dateFrom'></editor-field>
    <editor-field name='dateTo'></editor-field>
    <editor-field name='archived'></editor-field>
  </fieldset>
  <fieldset>
    <legend>Method:</legend>
    <editor-field name='method'></editor-field>
  </fieldset>
  <fieldset>
    <legend>Notes:</legend>
    <editor-field name='notes'></editor-field>
  </fieldset>
</div>

<div id='responseForm' class="ui form">
  <div id='responseTitle'></div>
  <!--fieldset-->
    <!--legend>Response:</legend-->
    <editor-field name='inviteID'></editor-field>
    <editor-field name='response'></editor-field>
  <!--/fieldset>
  <fieldset-->
    <!--legend>Method:</legend-->
    <editor-field name='method'></editor-field>
  <!--/fieldset>
  <fieldset-->
    <!--legend>Notes:</legend-->
    <editor-field name='notes'></editor-field>
  <!--/fieldset-->
</div>

<div id='attendanceForm' class="ui form">
  <fieldset>
    <legend>Select Course</legend>
    <editor-field name='courseID'></editor-field>
    <editor-field name='dateFrom'></editor-field>
    <editor-field name='dateTo'></editor-field>
    <editor-field name='archived'></editor-field>
  </fieldset>
  <fieldset>
    <legend>Notes:</legend>
    <editor-field name='notes'></editor-field>
  </fieldset>
</div>

