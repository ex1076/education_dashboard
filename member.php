<?php
  session_start();

  if(empty($_SESSION['user'])) {
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/login.php');
    print '
      <div class="ui main text center aligned container">
        <div class="ui center aligned container">
          This page requires a login
      ';
      include "login.php";
    print "</div></div>";
      exit;
  }
  include 'php/member_info.php';
  if(empty($data["Member"])) {
    print '
      <div class="ui main text center aligned container">
        <div class="ui center aligned container">
          There are no records for this member.
      ';
    print "</div></div>";
    exit;
  }

  //$data["Activist"]=str_replace("ý", ", ", $data["Activist"]);
  //$data["Activist"]=str("\[\^A-Za-z0-9\]+", ", ", $data["Activist"]);
?>

<style>
  .ui.table tr td {
        border-top: 0px;
  }
  .ui[class*="very compact"].table td {
        padding: .2em .2em;
  }
  .ui.checkbox input[disabled] ~ .box::after, .ui.checkbox input[disabled] ~ label, .ui.disabled.checkbox .box::after, .ui.disabled.checkbox label {
        opacity: 1.0;
        color: rgba(0,0,0); 
  }
  .ui.disabled.segment {
        opacity: 0.7;
        color: rgba(0,0,0); */
  }
</style>

<script type="text/javascript" language="javascript" class="init">
  var editor; // use a global for the submit and return data rendering in the examples
   
  $(document).ready(function() {
    var test= new Array({"label" : "a", "value" : "a"});
    var level=<?=$_SESSION['level']?>;
     
    var OOR=<?=$data["OOR"]?>;
    $('#contactCard,#employmentCard').on( 'dblclick', function() {
	str=$('#contactCard').attr('class');
	if(str.search('disabled') >= 0 || level>=3 || OOR ===0 ){return};
      $('#contactForm').modal( {
        inverted: false,
        dimmerSettings: { 
          useCSS   : true,
        }
      })
      .modal('show');
    });
     
    $('#editPage').on( 'click', function() {
      var selectedRows = table.rows( { selected: true } ).count();
	locked=1;
	if ( $('#editPage > i').attr('class') === 'ui unlock icon'){ locked=0; };
	if ( locked){
	  $('#editPage > i').attr('class','ui unlock icon');
	  $('#venueCard').removeClass('disabled')
	  $('#venueCard form div').removeClass('disabled');
	  if(level <= 3){
	    table.select.style('os');
	    table.button( 0 ).enable();
	    table.button( 2 ).enable();
	    table.button( 1 ).enable( selectedRows > 0 );
	    table.button( 3 ).enable( selectedRows > 0 );
	    if(OOR===1){
		$('#contactCard').removeClass('disabled');
		$('#employmentCard').removeClass('disabled');
	    };
	  };
	}else{
	  $('#editPage > i').attr('class','ui lock icon');
	  $('#venueCard').addClass('disabled')
	  $('#venueCard form div.ui.item').addClass('disabled');
        table.rows().deselect();
        table.select.style('api');
        table.buttons().disable();
	  if(level <= 3 && OOR===1){
	    $('#contactCard').addClass('disabled');
	    $('#employmentCard').addClass('disabled');
	  };
	};

    });

    $('#submitMember').on( 'click', function () {
      data=	{
	  Member : "<?=$data['Member']?>",
	  Title : $("#Title").val(),
	  FirstName : $("#FirstName").val(),
	  Surname : $("#Surname").val(),
	  OfficerName : $("#OfficerName").val(),
	  WorkplaceName : $("#WorkplaceName").val(),
	  Workplace : $("#Workplace").val(),
	  EmployerName : $("#EmployerName").val(),
	  Employer : $("#Employer").val(),
	  BranchName : $("#BranchName").val(),
	  BranchCode : $("#BranchCode").val(),
	  BSMailName : $("#BSMailName").val(),
	  Add1 : $("#Add1").val(),
	  Add2 : $("#Add2").val(),
	  Add3 : $("#Add3").val(),
	  Add4 : $("#Add4").val(),
	  Postcode : $("#Postcode").val(),
	  TelNoHome : $("#TelNoHome").val(),
	  TelNoWork : $("#TelNoWork").val(),
	  TelNoMobile : $("#TelNoMobile").val(),
	  EmailAddress1 : $("#EmailAddress1").val()
    };

	$.ajax({
        url:'php/member2.php',
        type:'POST',
	  data:{
	    action: 'edit',
		data: data
	  }, 
	  success: function () {
	    $('#contactForm').modal('hide');
	    window.location.reload();
	  },
	  error: function (err) {
          console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
	  }
      });
     });
    $('#deleteMember').on( 'click', function () {
      $('#deleteMemberForm').modal( {
        inverted: false,
        dimmerSettings: { 
          useCSS   : true,
        }
      })
      .modal('show');
    }) ;

    $('#deleteMemberConfirm').on( 'click', function () {
	$.ajax({
        url:'removeOORMember.php',
        data:{member:"<?=$data["Member"]?>"},
	  success: function() {
	    var $window = $(window),
	    fallback = '/education/';
	    hasHistory = false;

	    $window.on('beforeunload', function(){
		  hasHistory = true;
	    });

	    window.history.go(-1);
	    setTimeout(function(){
		if (!hasHistory){
		  window.location.href = fallback;
		};
	    }, 400);
	  }
	});
    }) ;

    editor = new $.fn.dataTable.Editor( {
        ajax: "sandbox/standalone.php",
        fields: [ 
                  {
                label: "Member:",
                name:  "Member",
                default: "<?=$_GET['member']?>",
            }, 
                  {
                label: "Course:",
                name:  "Course",
                type: "select",
                def: "select",
                options: [
                  {label:'select',name:'select'},
                  {label:'GMB',name:'GMB'},
                  {label:'WPO1',name:'WPO1'},
                  {label:'WPO2',name:'WPO2'},
                  {label:'HS1',name:'HS1'},
                  {label:'HS2',name:'HS2'},
                  {label:'ELC',name:'ELC'},
                  {label:'EQU',name:'EQU'}
                  ]
            },
                  {
                label: "Venue:",
                name:  "Venue",
                type: "datetime"
            },
                  {
                label: "DateFrom:",
                name:  "DateFrom",
                type: "datetime"
            },
                  {
                label: "DateTo:",
                name:  "DateTo",
                type: "datetime"
            },
                  {
                label: "CourseO:",
                name:  "CourseO",
                type: "select",
                //options: <?php include "test.php"?>
                //options: loader()
            },

            {
                label: "TrainingID:",
                name:  "TrainingID",
                type: "select",
                options: [ "Simple", "All" ],
                def: "Simple"
                //{ label: 'Norwich', value: '998' },
                //{ label: 'Hendon', value: '997' }
                //]
            }
        ]
    } );
     
    editor.dependent( 'TrainingID', function ( val ) {
      return val === 'Simple' ?
                { hide: ['DateFrom','DateTo'] } :
                { show: ['DateFrom','DateTo'] };
    } );
 
    $('#edit').on( 'click', function () {
        editor
            .buttons( {
                label: "Save",
                fn: function () { this.submit(); }
            } )
            .create( $(this).data('id'));
    } );
  } );
</script>

<div class="ui center aligned main container grid">
  <div class="ui row">
    <div class="ui two wide column">
    </div>
    <div class="ui twelve wide column">
      <h1 class="ui header"> 
        <?=$data["FirstName"]?> 
        <?=$data["Surname"]?> 
        <div class="ui mini sub header"><?=$data["Member"]?> </div>
      </h1>
    </div>
    <div class="ui right aligned two wide column">
      <div class="ui horizontal list">
        <!--div class="ui item">
          <i class="ui lock circular icon"></i>
        </div-->
        <?php
          if ($_SESSION['level']<4){
		if ($data["OOR"] == 1){
		  echo '
        		<div id="deleteMember" class="ui circular icon basic button">
        		  <i class="ui remove user icon"></i>
        		</div>';
		};
	    echo '
		<div id="editPage" class="ui circular icon basic button">
		  <i class="ui lock icon"></i>
		</div>';
	    };
        ?>
        </div>
      </div>
    </div>
  </div>
  <div class="ui row">
    <div class="ui basic segment">
      <div class="ui centered three column equal width grid">
        <div class="ui large screen only four wide column ">
          <div id='contactCard' class="ui right aligned raised disabled segment">
            <h4 class="ui center aligned top header"> Contact Details </h4>
            <table class="ui left aligned fixed single line very basic very compact table"> 
              <th class="ui right aligned">
              </th>
              <th class="ui fourteen wide">
              </th>
              <?php if (!empty(trim($data["Add1"]))){
                echo "<tr>";
                  echo "<td>";
                    echo "<i class='ui outline home icon'></i>";
                  echo "</td>";
                  echo "<td>";
                  if (!empty(trim($data["Add1"]))){echo $data["Add1"] . "<br>";}
                  if (!empty(trim($data["Add2"]))){echo $data["Add2"] . "<br>";}
                  if (!empty(trim($data["Add3"]))){echo $data["Add3"] . "<br>";}
                  if (!empty(trim($data["Add4"]))){echo $data["Add4"] . "<br>";}
                  if (!empty(trim($data["Postcode"]))){echo $data["Postcode"] ;}
                  echo "</td>";
                echo "</tr>";
              }?>
              <?php if (!empty(trim($data["TelNoHome"]))){
                echo "<tr>";
                  echo "<td>";
                    echo "<i class='ui call icon'></i>";
                  echo "</td>";
                  echo "<td>";
                    echo $data["TelNoHome"] ;
                  echo "</td>";
                echo "</tr>";
              }?>
              <?php if (!empty(trim($data["TelNoWork"]))){
                echo "<tr>";
                  echo "<td>";
                    echo "<i class='ui configure outline icon'></i>";
                  echo "</td>";
                  echo "<td>";
                    echo $data["TelNoWork"] ;
                  echo "</td>";
                echo "</tr>";
              }?>
              <?php if (!empty(trim($data["TelNoMobile"]))){
                echo "<tr>";
                  echo "<td>";
                    echo "<i class='ui center aligned mobile icon'></i>";
                  echo "</td>";
                  echo "<td>";
                    echo $data["TelNoMobile"] ;
                  echo "</td>";
                echo "</tr>";
              }?>
              <?php if (!empty(trim($data["EmailAddress1"]))){
                echo "<tr>";
                  echo "<td>";
                    echo "<i class='ui outline mail icon'></i>";
                  echo "</td>";
                  echo "<td>";
                    echo "<a href='mailto:" . $data["EmailAddress1"] . "'>" ;
                      echo $data["EmailAddress1"];
                    echo "</a>"; 
                  echo "</td>";
                echo "</tr>";
              }?>
            </table>
          </div>
          <div id="employmentCard" class="ui raised disabled segment">
            <h4 class="ui center aligned top header"> Employment Details </h4>
            <div  class="ui relaxed list"> 
                <?php 
                  if (!empty(trim($data["EmployerName"]))){ 
                    echo '<div class="ui item">';
                    echo '<div class="header">Employer</div>';
                    echo $data["EmployerName"] ;
                    if (!empty(trim($data["Employer"]))){ echo "<br><small>" . $data["Employer"] . "</small>";}
                    echo '</div>';
                  } 
                ?>
                <?php 
                  if (!empty(trim($data["WorkplaceName"]))){ 
                    echo '<div class="ui item">';
                    echo '<div class="header">Workplace</div>';
                    echo $data["WorkplaceName"] ;
                    if (!empty(trim($data["Workplace"]))){ echo "<br><small>" . $data["Workplace"] . "</small>";}
                    echo '</div>';
                  } 
                ?>
                <?php 
                  if (!empty(trim($data["BranchCode"]))){ 
                    echo '<div class="ui item">';
                    echo '<div class="header">Branch</div>';
                    echo $data["BranchName"] . " - "; 
                    echo $data["BranchCode"] . "<br>";
                    if (!empty(trim($data["BSMailName"]))){ echo "<small>" . $data["BSMailName"] . "</small>";}
                    echo '</div>';
                  } 
                ?>
                <?php 
                  if (!empty(trim($data["OfficerName"]))){ 
                    echo '<div class="ui item">';
                    echo '<div class="header">Officer</div>';
                    echo $data["OfficerName"];
                    echo '</div">';
                  } else { 
                    echo '<div class="ui item">';
                    echo '<div class="header">Officer</div>';
                    echo "Unallocated";
                    echo '</div">';
                  }
                ?>
            </div>
          </div>
        </div>
      </div> 
      <div class="ui eight wide column">
        <div id='trainingCard' class="ui raised segment">
	    <div class="ui two column grid">
		<div class="ui middle aligned row">
		  <div class="ui column">
		    <h4 class="ui header"> Invitation and Training History </h4>
		  </div>
		  <div class="ui right aligned column">
		    <div id='btnInvite' class="ui buttons"> 
		    </div>
		  </div>
		</div>
		<?php include_once "invitetable.php"; ?>
	    </div>
        </div>
      </div>
      <div class="ui large screen only four wide column">
        <div id='venueCard' class="ui raised disabled segment">
          <h4 class="ui center aligned top header"> Venue Preferences </h4>
          <?php include_once "preferredVenues.php"; ?>
        </div>

        <div id='coursesCard' class="ui raised disabled segment">
          <h4 class="ui center aligned top header"> Completed Core Courses </h4>
          <?php include_once "completedCourses.php"; ?>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<div class="ui ">
  <div id="deleteMemberForm" class="ui mini modal">
    <div class="ui icon header">
	<i class="archive icon"></i>
	Remove Membership Data
    </div>
    <div class="content">
	<p>This action will archive this member's personal details which will then scheduled for deletion at a later date. </p><p> Are you sure you want to continue</p>
    </div>
    <div class="actions">
	<div class="ui red basic cancel button">
	  <i class="remove icon"></i>
	  No
	</div>
	<div id="deleteMemberConfirm" class="ui green ok inverted button">
	  <i class="checkmark icon"></i>
	  Yes
	</div>
    </div>
  </div>
</div>

<div id="contactForm" class="ui tiny modal">
  <h1 class="ui center aligned header">
    <!--?=$data["Member"]?>
    <div class="ui mini sub header">Amend Details</div-->
    Amend Details
    <div class="ui mini sub header"><?=$data["Member"]?></div>
  </h1>
  <div class="content">
    <div class="ui form">
	<h4 class="ui dividing header">Personal Data</h4>
	<div class="fields">
	  <div class="field">
	    <div class="three fields">
		<div class="field">
		  <label>Name</label>
		  <div class="field">
		    <input id="Title" value="<?=$data["Title"]?>" placeholder="Title" type="text">
		  </div>
		  <div class="field">
		    <input id="FirstName" value="<?=$data["FirstName"]?>" placeholder="First Name" type="text">
		  </div>
		  <div class="field">
		    <input id="Surname" value="<?=$data["Surname"]?>" placeholder="Last Name" type="text">
		  </div>
		</div>
		<div class="field">
		  <label>Address</label>
		  <div class="field">
		    <input id="Add1" value="<?=$data["Add1"]?>" placeholder="First llne" type="text">
		  </div>
		  <div class="field">
		    <input id="Add2" value="<?=$data["Add2"]?>" placeholder="Second llne" type="text">
		  </div>
		  <div class="field">
		    <input id="Add3" value="<?=$data["Add3"]?>" placeholder="Third llne" type="text">
		  </div>
		  <div class="field">
		    <input id="Add4" value="<?=$data["Add4"]?>" placeholder="Fourth llne" type="text">
		  </div>
		  <div class="field">
		    <input id="Postcode" value="<?=$data["Postcode"]?>" placeholder="Postcode" type="text">
		  </div>
		</div>
		<div class="field">
		  <label>Contact</label>
		  <div class="field">
		    <input id="TelNoHome" value="<?=$data["TelNoHome"]?>" placeholder="Home No" type="text">
		  </div>
		  <div class="field">
		    <input id="TelNoWork" value="<?=$data["TelNoWork"]?>" placeholder="Work No" type="text">
		  </div>
		  <div class="field">
		    <input id="TelNoMobile" value="<?=$data["TelNoMobile"]?>" placeholder="Mobile No" type="text">
		  </div>
		  <div class="field">
		    <input id="EmailAddress1" value="<?=$data["EmailAddress1"]?>" placeholder="Email Address" type="text">
		  </div>
		</div>
	    </div>
	  </div>
	</div>
	<h5 class="ui dividing header">Employment</h5>
	<div class="two fields">
	  <div class="field">
	  <label>Employer</label>
	    <div class="field">
		<input id="EmployerName" value="<?=$data["EmployerName"]?>" placeholder="Name" type="text">
	    </div>
	    <div class="field">
		<input id="Employer" value="<?=$data["Employer"]?>" placeholder="Code" type="text">
	    </div>
	  </div>
	  <div class="field">
	  <label>Workplace</label>
	    <div class="field">
		<input id="WorkplaceName" value="<?=$data["WorkplaceName"]?>" placeholder="Name" type="text">
	    </div>
	    <div class="field">
		<input id="Workplace" value="<?=$data["Workplace"]?>" placeholder="Code" type="text">
	    </div>
	  </div>
	</div>
	<h5 class="ui dividing header">Union Details</h5>
	<div class="two fields">
	  <div class="field">
	  <label>Branch</label>
	    <div class="field">
		<input id="BranchName" value="<?=$data["BranchName"]?>" placeholder="Name" type="text">
	    </div>
	    <div class="field">
		<input id="BranchCode" value="<?=$data["BranchCode"]?>" placeholder="Code" type="text">
	    </div>
	  </div>
	  <div class="field">
	  <label>Contacts</label>
	    <div class="field">
		<input id="BSMailName" value="<?=$data["BSMailName"]?>" placeholder="Branch Sec" type="text">
	    </div>
	    <div class="field">
		<input id="OfficerName" value="<?=$data["OfficerName"]?>" placeholder="Officer" type="text">
	    </div>
	  </div>
	</div>
    </div>
  </div>
  <div class="actions">
    <div id="submitMember" class="ui ok  button">
	Submit
    </div>
  </div>
</div>
