<?php

  require('php/config.php');
    $db_username    = str_ireplace("@gmb.org.uk","",$_POST['user']);
    $db_password    = $_POST['password'];
    $db_name        = 'education';
    $db_host        = 'localhost';

  $mysqli = @new mysqli($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "<br>";
    exit;
  }

  // username and password sent from form
  $myusername= str_ireplace("@gmb.org.uk","",$_POST['user']);
  $mypassword=$_POST['password'];
  // To protect MySQL injection (more detail about MySQL injection)
  //$myusername = stripslashes($myusername);
  //$mypassword = stripslashes($mypassword);
  //$myusername = mysqli_real_escape_string($mysqli,$myusername);
  //$mypassword = mysqli_real_escape_string($mysqli,$mypassword);

  if (!($res = $mysqli->query(
    "SELECT * FROM Users WHERE user='" . $myusername . "'"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
    exit;
  };

  // Mysql_num_row is counting table row
  $count=mysqli_num_rows($res);

  // If result matched $myusername and $mypassword, table row must be 1 row
  if($count==1){
    //
    // Register $myusername, $mypassword and redirect to file "login_success.php"
    session_start();
    $row=mysqli_fetch_array($res);
    echo (session_id());
    $_SESSION["user"] = $db_username;
    $_SESSION["firstname"] = $row["FirstName"];
    $_SESSION["surname"] = $row["Surname"];
    $_SESSION["level"] = $row["Level"];
    $_SESSION["password"] = $db_password;
    #$_SESSION["password"] = password_hash($db_password, PASSWORD_DEFAULT);

    //header("Location: loginsuccess.php");
    //header("Location: /education/?page=loginsuccess");
    header("Location: http://".$_SERVER['SERVER_NAME'].$_SESSION['page']);
    //
    if (!($res = $mysqli->query(
      "INSERT INTO Audit_Logins (Username, Hostname, Status) Values('" . $myusername . "','" . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "','logged in')"
    ))) {
      echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
      exit;
    };
  } else {
    if (!($res = $mysqli->query(
      "INSERT INTO Audit_Logins (Username, Hostname, Status) Values('" . $myusername . "','" . gethostbyaddr($_SERVER['REMOTE_ADDR']) . "','failed login')"
    ))) {
      echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
      exit;
    };
    echo "Wrong Username or Password <br>";
  }
?>
