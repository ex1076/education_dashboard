<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Landing Page</title>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    $(document).ready(function() {
      $('.ui.form').form({
        on: 'submit',
        fields : {
          user: {
            identifier : 'user',
            rules: [
              {
                type : 'regExp[/.*@gmb.org.uk$/]',
                prompt : 'Make sure you use a valid GMB email address'
              }
            ]
          },
          password: {
            identifier : 'password',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }
            ]
          }
       }
      }) ;
    });
  </script>

  <body>
    <!--div class="ui fluid center aligned container">
      <div class="ui container"-->
        <div class="ui basic centered aligned card">
        <div class="ui basic content">
        <h1 class="ui header">Login</h1>
        <form class="ui form" name="login" action="checklogin.php" method="post">
          <div class="field">
            <input type="text" name="user" placeholder="Email Address"></input><br>
          </div>
          <div class="field">
            <input type="password" name="password" placeholder="Password"></input><br>
          </div>
          <!-- if this is checked enable the login to persist between browser sessions (up until a limit)-->
          <div class="ui submit button">Login</div>
           
          <div class="ui button">Forgot Password</div>
          <!--div class="field">
            <input type="checkbox" name="remember" >Remember Me</input><br>
          </div-->
          <div class="ui error message"></div>
        </form>
        </div>
        </div>
          <!-- if this button us pushed bring up a modal, that checks if user wants to reset password
            if confirmed run script that emails password back to user as long as they are registered
            if they are not registered then say so
            <validate that the email address ends in @gmb.org.uk
      </div>
    </div>
            -->
  </body>
</html>
