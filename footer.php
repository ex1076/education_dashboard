
<footer class="site-footer">
<div class="ui inverted vertical footer segment" style="padding:20px">
  <div class="ui center aligned container">
    <div class="ui stackable center centered inverted basic grid">
      <div class="five wide column">
        <div class="ui right aligned inverted segment">
        <h4 class="ui inverted right aligned header">Contact Us</h4>
          <p>GMB Luton Office<br>
             Jansel House<br>
             Hitchin Road<br> 
             Luton<br>
             LU2 7XH<br>
          </p>
        
          <div class="ui item ">
            <i class="ui phone icon"></i> 01582 393 165
          </div>
          <div class="ui item ">
            <i class="ui mail icon"></i>
            <a style="color:#ffffff;" href="mailto:helen.mendieta@gmb.org.uk">helen.mendieta@gmb.org.uk</a>
          </div>
        </div>
      </div>
      <div class="five wide column">
        <div class="ui text left aligned inverted tiny segment">
        <h4 class="ui inverted centered header">Data Accuracy</h4>
          <p>
          All membership data (which includes personal, workplace, and branch details etc.) are accurate as of <?php include "php/lastimport.php"?>.  If you know of data that needs amending please use the Membership System to make the apprioriate changes.</p><p>
          All training data (which includes invitation and attendance records) are live and up-to-date.
          </p>
          <div class="ui horizontal inverted small divided link list">
            <a class="item" href="/education/?page=privacy">Privacy Policy</a>
          </div>
        </div>
      </div>
          <div class="ui basic row ">
    <a href="https://www.gmblondon.org.uk" >
      <i class="ui inverted centered large browser link icon"> </i>
    </a>
    <a href="https://www.facebook.com/GMBLondonRegion/" >
      <i class="ui inverted centered large facebook link icon"> </i>
    </a>
    <a href="https://twitter.com/GMBLondonRegion" >
      <i class="ui inverted centered large twitter link icon"> </i>
    </a>
    <a href="https://www.flickr.com/photos/gmblondon/" >
      <i class="ui inverted centered large flickr link icon"> </i>
    </a>
    <a href="https://www.youtube.com/channel/UCSCkNCcvtSssI3zJv_jH4bQ" >
      <i class="ui inverted centered large youtube play link icon"> </i>
    </a>
          </div>
    </div>

  </div>
</div>
</footer>
