<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Landing Page</title>
  </head>

  <script type="text/javascript" language="javascript" class="init">

    $(document).ready(function() {
      $('.ui.dropdown').dropdown();

      $('.ui.form').form({
        on: 'submit',
        fields : {
          FirstName: {
            identifier : 'FirstName',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a first name'
              }
            ]
          },
          Surname: {
            identifier : 'Surname',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a surname'
              }
            ]
          },
          EmailAddress: {
            identifier : 'EmailAddress',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter an email address'
              }, {
                type : 'regExp[/.*@gmb.org.uk$/]',
                prompt : 'Please enter a GMB email address'
              }
            ]
          },
          Password: {
            identifier : 'Password',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }
            ]
          },
          ConfirmPassword: {
            identifier : 'ConfirmPassword',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }, {
                type : 'match[Password]',
                prompt : 'Passwords do not match'
              }
            ]
          },
          Permissions: {
            identifier : 'Permissions',
            rules: [
              {
                type : 'empty',
                prompt : 'Please select a permision level'
              }
            ]
          }
        }
      }) ;
    });

  </script>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">
        <?php 
          echo $_SESSION["firstname"] . " ";
          echo $_SESSION["surname"] ;
        ?>
      </h1>
      <div class="ui container">
        <div class="ui compact center segment">
          <form class="ui form" name="addUser" action="addUser.php" method="post">
            <div class="field">
              <label>Email Address</label>
              <input name="EmailAddress" placeholder="Email Address" type="text">
            </div>
            <div class="field">
              <label>First Name</label>
              <input name="FirstName" placeholder="First Name" type="text">
            </div>
            <div class="field">
              <label>Surname</label>
              <input name="Surname" placeholder="Surname" type="text">
            </div>
            <div class="field">
              <label>Password</label>
              <input name="Password" placeholder="Password" autocomplete="off" type="Password">
            </div>
            <div class="field">
              <label>Confirm Password</label>
              <input name="ConfirmPassword" placeholder="ConfirmPassword" autocomplete="off" type="password">
            </div>
            <div class="field">
              <label>Permissions</label>
              <div class="ui fluid normal selection dropdown">
                <input name="Permissions" type="hidden">
                <i class="dropdown icon"></i>
                <div class="default text">Permissions</div>
                <div class="menu">
                  <div class="item" data-value="4">User</div>
                  <div class="item" data-value="3">Admin</div>
                  <div class="item" data-value="2">SuperUser</div>
                </div>
              </div>
            </div>
            <button class="ui submit button">Submit</button>
            <div class="ui error message"></div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
