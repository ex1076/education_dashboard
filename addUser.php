<?php

  session_start();
  require('php/config.php');
  $db_username 	= str_ireplace("@gmb.org.uk","",$_SESSION['user']);
  $db_password 	= $_SESSION['password'];
  $db_name 		= 'education';
  $db_host 		= 'localhost';

  $user      	  = str_ireplace("@gmb.org.uk","",$_POST['EmailAddress']);
  $user_password 	  = $_POST['Password'];
  $user_permissions = $_POST['Permissions'];
  $user_email       = $_POST['EmailAddress'];
  $user_firstname   = $_POST['FirstName'];
  $user_surname 	  = $_POST['Surname'];

  $mysqli = @new mysqli($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "<br>";
    exit;
  }

  // username and password sent from form
  $myusername=$_POST['user'];
  $mypassword=$_POST['password'];
  // To protect MySQL injection (more detail about MySQL injection)
  $myusername = stripslashes($myusername);
  $mypassword = stripslashes($mypassword);
  $myusername = mysqli_real_escape_string($mysqli,$myusername);
  $mypassword = mysqli_real_escape_string($mysqli,$mypassword);

  if (!($res = $mysqli->query(
    "CREATE USER '" . $user . "'@'localhost' IDENTIFIED BY '" . $user_password . "';"
    #"CREATE USER 'TOOL_hand'@'localhost'"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
    exit;
  }else{
    $query = "INSERT INTO Users(user,Level,EmailAddress,FirstName,Surname) " . 
    "VALUES('" . 
    $user . "','" . 
    $user_permissions . "','" . 
    $user_email . "','" . 
    $user_firstname . "','" . 
    $user_surname . "'" . 
    ");";
    if ($user_permissions <= 4){ 
      $query .= 
      "GRANT INSERT ON education.Audit_Logins TO '" . $user . "'@'localhost';" . 
      "GRANT SELECT ON education.* TO '" . $user . "'@'localhost';" ;
    };
    if ($user_permissions <= 3){ 
      $query .= 
      "GRANT SELECT, INSERT, UPDATE ON education.* TO '" . $user . "'@'localhost';";
    };
    if ($user_permissions <= 2){ 
      $query .= 
      "GRANT CREATE USER ON *.* TO '" . $user . "'@'localhost' with GRANT OPTION;";
    };
    if ($user_permissions <= 1){ 
      $query .= 
      "GRANT ALL PRIVILEGES ON *.* TO '" . $user . "'@'localhost' with GRANT OPTION;";
    }
    if (!($res = $mysqli->multi_query($query))) {
      echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
      exit;
    }
  };


?>
