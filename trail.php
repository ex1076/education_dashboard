<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Editor example - Basic initialisation</title>
    <style type="text/css" class="init"> </style>
  </head>

  <script type="text/javascript" language="javascript" class="init">

    $(document).ready(function() {

      $('.tabular.menu .item').tab();
      console.log($('.tabular.menu .item'));

      function loader(phpFile){
        var test= new Array({"label" : "a", "value" : "a"});
         
        test.splice(0,1);
        console.log(test);
        $.ajax({
          url: phpFile,
          async: false,
          dataType: 'json',
          success: function (json) {
              for(var a=0;a<json.length;a++){
                if (json[a]['label'] !== undefined){
                  obj= { "label" : json[a]['label'], "value" : json[a]['value']};
                }else{
                  obj= { "label" : json[a][1], "value" : json[a][0]};
              }
                test.push(obj);
              }
            }
        });
        console.log("test");
        console.log(test);
        return test;
      }

      // This selects which elements are editable-->
      var editor = new $.fn.dataTable.Editor( {
        ajax: "php/trail_activity.php"
      } );
      
      // Form to create course
      $('#BigFucker').on( 'click', function () {
        console.log("In Big Fucker");
        editor.buttons( {
          label: "Save",
          fn: function () { 
            this.submit();
            setTimeout(function(){table1.ajax.reload(null , false)}, 0750);
          }
        } )
        //.on( 'submitComplete', function (r, json,data) {alert('New row added')})
        .on( 'submitComplete', function (r, json,data) {
          setTimeout(function(){table1.ajax.reload(null , false)}, 0750);
        })
        .create();
        table.draw();
      } );

      // DataTable definition
      var table1 =	$('#tempn').DataTable( {
        autoWidth: false,
        order: [[1, 'desc']], //sort by date
        select: true,
        //dom:'r<ti><T>S',
        dom:'<ti>p<T>S',
        ajax: { 
          url: "php/trail_activity.php",
        },
        columns: [
          { data: 'date_chng'},
          { data: 'username'},
          { data: 'status'},
          { data: 'table'},
          { data: 'row_id'},
          { data: 'values'}
        ],
        order: [[ 0, "desc" ]],
        buttons: [
            { extend: "create", editor: editor },
        ]
      } );
      //
    });

  </script>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">User Activity</h1>
      <div class="ui basic segment">
        <div class="ui bottom attached active tab basic segment" data-tab="upcoming">
          <?php // include_once "coursestable.html"; ?>
          <!--table style="visibility: hidden;" id="tempn" class="ui compact selectable striped called table raised segment" -->
          <!-- DataTable -->
          <table id="tempn" class="ui small sortable very compact single line table" >
            <thead>
              <tr>
                <th>Date</th>
                <th>Username</th>
                <th>Status</th>
                <th>Table</th>
                <th>Row</th>
                <th>Values</th>
                <!--<th>Tutor Allocated</th>-->
                <!--<th>Materials Sent</th>-->
              </tr>
            </thead>
          </table>
        </div>
        <div  class="ui right very close rail">
           <div class="ui basic left aligned segment">
             <div data-hmtl="test" id="search" class="ui icon">
             <i id="BigFucker" class="ui plus circular icon link"></i>
             </div>
           </div>
        </div>
      </div>
    </div>
  </body>

<html>
