<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
      <title>Education Dashboard - Repslist</title>
	  <link rel="stylesheet" type="text/css" href="media/css/venueform.css">
	<style>
table.dataTable.table thead th.sorting::after, table.dataTable.table thead th.sorting_asc::after, table.dataTable.table thead th.sorting_desc::after, table.dataTable.table thead td.sorting::after, table.dataTable.table thead td.sorting_asc::after, table.dataTable.table thead td.sorting_desc::after {
	position: absolute;
	top: 5px;
	right: 8px;
	display: block;
	font-family: Icons;
}
      body.animating.in.dimmable, body.dimmed.dimmable {
	  overflow: visible;
      }
	</style>

    <?php 
      session_start();
      if(empty($_SESSION['user'])) {

        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/login.php');
        print '
        <div class="ui main text center aligned container">
          <div class="ui center aligned container">
            This page requires a login
          ';
          include "login.php";
        print "</div></div>";
          exit;
      }
      include ('php/corecourses.php');
      $myArray2=[];
      foreach ($myArray as &$value) {
        array_push($myArray2,"ATT." . $value[0]);
      }
    ?>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    var editor; // use a global for the submit and return data rendering in the examples
                
    function filterColumn (i) {
      //alert("ONE " + $('#col'+i+'_filter').val());
     // alert("TWO " + $('#col'+i+'_filter').val().split(",").join("|"));
      $('#example').DataTable().column(i).search(
        $('#col'+i+'_filter').val().split(",").join("|"),true,false
      ).draw(); 
    }

function convertDate(i) {
  var m_names = new Array("January", "February", "March", 
  "April", "May", "June", "July", "August", "September", 
  "October", "November", "December");

  var d = new Date(i);
  var curr_date = d.getDate();
  var sup = "";
  if (curr_date == 1 || curr_date == 21 || curr_date ==31)
     {
     sup = "st";
     }
  else if (curr_date == 2 || curr_date == 22)
     {
     sup = "nd";
     }
  else if (curr_date == 3 || curr_date == 23)
     {
     sup = "rd";
     }
  else
     {
     sup = "th";
     }

  var curr_month = d.getMonth();
  var curr_year = d.getFullYear();

  return curr_date + "<SUP>" + sup + "</SUP> " + m_names[curr_month] + " " + curr_year;
}
                
    function empty(data) {
      if(typeof(data) == 'number' || typeof(data) == 'boolean')
      { 
        return false; 
      }
      if(typeof(data) == 'undefined' || data === null)
      {
        return true; 
      }
      if(typeof(data.length) != 'undefined')
      {
        return data.length == 0;
      }
      var count = 0;
      for(var i in data)
      {
        if(data.hasOwnProperty(i))
        {
          count ++;
        }
      }
      return count == 0;
    }
    
    $(document).ready(function() {
	$('.column_filter').on('keyup change', function() { filterColumn( $(this).parent().attr('data-column')); 
	} );

      $('#filter_officer')
	.dropdown({
	    apiSettings: {
		    // this url parses query server side and returns filtered results
	    url: 'php/officer_list.php',
	    },
	    filterRemoteData: true,
	    fullTextSearch: true 
	}) ;



      $('#filter_employer')
	.dropdown({
	    apiSettings: {
		    // this url parses query server side and returns filtered results
	    url: 'php/employer_list.php',
	    },
	    saveRemoteData: false,
	    filterRemoteData: true,
	    fullTextSearch: 'exact' 
	}) ;
      $('#filter_workplace')
	.dropdown({
	    apiSettings: {
		    // this url parses query server side and returns filtered results
	    url: 'php/workplace_list.php',
	    },
	    saveRemoteData: false,
	    filterRemoteData: true,
	    fullTextSearch: 'exact' 
	}) ;
      $('#filter_branch')
	.dropdown({
	    apiSettings: {
		    // this url parses query server side and returns filtered results
	    url: 'php/branch_list.php',
	    },
	    saveRemoteData: false,
	    filterRemoteData: true,
	    fullTextSearch: 'exact' 
	}) ;

      $('#filter_postholder')
	.dropdown({
	    apiSettings: {
		    // this url parses query server side and returns filtered results
	    url: 'php/postholder_list.php',
	    },
	    saveRemoteData: false,
	    filterRemoteData: true,
	    fullTextSearch: 'exact' 
	}) ;

      $('#clearbutton').on( 'click', function () {
	  $('.ui.dropdown').dropdown('clear');
	  $('.column_filter').val("");
      }) ;

      $('#BigFucker').on( 'click', function () {
	  $('#customSearchForm').modal( {
	    inverted: true,
	    dimmerSettings: { opacity: 0.3, useCSS   : true }
	  })
	  .modal('show');
      }) ;

      $('#BigFucker2').on( 'click', function () {
        console.log("In Big Fucker");
        editor
          .title( "Add Non-Regional Delegate")
          .buttons( {
            label: "Save",
            fn: function () { this.submit(); }
          } )
          .on( 'submitComplete', function (r, json,data) {
            setTimeout(function(){table1.ajax.reload(null , false)}, 0750);
          })
          .create();
      } );

      // -#This selects which elements are editable-->
      editor = new $.fn.dataTable.Editor( {
        ajax: "php/member2.php",
        table: "#example",
        template: "#customForm",
        title: "#customForm",
        fields: [ {
          name: "Member",
          attr:  { placeholder: 'Membership No' }
        }, {
          name: "Title",
          attr:  { placeholder: 'Title' }
        }, {
          name: "FirstName",
          attr:  { placeholder: 'First Name' }
        }, {
          name: "Surname",
          attr:  { placeholder: 'Last Name' }
        }, {
          name: "Add1",
          attr:  { placeholder: 'Address 1' }
        }, {
          name: "Add2",
          attr:  { placeholder: 'Address 2' }
        }, {
          name: "Add3",
          attr:  { placeholder: 'Address 3' }
        }, {
          name: "Add4",
          attr:  { placeholder: 'Address 4' }
        }, {
          name: "Postcode",
          attr:  { placeholder: 'Post Code' }
        }, {
          name: "TelNoHome",
          attr:  { placeholder: 'Home Telephone' }
        }, {
          name: "TelNoWork",
          attr:  { placeholder: 'Work Telephone' }
        }, {
          name: "TelNoMobile",
          attr:  { placeholder: 'Mobile Telephone' }
        }, {
          name: "EmailAddress1",
          attr:  { placeholder: 'Email Address' }
        }
        ]
      } );

      // Activate an inline edit on click of a table cell
      $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this );
      } );
      
      var myArray= <?php echo json_encode($myArray2 ); ?>;
      var jArray= <?php echo json_encode($myArray2 ); ?>;
      var arrayLength = jArray.length;
      for(var i=0;i<arrayLength;i++){
        myArray=myArray.concat([jArray[i]]);
      }

      var columns1 = [
         { 
           data: null, 
           visible: true,
           width: '180pt', 
           render: function (data,type,row){
             // Combine the first and last names into a single table field
	     if ( type === 'export'){return data.MD.Member}
             var popup=
               "<h4 class=\'ui center aligned top header\'> Member Details </h4>" +
               "<div style=\'display:none;\'>" + data.MD.Member + "</div>" + 
               "<div class=\'ui very compact basic segment\'>" +
                 "<table  class=\'ui very compact  basic table\'> " +
                   "<tr class=\'top aligned\'>" +
                     "<td class=\'right aligned\'>Employer:</td>" +
                     "<td>" + data.MD.EmployerName + " <small><b>(" + data.MD.Employer + ")</b></small></td>" +
                   "</tr>" +
                   "<tr class=\'top aligned\'>" +
                     "<td class=\'right aligned\'>Workplace:</td>" +
                     "<td>" + data.MD.WorkplaceName + " <small><b>(" + data.MD.Workplace + ")</b></small></td>" +
                   "</tr>" +
                   "<tr class=\'top aligned\'>" +
                     "<td class=\'right aligned\'>Officer:</td>" +
                     "<td>" + data.MD.OfficerName + " <small><b>" + "</b></small></td>" +
                   "</tr>" +
                   "<tr class=\'top aligned\'>" +
                     "<td class=\'right aligned\'>Branch:</td>" +
                     "<td>" + data.MD.BranchName + " <small><b>(" + data.MD.BranchCode + ")</b></small></td>" +
                   "</tr>" +
                 "</table>" +
               "</div>";
               return "<div data-html=\""+popup+"\"  id=\"smile\" class=\"MD_popup ui basic right aligned\" data-variation=\"very wide\"> <a href= ./?page=member&member=" + data.MD.Member + ">" +  data.MD.Member + "</a><br>"
                 //return "<a href= ./?page=member&member=" + data.MD.Member + ">" 
                 //+ data.MD.Member + "</a> <br> " 
                 + data.MD.FirstName + " "  
                 + data.MD.Surname + "</div>";}
           }, { 
             "_": "member",
             data:  null, 
             visible: false, render: function (data,type,row){
	       if ( type === 'export'){return data.MD.FirstName + " " + data.MD.Surname}
             return "<div>" + data.MD.Member + " " 
                            + data.MD.FirstName + " " 
                            + data.MD.Surname + " " 
                            + data.MD.Add1 + " " 
                            + data.MD.Add2 + " " 
                            + data.MD.Add3 + " " 
                            + data.MD.Add4 + " " 
                            + data.MD.Postcode + " " 
                            + data.MD.TelNoHome + " " 
                            + data.MD.TelNoWork + " " 
                            + data.MD.TelNoMobile + " " 
                            + data.MD.EmailAddress1 + "</div>";
             }
           }, { 
             "_": "officer",
             data:  null, 
             visible: false,
             render: function (data,type,row){
		  if ( empty(data.MD.OfficerName)) {
		    return ""
		  } 
             return "<div>" + data.MD.OfficerName + "</div>";
             }
           }, { 
             "_": "contact",
             data:  null, 
             visible: false,
             render: function (data,type,row){
             return "<div>" + data.MD.Member + " " 
                            + data.MD.FirstName + " " 
                            + data.MD.Surname + " " 
                            + data.MD.Add1 + " " 
                            + data.MD.Add2 + " " 
                            + data.MD.Add3 + " " 
                            + data.MD.Add4 + " " 
                            + data.MD.Postcode + " " 
                            + data.MD.TelNoHome + " " 
                            + data.MD.TelNoWork + " " 
                            + data.MD.TelNoMobile + " " 
                            + data.MD.EmailAddress1 + "</div>";
             }
           }, { 
             "_": "employer",
             data:  null, 
             visible: false,
             render: function (data,type,row){
		  if ( empty(data.MD.Employer)) {
		    return ""
		  } 
             return "<div>" + data.MD.EmployerName + " (" + data.MD.Employer + ")</div>";
             }
           }, { 
             "_": "workplace",
             data:  null, 
             visible: false,
             render: function (data,type,row){
		  if ( empty(data.MD.Workplace)) {
		    return ""
		  } 
             return "<div>" + data.MD.WorkplaceName + " (" + data.MD.Workplace + ")</div>";
             }
           }, { 
             "_": "branch",
             data:  null, 
             visible: false,
             render: function (data,type,row){
		  if ( empty(data.MD.BranchCode)) {
		    return ""
		  } 
	       if ( type === 'export'){return data.MD.BranchName + " (" + data.MD.BranchCode + ")"}
               return "<div>" + data.MD.BranchName + " " + data.MD.BranchCode + " " + data.MD.BSMailName + "</div>";
               //return "<div>" + data.MD.WorkplaceName + " " + data.MD.Workplace + "</div>";
             }
           }, { 
             "_": "activist",
             data:  null, 
             visible: false,
             render: function (data,type,row){
               return "<div>" + data.MD.Activist + "</div>";
               //return "<div>" + data.MD.WorkplaceName + " " + data.MD.Workplace + "</div>";
             }
         },
      ];
      var arrayLength = myArray.length;
      for (var i = 0; i < arrayLength; i++) {
	  columns1=columns1.concat([ { 
		 data:  myArray[i],
//             render: function (data,type,row){
//		   return "<div>" + myArray[i] +  "</div>";
//		 }
        } ]);
      }
      var table1 =	$('#example').DataTable( {
        //dom: "Bfrtip",
        "fnDrawCallback": function(){
          $('.MD_popup') .popup({
            position : 'bottom left',
            title    : 'Invited:',
            delay: { show: 900, hide: 0 }
          });
        },
        dom:'B<ti>p<T>S',
        ajax: "php/member.php",
        scrollCollapse: true,
	  //buttons: [ 
	  //  { extend: 'collection',
	  //    className: 'ui basic button ',
	 //     text: 'export',
		buttons: [
		{
                extend:    'csv',
		    title: 'TrainingRecords',
		    className: 'ui circular icon basic button',
                text:      '<i class="ui download icon "></i>',
		    exportOptions: { 
			orthogonal: 'export', 
			columns: [0, 1, 2, 4, 5, 6, '.corecourse']
		    }
		    //exportOptions: { columns: '.corecourse'}
                
            }
	//	  , 'csvHtml5', 'excel'],
	//    }
	  ],
        columns: columns1,
        columnDefs: [
          {
            targets: "course",
            searchable: true,
            className: "ui basic center aligned corecourse",
            width: "50pt",
            visible: true,
            render: function (data,type,row){
		  if ( empty(data)) {
		    return ""
		  } 
              if (type === 'export' ){ 
                return data
		  } else {
		    var popup=
                "<div class=\'ui center aligned \'>Attended: " + convertDate(data) + "</div>" ;
                return "<div data-html=\""+popup+"\" class=\"MD_popup ui icon\"> <i class=\"checkmark icon\"></i></div>"
              }
            }
          } , {
            targets: "_all",
            visible: false
          }
        ],
        select: 'api',
        //select: {
          //style:    'os',
          //selector: 'td:first-child'
        //},
      } );

	table1.buttons().container()
          .appendTo($('#btnPDF'));


    } );
  </script> 

  <body>
    <div class="ui main center aligned container">
      <h1 class="ui header">Training Records</h1>
      <div class="ui centered grid basic segment">
	  <div id="btnPDF" class="column">
	    <div id="BigFucker" class="ui circular basic icon button">
		<i class="ui search icon"></i>
	    </div>
	    <?php
		if  (!empty($_SESSION['user']) && $_SESSION['level']<=3){
		  print '
			  <div id="BigFucker2" class="ui circular basic icon button">
			    <i class="ui add user icon"></i>
			  </div>';
		}
	    ?>
	  </div>
	  <table id="example" class="ui very compact striped  table" >
	  <!--table id="example" class="display responsive nowrap" -->
	    <thead>
		<tr>
		  <!--<th></th>-->
		  <th class="right aligned">Member</th>
		  <th>Name</th>
		  <th>Officer</th>
		  <th>Contact Details</th>
		  <th>Employer</th>
		  <th>Workplace</th>
		  <th>Branch</th>
		  <th>Posts</th>
		  
		  <?php
		    foreach($myArray as $item){
			echo "<th class='ui center aligned course'>" . $item['Abbreviation'] . "</th>";
		    };
		  ?>
		</tr>
	    </thead>
	  </table>
        </div>
      </div>
    </div>
    <div class="ui ">
	<div id="customSearchForm" class="ui mini modal">
	  <div class="ui center aligned basic segment">
	    <h4 class="ui center aligned  header">Record Search</h4>
	    <div id="cunt" class="ui center aligned  form">
		<div id="filter_member" data-column="1" class="ui fluid input">
		  <input type="text" placeholder="Name, Contact Details, Membership No." class="column_filter" id="col1_filter" autocomplete="off"> 
		</div>
		<div id="filter_officer" data-column="2" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" name="Member" class="column_filter" id="col2_filter" > 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Officer</div>
		</div>
		<div id="filter_employer" data-column="4" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" class="column_filter" id="col4_filter" > 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Employer</div>
		</div>
		<div id="filter_workplace" data-column="5" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" class="column_filter" id="col5_filter" > 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Workplace</div>
		</div>
		<div id="filter_branch" data-column="6" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" class="column_filter" id="col6_filter" > 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Branch</div>
		</div>
		<div id="filter_postholder" data-column="7" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" name="Posts" class="column_filter" id="col7_filter" autocomplete="off"> 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Posts</div>
		</div> 
		<div id="clearbutton"  class="ui labelled fluid button">Clear </div> 
	    </div>
	  </div>
	</div>
    </div>
    <div class="ui ">
	<div id="customForm">
	  <fieldset class="Name">
	    <legend>Name</legend>
	    <editor-field name="Member"></editor-field>
	    <editor-field name="Title"></editor-field>
	    <editor-field name="FirstName"></editor-field>
	    <editor-field name="Surname"></editor-field>
	  </fieldset>
	  <fieldset class="ContactDetails">
	    <legend>Contact Details</legend>
	    <editor-field name="TelNoHome"></editor-field>
	    <editor-field name="TelNoWork"></editor-field>
	    <editor-field name="TelNoMobile"></editor-field>
	    <editor-field name="EmailAddress1"></editor-field>
	  </fieldset>
	  <fieldset class="Address">
	    <legend>Address</legend>
	    <editor-field name="Add1"></editor-field>
	    <editor-field name="Add2"></editor-field>
	    <editor-field name="Add3"></editor-field>
	    <editor-field name="Add4"></editor-field>
	    <editor-field name="Postcode"></editor-field>
	  </fieldset>
	</div>
    </div>
  </body>
</html>
