<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Landing Page</title>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    $(document).ready(function() {
      $('.ui.form').form({
        on: 'submit',
        fields : {
          CurrentPassword: {
            identifier : 'CurrentPassword',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }
            ]
          },
          NewPassword: {
            identifier : 'NewPassword',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }
            ]
          },
          ConfirmPassword: {
            identifier : 'ConfirmPassword',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }, {
                type : 'match[NewPassword]',
                prompt : 'Passwords do not match'
              }
            ]
          }
        }
      }) ;
    });
  </script>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">
        <?php 
          echo $_SESSION["firstname"] . " ";
          echo $_SESSION["surname"] ;
        ?>
      </h1>
      <div class="ui container">
        <div class="ui compact center segment">
          <form class="ui basic form" name="changePassword" action="changePassword.php" method="post">
            <div class="field">
              <label>Current Password</label>
              <input name="CurrentPassword" placeholder="Current Password" type="password">
            </div>
            <div class="field">
              <label>New Password</label>
              <input name="NewPassword" placeholder="New Password" type="password">
            </div>
            <div class="field">
              <label>Confirm Password</label>
              <input name="ConfirmPassword" placeholder="Confirm Password" type="password">
            </div>
            <button class="ui submit button">Submit</button>
            <div class="ui error message"></div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
