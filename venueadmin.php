<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Repslist</title>
    <link rel="stylesheet" type="text/css" href="media/css/venueform.css">
    <?php 
      session_start();
      if(empty($_SESSION['user'])) {
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/login.php');
        print '
          <div class="ui main text center aligned container">
          <div class="ui center aligned container">
          This page requires a login
        ';
        include "login.php";
        print "</div></div>";
        exit;
      }
    ?>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    var editor; // use a global for the submit and return data rendering in the examples
                
    function filterColumn (i) {
      $('#example').DataTable().column(i).search(
        $('#col'+i+'_filter').val(),true,true
      ).draw(); 
    };    
    
    function empty(data) {
      if(typeof(data) == 'number' || typeof(data) == 'boolean')
      { 
        return false; 
      }
      if(typeof(data) == 'undefined' || data === null)
      {
        return true; 
      }
      if(typeof(data.length) != 'undefined')
      {
        return data.length == 0;
      }
      var count = 0;
      for(var i in data) {
        if(data.hasOwnProperty(i))
        {
          count ++;
        }
      }
      return count == 0;
    }
    
    $(document).ready(function() {
      $('.column_filter').on('keyup click', function() { filterColumn( $(this).parents('tr').attr('data-column')); } );

      $('#BigFucker').popup({
        on       : 'click',
        inline   : true,
        closable   : false,
        //preserve : true,
        //hoverable: true,
        position : 'bottom left',
        delay: { show: 300, hide: 800 }
      }) ;

      // -#This selects which elements are editable-->
      editor = new $.fn.dataTable.Editor( {
        ajax: "php/venueadmin.php",
        table: "#example",
        template: "#customForm",
        fields: [ {
          label: "Abbreviation:",
          name: "Abbreviation"
        }, {
          label: "Name:",
          name: "Name"
        }, {
          label: "",
          name: "Add1"
        }, {
          label: "",
          name: "Add2"
        }, {
          label: "",
          name: "Add3"
        }, {
          label: "",
          name: "Add4"
        }, {
          label: "",
          name: "Add5"
        }, {
          label: "CoreVenue:",
          name: "CoreVenue",
          type: "checkbox",
          separator: "|",
          options:   [
            { label: '', value: 1 }
          ],
          unselectedValue: 0,
        }
        ]
      } );

      // Activate an inline edit on click of a table cell
      $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this );
      } );
        
      $('#example').DataTable( {
        dom: "Bfrtip",
        "fnDrawCallback": function(){
          $('.VD_popup') .popup({
            position : 'top right',
            title    : 'Invited:',
            delay: { show: 900, hide: 0 }
          });
        },
        // dom:'B<ti>p<T>S',
        ajax: "php/venueadmin.php",
        select: "true",
        columns: [ { 
            "_": "abbreviation",
            data: null, 
            visible: true,
            width: '25%', 
            render: function (data,type,row){
              return "<div>" + data.Abbreviation + "</div";
            }
          } , { 
            "_": "name",
            data:  null, 
            visible: true,
            render: function (data,type,row){
              return "<div>" + data.Name + "</div";
            }
          } , { 
            "_": "address",
            data:  null, 
            visible: true,
            render: function (data,type,row){
              return "<div>" + data.Add1 + "</div>" + 
                     "<div>" + data.Add2 + "</div>" + 
                     "<div>" + data.Add3 + "</div>" + 
                     "<div>" + data.Add4 + "</div>" + 
                     "<div>" + data.Add5 + "</div"
            }
          } , { 
            "_": "address2",
            data:  null, 
            visible: false,
            render: function (data,type,row){
              return "<div>" + data.Add2 + "</div";
            }
          } , { 
            "_": "address3",
            data:  null, 
            visible: false,
            render: function (data,type,row){
              return "<div>" + data.Add3 + "</div";
            }
          } , { 
            "_": "address4",
            data:  null, 
            visible: false,
            render: function (data,type,row){
              return "<div>" + data.Add4 + "</div";
            }
          } , { 
            "_": "address5",
            data:  null, 
            visible: false,
            render: function (data,type,row){
              return "<div>" + data.Add5 + "</div";
            }
          } , { 
            "_": "corevenue",
            data:  null, 
            visible: false,
            render: function (data,type,row){
              return "<div>" + data.CoreVenue + "</div";
            }
          }
        ],
        columnDefs: [
        { 
          targets: "abbreviation", className: "ui basic center align",
          searchable: false,
          width: "7%",
          visible: true,
          render: function (data,type,row){
            if ( ! empty(data)) {
              return "<div data-html=\""+data+"\"  id=\"smile\" class=\"attended_to_popup ui icon\"> <i class=\"checkmark icon\"></i></div>"
            } else { return null }
          }
        } , {
          targets: "_all",
          visible: true
        } ],
         //select: true,
        buttons: [
          { extend: "create", editor: editor },
          { extend: "edit",   editor: editor },
          { extend: "remove", editor: editor }
        ]
      } );
    } );
  </script>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">Venue Administration</h1>

      <div class="ui container">
        <div class="ui basic segment">

          <div id="customForm">
            <fieldset class="Details">
              <legend>Details</legend>
              <editor-field name="Abbreviation"></editor-field>
              <editor-field name="Name"></editor-field>
              <editor-field name="CoreVenue"></editor-field>
            </fieldset>
            <fieldset class="Address">
              <legend>Address</legend>
              <editor-field name="Add1"></editor-field>
              <editor-field name="Add2"></editor-field>
              <editor-field name="Add3"></editor-field>
              <editor-field name="Add4"></editor-field>
              <editor-field name="Add5"></editor-field>
            </fieldset>
          </div>

          <table id="example" class="ui small sortable very compact single line table" >
            <thead>
              <tr>
                <!--<th></th>-->
                <th>Abbreviation</th>
                <th>Name</th>
                <th>Address</th>
                <th>Address2</th>
                <th>Address3</th>
                <th>Address4</th>
                <th>Address5</th>
                <th>CoreVenue</th>
              </tr>
            </thead>
          </table>

          <div  class="ui right very close rail">
            <div class="ui basic left aligned segment">
              <div data-hmtl="test" id="search" class="ui icon">
                <i id="BigFucker" class="ui search circular icon link"></i>
                <div class="ui custom popup top left transition hidden">
                  <table class="ui table">
                    <tbody>
                      <tr id="filter_member" data-column="1"> 
                        <td><input type="text" placeholder="Member" class="column_filter" id="col1_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_officer" data-column="2"> 
                        <td><input type="text" placeholder="Officer" class="column_filter" id="col2_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_contact" data-column="3"> 
                        <td><input type="text" placeholder="Contact Details" class="column_filter" id="col3_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_employer" data-column="4"> 
                        <td><input type="text" placeholder="Employer" class="column_filter" id="col4_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_workplace" data-column="5"> 
                        <td><input type="text" placeholder="Workplace" class="column_filter" id="col5_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_branch" data-column="6"> 
                        <td><input type="text" placeholder="Branch" class="column_filter" id="col6_filter" autocomplete="off"> 
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </body>

</html>
