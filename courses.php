<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Editor example - Basic initialisation</title>
    <style type="text/css" class="init"> </style>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    function filterColumn (i) {
      $('#tempo').DataTable().column(i).search(
        $('#col'+i+'_filter').val().split(",").join("|"),true,false
      ).draw();
      $('#tempn').DataTable().column(i).search(
        $('#col'+i+'_filter').val().split(",").join("|"),true,false
      ).draw();
    }

    function convertDate(i) {
      var m_names = new Array("January", "February", "March",
      "April", "May", "June", "July", "August", "September",
      "October", "November", "December");
      var m_names = new Array("Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul", "Aug", "Sep",
      "Oct", "Nov", "Dec");

      var d = new Date(i);
      var curr_date = d.getDate();
      var sup = "";
      if (curr_date == 1 || curr_date == 21 || curr_date ==31)
         {
         sup = "st";
         }
      else if (curr_date == 2 || curr_date == 22)
         {
         sup = "nd";
         }
      else if (curr_date == 3 || curr_date == 23)
         {
         sup = "rd";
         }
      else
         {
         sup = "th";
         }

      var curr_month = d.getMonth();
      var curr_year = d.getFullYear();
      var curr_year = curr_year.toString().substring(2);

      return curr_date + "<SUP>" + sup + "</SUP> " + m_names[curr_month] + " " + curr_year;
    }
    
    $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
	var min = $('#min').val();
	var max = $('#max').val();
	/* var max = $('#max').val();*/

	min=min.substring(6,10) + min.substring(3,5)+ min.substring(0,2);
	max=max.substring(6,10) + max.substring(3,5)+ max.substring(0,2);

	var StartDate=data[3].substring(0,4) + data[3].substring(5,7)+ data[3].substring(8,10);

	if ( min === "" && max === "" )
	{ return true; }
	else if ( min <= StartDate && max === "")
	{ return true; }
	else if (min <= StartDate && max >= StartDate)
	{ return true; }
	return false;
    }
    );

    $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
	var min = parseInt( $('#elmin').val(), 10 ); 
	var max = parseInt( $('#elmax').val(), 10 ); 
	var el = parseInt ( data[4], 10);

	if ( isNaN (min) && isNaN(max) )
	{ console.log("1");return true; }
	else if ( min <= el && isNaN(max) )
	{ console.log("2");return true; }
	else if (min <= el && max >= el)
	{ console.log("3");return true; }
	else if (isNaN(min) && max >= el)
	{ console.log("4");return true; }
	return false;
    }
    );

    $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
	var min = parseInt( $('#inmin').val(), 10 ); 
	var max = parseInt( $('#inmax').val(), 10 ); 
	var ina = parseInt ( data[5], 10);

	if ( isNaN (min) && isNaN(max) )
	{ console.log("1");return true; }
	else if ( min <= ina && isNaN(max) )
	{ console.log("2");return true; }
	else if (min <= ina && max >= ina)
	{ console.log("3");return true; }
	else if (isNaN(min) && max >= ina)
	{ console.log("4");return true; }
	return false;
    }
    );

    $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
	var min = parseInt( $('#remin').val(), 10 ); 
	var max = parseInt( $('#remax').val(), 10 ); 
	var re = parseInt ( data[6], 10);

	if ( isNaN (min) && isNaN(max) )
	{ console.log("1");return true; }
	else if ( min <= re && isNaN(max) )
	{ console.log("2");return true; }
	else if (min <= re && max >= re)
	{ console.log("3");return true; }
	else if (isNaN(min) && max >= re)
	{ console.log("4");return true; }
	return false;
    }
    );

    $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
	var min = parseInt( $('#comin').val(), 10 ); 
	var max = parseInt( $('#comax').val(), 10 ); 
	var co = parseInt ( data[7], 10);

	if ( isNaN (min) && isNaN(max) )
	{ console.log("1");return true; }
	else if ( min <= co && isNaN(max) )
	{ console.log("2");return true; }
	else if (min <= co && max >= co)
	{ console.log("3");return true; }
	else if (isNaN(min) && max >= co)
	{ console.log("4");return true; }
	return false;
    }
    );


    $(document).ready(function() {
	$('.column_filter').on('keyup change', function() { 
	  filterColumn( $(this).parent().attr('data-column'));
      } );
	$('#comin')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#comax')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#remin')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#remax')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#inmin')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#inmax')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#elmin')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#elmax')
	  .on('keyup change', function() { 
          table1.draw();
	  } );
	$('#min')
	  .on('keyup change', function() { 
          table1.draw();
	  } )
        .datepicker({ 
	    format: 'dd/mm/yyyy',
	  }); 
	$('#max')
	  .on('keyup change', function() { 
          table1.draw();
	  } )
        .datepicker({ 
	    format: 'dd/mm/yyyy',
	  }); 

      editor2 = new $.fn.dataTable.Editor( {
        fields: [ {
                label: "Filter Dates:",
                name:  "filter_dates",
                type:  'datetime',
                def:  'datetime',
            }
        ]
      } )

      $('#filter_courses')
      .dropdown({
          apiSettings: {
                // this url parses query server side and returns filtered results
          url: 'php/course_list.php',
          },
          saveRemoteData: false,
          filterRemoteData: true,
          fullTextSearch: 'exact'
      }) ;

      $('#filter_venues')
      .dropdown({
	  apiSettings: {
		  // this url parses query server side and returns filtered results
	  url: 'php/venue_list.php',
	  },
	  saveRemoteData: false,
	  filterRemoteData: true,
	  fullTextSearch: 'exact'
      }) ;

      $('.tabular.menu .item').tab();

      function loader(phpFile){
        var test= new Array({"label" : "a", "value" : "a"});
         
        test.splice(0,1);
        $.ajax({
          url: phpFile,
          async: false,
          dataType: 'json',
          success: function (json) {
              for(var a=0;a<json.length;a++){
                //if (phpFile='php/responseRecordAdd.php' ) alert(json[a]);
                //alert(json[a]['label']);
                //alert(json[a]['label']);
                if (json[a]['label'] !== undefined){
                  obj= { "label" : json[a]['label'], "value" : json[a]['value']};
                }else{
                  obj= { "label" : json[a][1], "value" : json[a][0]};
              }
                test.push(obj);
              }
            }
        });
        return test;
      }

      // This selects which elements are editable-->
      var editor = new $.fn.dataTable.Editor( {
        ajax: "php/courselist1.php",
        fields: [ {
            label: "Venue:",
            name: "VenueAbb",
            type: "select2",
            placeholder: "Choose venue",
            options: loader('php/venues.php')
          },{
            label: "Course:",
            name: "CourseAbb",
            type: "select2",
            placeholder: "Choose course",
            options: loader('php/courses.php')
          },{
            label: "Reference:",
            name: "Series",
          },{
            label: "Start Date:",
            type: "datetime",
            name: "StartDate"
          },{
            label: "_:",
            name: "InvitesDone",
            type: "hidden", 
            def: 0
          },{
            label: "_:",
            name: "MaterialsDone",
            type: "hidden", 
            def: 0
          },{
            label: "_:",
            name: "ExpensesDone",
            type: "hidden", 
            def: 0
          },{
            label: "_:",
            name: "Archived",
            type: "hidden", 
            def: 0
        }
        ]
      } );
      
      $('#FigBucker').on( 'click', function () {
        console.log("In Big Fucker");
	  $('#customSearchForm').modal( {
	    inverted: true,
	    dimmerSettings: { opacity: 0.3, useCSS   : true }
	  })
	  .modal('show');
      }) ;
	
      // Form to create course
      $('#FigBucker2').on( 'click', function () {
        console.log("In Big Fucker");
        editor.buttons( {
          label: "Save",
          fn: function () { 
            this.submit();
            setTimeout(function(){table1.ajax.reload(null , false)}, 0750);
          }
        } )
        //.on( 'submitComplete', function (r, json,data) {alert('New row added')})
        .on( 'submitComplete', function (r, json,data) {
          setTimeout(function(){table1.ajax.reload(null , false)}, 0750);
        })
        .create();
      } );

      // DataTable definition
      var table1 =	$('#tempn').DataTable( {
        autoWidth: false,
        order: [[3, 'asc']], //sort by date
        select: 'single',
        //dom:'r<ti><T>S',
        dom:'<ti>p<T>S',
        ajax: { 
          url: "php/courselist.php?archived=0",
        },
        columns: [
          { data: null,
              width : '40%',
              render : function (data, type, row) {
                if (type === 'sort'){ 
                  return data.TP.StartDate;
                } else {
                  return "<div style='white-space:normal;'>"
                         + "<a href= ./?page=course&courseID=" + data.TP.ID  + ">"
                         + data.Courses.Title + "</a><br>(<small>"
                         + data.Venues.Name + " - "
                         + convertDate(data.TP.StartDate) + "</small>)"
                         + "</div>";
                }
              }
          }, { 
            data: 'TP.CourseAbb',
            visible: false
          }, { 
            data: 'TP.VenueAbb',
            visible: false
          }, { 
            data: 'TP.StartDate',
            visible: false
          }, { 
            data: 'ET.EligibleCount',
            width : '15%',
            defaultContent: "<i>None</i>"
          }, { 
            data: 'CIC.InviteCount',
            width : '15%',
            "defaultContent": 0
          }, { 
            data: 'CIC.ResponseCount',
            width : '15%',
            "defaultContent": 0
          }, { 
            data: 'CIC.ConfirmCount',
            width : '15%',
            "defaultContent": 0
          }
        ],
        buttons: [
            { extend: "create", editor: editor },
            { extend: "create", editor: editor },
        ]
      } );
      //
      //// DataTable definition
      var table2 =	$('#tempo').DataTable( {
        autoWidth: false,
        order: [[3, 'des']], //sort by date
        select: true,
        dom:'<ti>p<T>S',
        ajax: { 
          url: "php/courselist.php?archived=1",
        },
        columns: [
          { data: null,
              width : '40%',
              render : function (data, type, row) {
                if (type === 'sort'){ 
                  return data.TP.StartDate;
                } else {
                  return "<div style='white-space:normal;'>"
                         + "<a href= ./?page=course&courseID=" + data.TP.ID  + ">"
                         + data.Courses.Title + "</a><br>(<small>"
                         + data.Venues.Name + " - "
                         + convertDate(data.TP.StartDate) + "</small>)"
                         + "</div>";
                }
              }
          }, { 
            data: 'TP.CourseAbb',
            visible: false
          }, { 
            data: 'TP.VenueAbb',
            visible: false
          }, { 
            data: 'TP.StartDate',
            visible: false
        //{ data: 'ET.EligibleCount'},
          }, { 
            data: 'CIC.InviteCount',
            width : '20%',
            "defaultContent": 0
          }, { 
            data: 'CIC.ResponseCount',
            width : '20%',
            "defaultContent": 0
          }, { 
            data: 'CAC.AttendanceCount',
            width : '20%',
            "defaultContent": 0
          }
        ],
      } );
    });
  </script>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">Course Schedule</h1>
      <!--p>This is a list of upcoming courses that will run<p-->

      <div class="ui centered grid basic segment">
        <div id="btnPDF" class="one wide column">
          <div id="search" class="ui circular basic icon button">
            <i id="FigBucker" class="ui search icon"></i>
          </div>
          <?php
            if  (!empty($_SESSION['user']) && $_SESSION['level']<=3){
              print '
                <div id="add" class="ui circular basic icon button">
                  <i id="FigBucker2" class="ui calendar plus icon"></i>
                </div>';
            }
          ?>
        </div>
        <div class="fifteen wide column">
          <div class="ui top attached tabular menu">
            <a class="active item" data-tab="upcoming">Upcoming</a>
            <a class="item" data-tab="previous">Previous</a>
          </div>
          <div class="ui bottom attached active tab basic segment" data-tab="upcoming">
            <?php // include_once "coursestable.html"; ?>
            <!--table style="visibility: hidden;" id="tempn" class="ui compact selectable striped called table raised segment" -->
            <!-- DataTable -->
            <table id="tempn" class="ui small sortable very compact single line center aligned table" >
              <thead>
                <tr>
                  <th>Course</th>
                  <th>Course</th>
                  <th>Venue</th>
                  <th>Start Date</th>
                  <th>Eligible</th>
                  <th>Invited</th>
                  <th>Responded</th>
                  <th>Confirmed</th>
                  <!--<th>Tutor Allocated</th>-->
                  <!--<th>Materials Sent</th>-->
                </tr>
              </thead>
            </table>
          </div>
          <div class="ui bottom attached tab basic segment" data-tab="previous">
            <table id="tempo" class="ui small sortable very compact single line center aligned table" >
              <thead>
                <tr>
                  <th>Course</th>
                  <th>Course</th>
                  <th>Venue</th>
                  <th>Start Date</th>
                  <th>Invited</th>
                  <th>Responded</th>
                  <th>Attended</th>
                  <!--<th>Tutor Allocated</th>-->
                  <!--<th>Materials Sent</th>-->
                </tr>
              </thead>
            </table>
          </div>
          <div  class="ui right very close rail">
             <div class="ui basic left aligned segment">
               <div data-hmtl="test" id="search" class="ui icon">
               <!--i id="BigFucker" class="ui plus circular icon link"></i-->
               </div>
             </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ui ">
	<div id="customSearchForm" class="ui mini modal">
	  <div class="ui center aligned basic segment">
	    <h4 class="ui center aligned  header">Record Search</h4>
	    <div id="cunt" class="ui center aligned  form">
		<div id="filter_venues" data-column="2" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" class="column_filter" id="col2_filter" > 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Venues</div>
		</div>
		<div id="filter_courses" data-column="1" class="ui fluid multiple search selection dropdown">
		  <input type="hidden" class="column_filter" id="col1_filter" > 
		  <i class="dropdown icon">   </i>
		  <div class="default text">Search Courses</div>
		</div>
		<div id="filter_datesa"  class="ui fluid input ">
              <input id="min" type="text" placeholder="Dates From" class="form-control"></input>
		</div>
		<div id="filter_datesb"  class="ui fluid input ">
              <input id="max" type="text" placeholder="Dates To" class="form-control"></input>
		</div>
		<div id="filter_eligiblea"  class="ui fluid input ">
              <input id="elmin" type="text" placeholder="Eligible Min" class="form-control"></input>
		</div>
		<div id="filter_eligibleb"  class="ui fluid input ">
              <input id="elmax" type="text" placeholder="Eligible Max" class="form-control"></input>
		</div>
		<div id="filter_inviteda"  class="ui fluid input ">
              <input id="inmin" type="text" placeholder="Invited Min" class="form-control"></input>
		</div>
		<div id="filter_invitedb"  class="ui fluid input ">
              <input id="inmax" type="text" placeholder="Invited Max" class="form-control"></input>
		</div>
		<div id="filter_Respondeda"  class="ui fluid input ">
              <input id="remin" type="text" placeholder="Responded Min" class="form-control"></input>
		</div>
		<div id="filter_Respondedb"  class="ui fluid input ">
              <input id="remax" type="text" placeholder="Responded Max" class="form-control"></input>
		</div>
		<div id="filter_Confirmeda"  class="ui fluid input ">
              <input id="comin" type="text" placeholder="Confirmed Min" class="form-control"></input>
		</div>
		<div id="filter_Confirmedb"  class="ui fluid input ">
              <input id="comax" type="text" placeholder="Confirmed Max" class="form-control"></input>
		</div>
		<div id="clearbutton"  class="ui labelled fluid button">Clear 
		</div>
	    </div>
	  </div>
	</div>
    </div>
  </body>
<html>
