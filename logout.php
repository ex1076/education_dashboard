<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Landing Page</title>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    $(document).ready(function() {
      $('.ui.form').form({
        on: 'submit',
        fields : {
          user: {
            identifier : 'user',
            rules: [
              {
                type : 'regExp[/.*@gmb.org.uk$/]',
                prompt : 'Make sure you use a valid GMB email address'
              }
            ]
          },
          password: {
            identifier : 'password',
            rules: [
              {
                type : 'empty',
                prompt : 'Please enter a password'
              }
            ]
          }
       }
      }) ;
    });
  </script>

  <body>
      <div class="ui basic content">
        <div class="ui header">
          <?php 
            session_start();
            print $_SESSION['firstname'] . " "  . $_SESSION['surname']
          ?>
        </div>
        <form class="ui form" name="logout" action="/education/?page=logoutsuccess" method="post">
          <div class="ui submit button">Logout</div>
          <div class="ui error message"></div>
        </form>
      </div>
  </body>
</html>
