/*
This is a comment
*/
DELIMITER $$
CREATE  FUNCTION `GetPrerequisite`(`Course` TINYTEXT) RETURNS char(4) CHARSET latin1
BEGIN
  declare output char(4);
  SELECT Prerequisite INTO output From Courses
  WHERE Abbreviation=Course;

  IF output IS NULL THEN SET output=''; END IF;

  RETURN output;
END$$
DELIMITER ;
