CREATE  FUNCTION `GetREsponseCount`(`s` TINYTEXT) RETURNS int(11)
    NO SQL
BEGIN
RETURN(
select count(*) from Responses as R 
left join Invitations as I 
ON R.InviteID=I.ID 
where I.TrainingID=s
    );
END	utf8	utf8_general_ci	latin1_swedish_ci
