CREATE  FUNCTION `jfhGetEligibleCount`(`jfhCourse` TINYTEXT, `jfhVenue` TINYTEXT) RETURNS int(11)
    NO SQL
BEGIN


CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemAtVen (member varchar(7));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemCompPrereq (member varchar(7));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemNotCompCourse (member varchar(7));


#Get Members By Preferred Venue#
IF jfhVenue = '' THEN
\tREPLACE INTO tmpLstMemAtVen (
      \t select MD.Member from MembershipDetails as MD
\t \t right Join PreferredVenues as PV
\t \t ON MD.Member=PV.Member
 \t \t WHERE MD.Member IS NOT NULL);
ELSE
\tREPLACE INTO tmpLstMemAtVen (
      \t select MD.Member from MembershipDetails as MD
\t \t right Join PreferredVenues as PV
\t \t ON MD.Member=PV.Member
 \t \t WHERE PV.Venue=jfhVenue 
 \t \t AND MD.Member IS NOT NULL);
END IF;

##Get Members Who havent completedcourse
#REPLACE INTO tmpLstMemNotCompCourse(
#SELECT DISTINCT MD.Member from MembershipDetails as MD LEFT JOIN (
#SELECT DISTINCT Member 
#FROM Attendance As A
#JOIN TrainingProgramme AS TP 
#ON A.TrainingID=TP.ID
#WHERE CourseAbb = jfhCourse) as DONE
#ON MD.Member=DONE.Member
#WHERE DONE.Member IS NULL);

#Get members who have completed prerequisite
IF GetPrerequisite(jfhCourse) = '' THEN

\tREPLACE INTO tmpLstMemCompPrereq(
      SELECT LNG.Member FROM
\t\t(select Member, CourseAbb from Attendance as A
\t\t left join TrainingProgramme as TP
\t \t on A.TrainingID=TP.ID
\t\t ) AS LNG
\t left join
\t\t(select Member, CourseAbb from Attendance as A
\t\t right join TrainingProgramme as TP
\t\t on A.TrainingID=TP.ID
\t\t where CourseAbb='jfhCourse') AS POOP
\t ON LNG.Member=POOP.Member
\t WHERE POOP.MeMber is null);


ELSE

\tREPLACE INTO tmpLstMemCompPrereq(
      SELECT LNG.Member FROM
\t\t(select Member, CourseAbb from Attendance as A
\t\t left join TrainingProgramme as TP
\t \t on A.TrainingID=TP.ID
\t\t where CourseAbb=GetPrerequisite(jfhCourse)) AS LNG
\t left join
\t\t(select Member, CourseAbb from Attendance as A
\t\t right join TrainingProgramme as TP
\t\t on A.TrainingID=TP.ID
\t\t where CourseAbb='jfhCourse') AS POOP
\t ON LNG.Member=POOP.Member
\t WHERE POOP.MeMber is null);

END IF;

SELECT count(*) INTO @jfhCNTR FROM tmpLstMemAtVen as a INNER JOIN tmpLstMemCompPrereq as b on a.Member=b.Member;
#SELECT COUNT(*) INTO @jfhCNTR  FROM member_pivot as mp JOIN(
#SELECT DISTINCT a.member FROM tmpLstMemAtVen as a inner JOIN (
#SELECT DISTINCT b.member FROM tmpLstMemCompPrereq as b INNER JOIN tmpLstMemNotCompCourse as c ON b.member=c.member\n#) as d on a.member=d.member order by a.member\n#) as cunt on mp.member=cunt.member;\nDROP TEMPORARY table tmpLstMemAtVen;\nDROP TEMPORARY table tmpLstMemCompPrereq;\nDROP TEMPORARY table tmpLstMemNotCompCourse;\n\nRETURN @jfhCNTR;\nEND	utf8	utf8_general_ci	latin1_swedish_ci
