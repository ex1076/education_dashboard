CREATE  FUNCTION `GetEligibleMembers`(`jfhCOURSE` TINYTEXT, `jfhVENUE` TINYTEXT) RETURNS int(11)
    NO SQL
BEGIN


CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemAtVen (member varchar(7));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemCompPrereq (member varchar(7));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemNotCompCourse (member varchar(7));


#Get Members By Preferred Venue#
IF jfhVenue = '' THEN
\tREPLACE INTO tmpLstMemAtVen (
\tSELECT DISTINCT Member 
\tFROM PreferredVenues);
ELSE
\tREPLACE INTO tmpLstMemAtVen (
\tSELECT DISTINCT Member
\tFROM PreferredVenues 
\tWHERE Venue=jfhVenue);
END IF;

#Get Members Who havent completedcourse#
REPLACE INTO tmpLstMemNotCompCourse(
SELECT DISTINCT MD.Member from MembershipDetails as MD LEFT JOIN (
SELECT DISTINCT Member 
FROM Attendance As A
JOIN TrainingProgramme AS TP 
ON A.TrainingID=TP.ID
WHERE CourseAbb = jfhCourse) as DONE
ON MD.Member=DONE.Member
WHERE DONE.Member IS NULL);

#Get members who have completed prerequisite
IF GetPrerequisite(jfhCourse) = '' THEN

\tREPLACE INTO tmpLstMemCompPrereq(
      SELECT DISTINCT Member
      FROM Attendance As A
    JOIN TrainingProgramme AS TP 
    ON A.TrainingID=TP.ID);


ELSE

\tREPLACE INTO tmpLstMemCompPrereq(
      SELECT DISTINCT Member
      FROM Attendance As A
    JOIN TrainingProgramme AS TP
    ON A.TrainingID=TP.ID
    WHERE CourseAbb=
    (SELECT GetPrerequisite(jfhCourse)));

END IF;

SELECT COUNT(*) INTO @jfhCNTR  FROM member_pivot as mp JOIN(
SELECT DISTINCT a.member FROM tmpLstMemAtVen as a inner JOIN (
SELECT DISTINCT b.member FROM tmpLstMemCompPrereq as b INNER JOIN tmpLstMemNotCompCourse as c ON b.member=c.member
) as d on a.member=d.member order by a.member
) as cunt on mp.member=cunt.member;
DROP TEMPORARY table tmpLstMemAtVen;
DROP TEMPORARY table tmpLstMemCompPrereq;
DROP TEMPORARY table tmpLstMemNotCompCourse;

RETURN @jfhCNTR;
END	utf8	utf8_general_ci	latin1_swedish_ci
