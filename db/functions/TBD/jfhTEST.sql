CREATE  FUNCTION `jfhTEST`(`jfhCourse` TINYTEXT, `jfhVenue` TINYTEXT) RETURNS int(11)
    NO SQL
BEGIN

#Get Members By Preferred Venue#
SELECT count(*) INTO @jfhCNTR FROM 
		(
      	 select MD.Member from MembershipDetails as MD
	 	 inner Join PreferredVenues as PV
	 	 ON MD.Member=PV.Member
 	 	 WHERE PV.Venue=jfhVenue 
 	 	 AND MD.Member IS NOT NULL) as PrefV
INNER JOIN
#Get members who have completed prerequisite
	  (
      SELECT LNG.Member FROM
		(select Member, CourseAbb from Attendance as A
		 inner join TrainingProgramme as TP
	 	 on A.TrainingID=TP.ID
		 where CourseAbb=GetPrerequisite(jfhCourse)) AS LNG
	 left join
		(select Member, CourseAbb from Attendance as A
		 inner join TrainingProgramme as TP
		 on A.TrainingID=TP.ID
		 where CourseAbb='jfhCourse') AS POOP
	 ON LNG.Member=POOP.Member
	 WHERE POOP.MeMber is null)
as CourseM on PrefV.Member=CourseM.Member;


RETURN @jfhCNTR;
END	utf8	utf8_general_ci	latin1_swedish_ci
