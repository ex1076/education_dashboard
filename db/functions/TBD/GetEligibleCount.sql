CREATE  FUNCTION `GetEligibleCount`(`s` CHAR(4), `t` CHAR(3)) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
BEGIN

IF t IS NULL OR t='' THEN
CALL GetEligibleByCourse(s,@EligibleCnt);
ELSE
CALL GetEligibleByCourseVenue(s,t,@EligibleCnt);
END IF;

RETURN (SELECT @EligibleCnt);
END	utf8	utf8_general_ci	latin1_swedish_ci
