/*
This is a comment
*/
DELIMITER $$
CREATE  FUNCTION `EligibilityCounter`(`jfhCourse` TINYTEXT, `jfhVenue` TINYTEXT, `jfhID` INTEGER) RETURNS int(11)
    NO SQL
    DETERMINISTIC
BEGIN

select count(distinct(AM.Member)) into @CNTR from 
AllMembers AS AM
left join PreferredVenues AS PV on AM.Member=PV.Member
left join InvitationsConCat as ICC on AM.Member=ICC.Member
left join
(
  select 
  Member, 
  group_concat(distinct CourseAbb) as courses 
  from 
  ( 
    select 
    ATT.*, 
    TP.CourseAbb 
    from 
    Attendance as ATT 
    left join 
    TrainingProgramme AS TP 
    on ATT.TrainingID=TP.ID 
    order by Member
  ) as temp 
  group by Member
) as ATT 
ON ATT.Member=AM.Member
left join 
LiveInvites as LI
ON LI.Member=AM.Member
where 
ATT.courses not like CONCAT('%', jfhCourse, '%')
AND ICC.shit NOT LIKE CONCAT('%, ', jfhID, '.%')
AND LI.Member is null
AND PV.Venue = jfhVenue
/*don't include previous preferences when*/
AND PV.Archived = 0
AND
if(
  GetPrerequisite(jfhCourse)!='' , 
  ATT.courses like CONCAT('%', (SELECT GetPrerequisite(jfhCourse)), '%') ,
  1>0
)
;

RETURN @CNTR;

END$$
DELIMITER ;
