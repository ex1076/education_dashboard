/*
This is a comment
*/
DELIMITER $$
CREATE  FUNCTION `SeriesCounter`(`jfhID` INT) RETURNS int(11)
    NO SQL
    DETERMINISTIC
BEGIN

set @num=0;

select row_number into @cntr from 
  (SELECT 
    ID,CourseAbb,VenueAbb, @num := if(@Year = Year, @num + 1, 1) as row_number, @Year := Year as dummy 
  FROM  
  (Select 
    *, Year(StartDate) Year 
  from 
    TrainingProgramme
  ) as r 
  ORDER BY  
    Year, ID
  ) TEMP 
where ID=jfhID;


RETURN @CNTR;

END$$
DELIMITER ;
