CREATE TABLE `SchemaChangeLog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MajorReleaseNumber` varchar(2) NOT NULL,
  `MinorReleaseNumber` varchar(2) NOT NULL,
  `PointReleaseNumber` varchar(4) NOT NULL,
  `ScriptName` varchar(50) NOT NULL,
  `DateApplied` datetime NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1
