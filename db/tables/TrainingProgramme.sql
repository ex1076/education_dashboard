CREATE TABLE `TrainingProgramme` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Series` varchar(4) NOT NULL,
  `CourseAbb` varchar(4) NOT NULL,
  `VenueAbb` varchar(3) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `InvitesDone` tinyint(1) NOT NULL,
  `MaterialsDone` tinyint(1) NOT NULL,
  `ExpensesDone` tinyint(1) NOT NULL,
  `Archived` int(11) NOT NULL,
  `Notes` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CourseVenue` (`CourseAbb`,`VenueAbb`)
)
ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=latin1
