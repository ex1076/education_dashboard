CREATE TABLE `PreferredVenues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Member` varchar(7) NOT NULL,
  `Venue` varchar(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Member` (`Member`,`Venue`),
  UNIQUE KEY `Member_2` (`Member`,`Venue`),
  UNIQUE KEY `MemberVenue` (`Member`,`Venue`)
)
ENGINE=InnoDB AUTO_INCREMENT=1524 DEFAULT CHARSET=latin1
