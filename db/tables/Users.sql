CREATE TABLE `Users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailAddress` text NOT NULL,
  `FirstName` text NOT NULL,
  `Surname` text NOT NULL,
  `Password` text NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1
