CREATE TABLE `Responses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `InviteID` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_Fri 01 Apr 2016 11:05:53 AM BST ON UPDATE CURRENT_TIMESTAMP,
  `Medium` varchar(1) NOT NULL,
  `Note` mediumtext,
  `Response` tinytext NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=1951 DEFAULT CHARSET=latin1
