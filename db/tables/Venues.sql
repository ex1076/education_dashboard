CREATE TABLE `Venues` (
  `Abbreviation` varchar(4) NOT NULL,
  `Name` tinytext NOT NULL,
  `Add1` tinytext,
  `Add2` tinytext,
  `Add3` tinytext,
  `Add4` tinytext,
  `Add5` tinytext,
  `Post Code` varchar(9) DEFAULT NULL,
  `Phone Number` varchar(16) DEFAULT NULL,
  `CoreVenue` tinyint(1) NOT NULL,
  PRIMARY KEY (`Abbreviation`),
  UNIQUE KEY `Abbreviation` (`Abbreviation`),
  KEY `Abbreviation_2` (`Abbreviation`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1
