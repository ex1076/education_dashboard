CREATE TABLE `Invitations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrainingID` int(11) NOT NULL,
  `Member` varchar(7) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Medium` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `MemTraining` (`Member`,`TrainingID`)
)
ENGINE=InnoDB AUTO_INCREMENT=3141 DEFAULT CHARSET=latin1
