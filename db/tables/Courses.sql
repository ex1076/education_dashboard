CREATE TABLE `Courses` (
  `Abbreviation` varchar(4) NOT NULL,
  `Title` tinytext NOT NULL,
  `Prerequisite` varchar(4) NOT NULL,
  `Duration` int(11) DEFAULT NULL,
  `CoreCourseOrder` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`Abbreviation`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1
