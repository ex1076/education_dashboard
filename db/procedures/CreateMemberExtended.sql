/*
This is a comment
*/
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateMemberExtended`()
    NO SQL
BEGIN

SET @sql = NULL;
SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(
      'case when CourseAbb= "',
      Abbreviation,
      '" then CourseAbb end AS ',
      replace(Abbreviation, ' ', '')
    )
  ) INTO @sql
from Courses
WHERE CoreCourseOrder > 0
Order By CoreCourseOrder;

SET @sql = CONCAT('
CREATE OR REPLACE VIEW member_extended as (                  
SELECT Member, ', @sql, ' 
FROM AttMemberCourse
);');
                  
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

End$$	
DELIMITER ;
