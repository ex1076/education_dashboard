Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
GetEligibleDynamic		CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEligibleDynamic`(IN `s` VARCHAR(4), OUT `EligibleCount` INT)
    NO SQL
BEGIN
CREATE TEMPORARY TABLE temp_table (member varchar(7));

SET @sql = NULL;
SET @poop = NULL;

SET @sql = CONCAT('
SELECT MP.Member FROM `member_pivot` AS MP
Left JOIN 
MembershipDetails AS MD
ON MD.Member =MP.Member');

SET @sql = CONCAT(@sql, 
' WHERE MP.', s, '=0');

IF GetPrerequisite(s) != "" THEN
SET @sql = CONCAT(@sql,
' AND MP.', GetPrerequisite(s), '>0');
END IF;

SET @poop = CONCAT(@sql,
' AND (
MD.Activist LIKE "%SHS%" OR
MD.Activist LIKE "%HSR%")');

SET @sql= CONCAT('
INSERT INTO temp_table ',@sql);


PREPARE stmt FROM @sql;
EXECUTE stmt;

SET EligibleCount = (SELECT COUNT(*) FROM temp_table);
DEALLOCATE PREPARE stmt;

END	utf8	utf8_general_ci	latin1_swedish_ci
