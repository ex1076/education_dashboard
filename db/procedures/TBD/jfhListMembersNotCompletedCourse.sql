Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
jfhListMembersNotCompletedCourse		CREATE DEFINER=`root`@`localhost` PROCEDURE `jfhListMembersNotCompletedCourse`(IN `s` TINYTEXT)
    NO SQL
BEGIN

       SELECT Member, CourseAbb FROM Attendance As A
         JOIN TrainingProgramme AS TP 
           ON A.TrainingID=TP.ID
        WHERE CourseAbb != s;

END	utf8	utf8_general_ci	latin1_swedish_ci
