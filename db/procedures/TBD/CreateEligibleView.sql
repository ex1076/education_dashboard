Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
CreateEligibleView		CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateEligibleView`()
    NO SQL
BEGIN

SET @sql = NULL;
SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(
      'GetEligibleCount(Abbreviation,"',
        Abbreviation,
       '") As ', Abbreviation
    ))
   INTO @sql
from Venues
WHERE CoreVenue=1;

SET @sql = CONCAT('
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW EligibleMembersByCourseVenue as (                  
SELECT Abbreviation, ', @sql, '  
FROM Courses 
WHERE CoreCourseOrder>0 
ORDER BY CoreCourseOrder
);');

                  
PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = CONCAT('
CREATE OR REPLACE VIEW EligibleMembersByCourse as (                  
SELECT Abbreviation, 
GetEligibleCount(Abbreviation,"") AS Waiting
FROM Courses 
WHERE CoreCourseOrder>0 
ORDER BY CoreCourseOrder
);');

                  
PREPARE stmt FROM @sql;
EXECUTE stmt;

DEALLOCATE PREPARE stmt;

End	
utf8	utf8_general_ci	latin1_swedish_ci
