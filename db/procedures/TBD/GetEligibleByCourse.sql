Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
GetEligibleByCourse		CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEligibleByCourse`(IN `s` CHAR(4), OUT `EligibleCount` INT)
    NO SQL
BEGIN
CREATE TEMPORARY TABLE tbl1 (member varchar(7));
CREATE TEMPORARY TABLE tbl2 (member varchar(7));
CREATE TEMPORARY TABLE tmp_out (member varchar(7));

INSERT INTO tbl1
 SELECT Member FROM AttMemberCourse
 WHERE CourseAbb=s
 GROUP BY Member;

CASE
  WHEN GetPrerequisite(s) =""
THEN
  INSERT INTO tbl2
  SELECT Member FROM MembershipDetails
  GROUP BY Member;
ELSE
  INSERT INTO tbl2
  SELECT Member FROM AttMemberCourse
  WHERE CourseAbb=GetPrerequisite(s)
  GROUP BY Member;
END CASE;

INSERT INTO tmp_out
SELECT MD.Member FROM MembershipDetails as MD
LEFT JOIN  tbl1 ON MD.Member=tbl1.Member
INNER JOIN tbl2 ON MD.Member=tbl2.Member
WHERE tbl1.Member IS NULL
AND MD.Member = tbl2.Member
AND (MD.Activist LIKE "%HSR%" OR MD.Activist LIKE "%SHS%")
GROUP BY MD.Member;

#SET EligibleCount=(SELECT COUNT(*) FROM tmp_out);

DROP TEMPORARY TABLE tbl1;
DROP TEMPORARY TABLE tbl2;
DROP TEMPORARY TABLE tmp_out;

END	utf8	utf8_general_ci	latin1_swedish_ci
