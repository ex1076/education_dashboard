Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
GetCoreCourses		CREATE DEFINER=`root`@`localhost` PROCEDURE `GetCoreCourses`()
    NO SQL
BEGIN
   SELECT Abbreviation FROM Courses
   WHERE CoreCourseOrder >0;
   END	utf8	utf8_general_ci	latin1_swedish_ci
