Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
GetEligibleMembers		CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEligibleMembers`(IN `VENUE` TINYTEXT, IN `COURSE` TINYTEXT)
    NO SQL
SELECT * FROM PreferredVenues AS PV 
JOIN 
      (
       SELECT Member, CourseAbb FROM Attendance As A
         JOIN TrainingProgramme AS TP 
           ON A.TrainingID=TP.ID 
        WHERE CourseAbb=
            (SELECT GetPrerequisite(COURSE))
      )
      AS Q 
on PV.Member=Q.Member 
Where PV.Venue=VENUE	utf8	utf8_general_ci	latin1_swedish_ci
