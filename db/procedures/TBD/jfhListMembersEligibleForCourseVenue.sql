Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
jfhListMembersEligibleForCourseVenue		CREATE DEFINER=`root`@`localhost` PROCEDURE `jfhListMembersEligibleForCourseVenue`(IN `jfhCOURSE` TINYTEXT, IN `jfhVENUE` TINYTEXT)
    NO SQL
BEGIN

CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemAtVen (member varchar(7));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemCompPrereq (member varchar(7));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpLstMemNotCompCourse (member varchar(7));


#Get Members By Preferred Venue#
IF jfhVenue = '' THEN
\tREPLACE INTO tmpLstMemAtVen (
\tSELECT DISTINCT Member 
\tFROM PreferredVenues);
ELSE
\tREPLACE INTO tmpLstMemAtVen (
\tSELECT DISTINCT Member
\tFROM PreferredVenues 
\tWHERE Venue=jfhVenue);
END IF;

#Get Members Who haven't completedcourse#
REPLACE INTO tmpLstMemNotCompCourse(
SELECT DISTINCT MD.Member from MembershipDetails as MD LEFT JOIN (
SELECT DISTINCT Member 
FROM Attendance As A
JOIN TrainingProgramme AS TP 
ON A.TrainingID=TP.ID
WHERE CourseAbb = jfhCourse) as DONE
ON MD.Member=DONE.Member
WHERE DONE.Member IS NULL);

#Get members who have completed prerequisite
IF GetPrerequisite(jfhCourse) = '' THEN

\tREPLACE INTO tmpLstMemCompPrereq(
      SELECT DISTINCT Member
      FROM Attendance As A
    JOIN TrainingProgramme AS TP 
    ON A.TrainingID=TP.ID);


ELSE

\tREPLACE INTO tmpLstMemCompPrereq(
      SELECT DISTINCT Member
      FROM Attendance As A
    JOIN TrainingProgramme AS TP
    ON A.TrainingID=TP.ID
    WHERE CourseAbb=
    (SELECT GetPrerequisite(jfhCourse)));
\nEND IF;\n\nSELECT * FROM member_pivot as mp JOIN(\nSELECT DISTINCT a.member FROM tmpLstMemAtVen as a inner JOIN (\nSELECT DISTINCT b.member FROM tmpLstMemCompPrereq as b INNER JOIN tmpLstMemNotCompCourse as c ON b.member=c.member\n) as d on a.member=d.member order by a.member\n) as cunt on mp.member=cunt.member;\nDROP table tmpLstMemAtVen;\nDROP table tmpLstMemCompPrereq;\nDROP table tmpLstMemNotCompCourse;\n\n\nEND	utf8	utf8_general_ci	latin1_swedish_ci
