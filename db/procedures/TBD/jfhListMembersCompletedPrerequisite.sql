Procedure	sql_mode	Create Procedure	character_set_client	collation_connection	Database Collation
jfhListMembersCompletedPrerequisite		CREATE DEFINER=`root`@`localhost` PROCEDURE `jfhListMembersCompletedPrerequisite`(IN `s` TINYTEXT)
    NO SQL
BEGIN

IF GetPrerequisite(s) = '' THEN

       SELECT Member, CourseAbb FROM Attendance As A
         JOIN TrainingProgramme AS TP 
           ON A.TrainingID=TP.ID;


ELSE

       SELECT Member, CourseAbb FROM Attendance As A
         JOIN TrainingProgramme AS TP 
           ON A.TrainingID=TP.ID 
        WHERE CourseAbb=
            (SELECT GetPrerequisite(s));

END IF;

END	utf8	utf8_general_ci	latin1_swedish_ci
