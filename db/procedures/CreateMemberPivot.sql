/*
This is a comment
*/
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateMemberPivot`()
    NO SQL
BEGIN

SET @sql = NULL;
SELECT
  GROUP_CONCAT(DISTINCT
    CONCAT(
      'COUNT(',
      Abbreviation,
      ') AS ',
      replace(Abbreviation, ' ', '')
    )
  ) INTO @sql
from Courses
WHERE CoreCourseOrder > 0
Order By CoreCourseOrder;

SET @sql = CONCAT('
CREATE OR REPLACE VIEW member_pivot as (                  
SELECT Member, ', @sql, ' 
FROM member_extended
GROUP BY Member                
);');
                  
PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END$$
DELIMITER ;
