CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `CourseInviteCount` AS
(
  select 
  * 
  from 
  (
    select 
    TP.ID, count(DISTINCT INV.Member) AS InviteCount
    from 
    TrainingProgramme TP 
    left join 
    Invitations INV 
    on TP.ID=INV.TrainingID 
    group by TP.ID
  ) A 
  natural join 
  (
    select 
    TP.ID, 
    count(DISTINCT R.InviteID) AS ResponseCount,
    sum(if(R.Response =1,1,0 )) AS ConfirmCount
    from 
    TrainingProgramme TP 
    left join 
    (
      select
      Invitations.TrainingID, T.*
      from
      Invitations
      right join
      (
        select
        Responses.*
        from
        Responses
        right join
        (
          select 
          Max(Responses.ID)  as ID
          from 
          (
            select         
            InviteID, Max(Responses.Date) as Date         
            from         
            Responses         
            group by inviteID
          ) P 
          right join 
          Responses 
          on P.Date=Responses.Date AND P.InviteID=Responses.InviteID 
          where P.InviteID Is not null 
          group by Responses.InviteID
        ) Q
        on Responses.ID=Q.ID
      ) T
      on Invitations.ID=T.InviteID
    ) R 
    on TP.ID=R.TrainingID  
    group by TP.ID
  ) B 
)




