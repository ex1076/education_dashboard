CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jfhTrainingPogrom` AS
select
`TP`.`ID` AS `ID`,
`TP`.`Series` AS `Series`,
`TP`.`CourseAbb` AS `CourseAbb`,
`TP`.`VenueAbb` AS `VenueAbb`,
`TP`.`StartDate` AS `StartDate`,
`TP`.`InvitesDone` AS `InvitesDone`,
`TP`.`MaterialsDone` AS `MaterialsDone`,
`TP`.`ExpensesDone` AS `ExpensesDone`,
`TP`.`Archived` AS `Archived`,
`TP`.`Notes` AS `Notes`,
`jfhGetEligibleCount`(`TP`.`CourseAbb`, `TP`.`VenueAbb`) AS `Eligible`,
`GetInvitedCount`(`TP`.`ID`) AS `Invited`,
`GetREsponseCount`(`TP`.`ID`) AS `Responded`,
`GetConfirmedCount`(`TP`.`ID`) AS `Confirmed`
from
`TrainingProgramme` `TP` 
where (`TP`.`StartDate` > '2015-10-01') 
group by
`TP`.`CourseAbb`, `TP`.`VenueAbb` 
order by
`TP`.`StartDate`
utf8	utf8_general_ci
