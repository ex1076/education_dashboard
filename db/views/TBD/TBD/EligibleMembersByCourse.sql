CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `EligibleMembersByCourse` AS
(select
`Courses`.`Abbreviation` AS `Abbreviation`,
`GetEligibleCount`(`Courses`.`Abbreviation`, '') AS `Waiting`
from
`Courses` 
where (`Courses`.`CoreCourseOrder` > 0) 
order by `Courses`.`CoreCourseOrder`)
utf8	utf8_general_ci
