CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `member_posts` AS
(select
`MD`.`Member` AS `Member`,
substring_index(substring_index(`MD`.`Activist`, '?', `numbers`.`n`), '?', -(1)) AS `Activist`
from
(`numbers` join `MembershipDetails` `MD`
on
((char_length(`MD`.`Activist`) - char_length(replace(`MD`.`Activist`, '?', ''))) >= (`numbers`.`n` - 1)))) 
order by
`MD`.`Member`, `numbers`.`n`)
utf8	utf8_general_ci
