CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `EligibleMembersByCourseVenue` AS
(select
`Courses`.`Abbreviation` AS `Abbreviation`,
`GetEligibleCount`(`Courses`.`Abbreviation`, 'GAN') AS `GAN`,
`GetEligibleCount`(`Courses`.`Abbreviation`, 'HAY') AS `HAY`,
`GetEligibleCount`(`Courses`.`Abbreviation`, 'HEN') AS `HEN`,
`GetEligibleCount`(`Courses`.`Abbreviation`, 'LUT') AS `LUT`,
`GetEligibleCount`(`Courses`.`Abbreviation`, 'NOR') AS `NOR`,
`GetEligibleCount`(`Courses`.`Abbreviation`, 'WIT') AS `WIT`
from
`Courses` 
where (`Courses`.`CoreCourseOrder` > 0) 
order by `Courses`.`CoreCourseOrder`)
utf8	utf8_general_ci
