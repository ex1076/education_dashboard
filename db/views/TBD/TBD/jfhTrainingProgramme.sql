CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jfhTrainingProgramme` AS
select
`TrainingProgramme`.`ID` AS `ID`,
`TrainingProgramme`.`Series` AS `Series`,
`TrainingProgramme`.`CourseAbb` AS `CourseAbb`,
`TrainingProgramme`.`VenueAbb` AS `VenueAbb`,
`TrainingProgramme`.`StartDate` AS `StartDate`,
`TrainingProgramme`.`InvitesDone` AS `InvitesDone`,
`TrainingProgramme`.`MaterialsDone` AS `MaterialsDone`,
`TrainingProgramme`.`ExpensesDone` AS `ExpensesDone`,
`TrainingProgramme`.`Archived` AS `Archived`,
`TrainingProgramme`.`Notes` AS `Notes`
from
`TrainingProgramme` 
where (`TrainingProgramme`.`StartDate` > '2015-10-01') 
group by
`TrainingProgramme`.`CourseAbb`, `TrainingProgramme`.`VenueAbb` 
order by
`TrainingProgramme`.`StartDate`
utf8	utf8_general_ci
