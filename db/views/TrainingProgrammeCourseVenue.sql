CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `TrainingProgrammeCourseVenue` AS
(
select
`TrainingProgramme`.`ID` AS `ID`,
`TrainingProgramme`.`CourseAbb` AS `CourseAbb`,
`TrainingProgramme`.`VenueAbb` AS `VenueAbb`,
`TrainingProgramme`.`StartDate` AS `StartDate`,
`TrainingProgramme`.`InvitesDone` AS `InvitesDone`,
`TrainingProgramme`.`MaterialsDone` AS `MaterialsDone`,
`TrainingProgramme`.`ExpensesDone` AS `ExpensesDone`,
`TrainingProgramme`.`Archived` AS `Archived`,
`TrainingProgramme`.`Notes` AS `Notes`,
concat(`TrainingProgramme`.`CourseAbb`, `TrainingProgramme`.`VenueAbb`) AS `CourseVenue`,
SeriesCounter(`ID`) AS `SeriesID`,
Year(`StartDate`) AS `Year`
from
`TrainingProgramme` 
order by ID
)
