CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `AttMemberCourse` AS
(select
`Att`.`Member` AS `Member`,
`TP`.`CourseAbb` AS `CourseAbb` 
from
  (`TrainingProgramme` `TP` join `Attendance` `Att`
  on
  (`Att`.`TrainingID` = `TP`.`ID`))
)
