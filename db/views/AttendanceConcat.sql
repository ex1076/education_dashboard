CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `AttendanceConcat` AS
(
select
`ATT`.`Member` AS `Member`,
group_concat(distinct `TP`.`CourseAbb` separator ',') AS `CourseLIst` 
from
 (`Attendance` `ATT` left join `TrainingProgramme` `TP`
 on
 (`ATT`.`TrainingID` = `TP`.`ID`))
group by `ATT`.`Member` 
order by `ATT`.`Member`	
) 
