CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `AttMemberCourseDate` AS
(
select
`ATT`.`Member` AS `Member`,
`TP`.`CourseAbb` AS `CourseAbb`,
`TP`.`StartDate` AS `StartDate` 
from
  (`Attendance` `ATT` left join `TrainingProgramme` `TP`
  on
  (`TP`.`ID` = `ATT`.`TrainingID`))
)	
