CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `member_pivot2` AS
(select
`member_extended2`.`Member` AS `Member`,
group_concat(`member_extended2`.`ELC` order by
`member_extended2`.`ELC` DESC separator ',
') AS
`ELC`,
group_concat(`member_extended2`.`EQU` order by
`member_extended2`.`EQU` DESC separator ',
') AS
`EQU`,
group_concat(`member_extended2`.`GMB` order by
`member_extended2`.`GMB` DESC separator ',
') AS
`GMB`,
group_concat(`member_extended2`.`HS1` order by
`member_extended2`.`HS1` DESC separator ',
') AS
`HS1`,
group_concat(`member_extended2`.`HS2` order by
`member_extended2`.`HS2` DESC separator ',
') AS
`HS2`,
group_concat(`member_extended2`.`OFC` order by
`member_extended2`.`OFC` DESC separator ',
') AS
`OFC`,
group_concat(`member_extended2`.`WPO1` order by
`member_extended2`.`WPO1` DESC separator '<br>') AS
`WPO1`,
group_concat(`member_extended2`.`WPO2` order by
`member_extended2`.`WPO2` DESC separator '<br>') AS `WPO2`
from
`member_extended2` group by
`member_extended2`.`Member`)
