CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `LiveInvites` AS
(
select 
`I`.`ID` AS `ID`,
`I`.`TrainingID` AS `TrainingID`,
`I`.`Member` AS `Member`,
`I`.`Date` AS `Date`,
`I`.`Medium` AS `Medium` 
/*`TP`.`Archived` AS `Archived`*/
from 
(`Invitations` `I` left join `Responses` `R` 
  on(
    (`I`.`ID` = `R`.`InviteID`)
    )
  left join `TrainingProgramme` `TP` 
  on(
    (`I`.`TrainingID` = `TP`.`ID`)
    )
) 
where
(
    (`I`.`TrainingID` <> 0) 
    /*filter out invites that have been responded to*/
    and isnull(`R`.`ID`)
    /*filter out archived courses*/
    and (`TP`.`Archived` = 0) 
)
)
