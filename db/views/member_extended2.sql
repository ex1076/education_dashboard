CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `member_extended2` AS
(
(select
`AttMemberCourseDate`.`Member` AS `Member`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'ELC') then `AttMemberCourseDate`.`StartDate` end) AS `ELC`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'EQU') then `AttMemberCourseDate`.`StartDate` end) AS `EQU`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'GMB') then `AttMemberCourseDate`.`StartDate` end) AS `GMB`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'HS1') then `AttMemberCourseDate`.`StartDate` end) AS `HS1`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'HS2') then `AttMemberCourseDate`.`StartDate` end) AS `HS2`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'OFC') then `AttMemberCourseDate`.`StartDate` end) AS `OFC`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'WPO1') then `AttMemberCourseDate`.`StartDate` end) AS `WPO1`,
(case when (`AttMemberCourseDate`.`CourseAbb` = 'WPO2') then `AttMemberCourseDate`.`StartDate` end) AS `WPO2`
from
`AttMemberCourseDate`)
)
