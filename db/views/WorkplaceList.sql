CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `WorkplaceList` AS
(select Workplace, any_value(WorkplaceName) from MembershipDetails GROUP BY Workplace);
