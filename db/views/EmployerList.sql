CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `EmployerList` AS
(select Employer, any_value(EmployerName) from MembershipDetails GROUP BY Employer);
