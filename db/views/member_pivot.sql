CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `member_pivot` AS
(select
`member_extended`.`Member` AS `Member`,
count(`member_extended`.`ELC`) AS `ELC`,
count(`member_extended`.`EQU`) AS `EQU`,
count(`member_extended`.`GMB`) AS `GMB`,
count(`member_extended`.`HS1`) AS `HS1`,
count(`member_extended`.`HS2`) AS `HS2`,
count(`member_extended`.`OFC`) AS `OFC`,
count(`member_extended`.`WPO1`) AS `WPO1`,
count(`member_extended`.`WPO2`) AS `WPO2`
from
`member_extended` 
group by
`member_extended`.`Member`)
