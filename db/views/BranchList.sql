CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `BranchList` AS
(select BranchCode, any_value(BranchName), any_value(BSMailName) from MembershipDetails GROUP BY BranchCode);
