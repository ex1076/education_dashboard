CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `member_pivot3` AS
(
select
`member_extended2`.`Member` AS `Member`,
(max(`member_extended2`.`ELC`)) AS `ELC`,
(max(`member_extended2`.`EQU`)) AS `EQU`,
(max(`member_extended2`.`GMB`)) AS `GMB`,
(max(`member_extended2`.`HS1`)) AS `HS1`,
(max(`member_extended2`.`HS2`)) AS `HS2`,
(max(`member_extended2`.`OFC`)) AS `OFC`,
(max(`member_extended2`.`WPO1`)) AS `WPO1`,
(max(`member_extended2`.`WPO2`)) AS `WPO2`
from
`member_extended2` 
group by
`member_extended2`.`Member`
)
