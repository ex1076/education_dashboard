CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `CourseAttendanceCount` AS
(select
`TP`.`ID` AS `ID`,
count(`ATT`.`ID`) AS `AttendanceCount`
from
(`Attendance` `ATT` left join `TrainingProgramme` `TP`
on
(`ATT`.`TrainingID` = `TP`.`ID`)) 
group by `TP`.`ID`
)
