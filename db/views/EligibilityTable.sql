CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `EligibilityTable` AS
(
select
`TP`.`ID` AS `ID`,
`TP`.`VenueAbb` AS `Venue`,
`TP`.`CourseAbb` AS `Course`,
concat(`TP`.`CourseAbb`, `TP`.`VenueAbb`) AS `CourseVenue`,
`EligibilityCounter`(`TP`.`CourseAbb`, `TP`.`VenueAbb`, `TP`.`ID`) AS `EligibleCount`
from
/*(`Venues` `V` join `Courses` `C`) */
(`TrainingProgramme` `TP`)
where (`TP`.`Archived` = 0) 
order by ID
/*where ((``.`CoreVenue` <> '') 
  and (`C`.`CoreCourseOrder` <> ''))*/
)
