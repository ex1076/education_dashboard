CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `PreferredVenuesConCat` AS
(
select
`PreferredVenues`.`Member` AS `Member`,
group_concat(concat('|', `PreferredVenues`.`Venue`, '|') separator ',') AS `Venue`,
`PreferredVenues`.`Archived` AS `Archived`
from
`PreferredVenues` 
where `PreferredVenues`.`Archived`=0
group by
`PreferredVenues`.`Member`,`PreferredVenues`.`Archived`
)
