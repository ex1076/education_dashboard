CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `AttendanceUInvitations` AS 
select 
`Attendance`.`Member` AS `Member`,
`Attendance`.`TrainingID` AS `TrainingID` 
from 
`Attendance` 
union 
select 
`Invitations`.`Member` AS `Member`,
`Invitations`.`TrainingID` AS `TrainingID` 
from `Invitations`
