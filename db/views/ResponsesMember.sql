CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ResponsesMember` AS
(
select 
  `R`.`ID` AS `ID`,
  `R`.`InviteID` AS `InviteID`,
  `R`.`Date` AS `Date`,
  `R`.`Medium` AS `Medium`,
  `R`.`Note` AS `Note`,
  `R`.`Response` AS `Response`,
  `INV`.`Member` AS `Member`,
  `INV`.`TrainingID` AS `TrainingID`,
  `ID_hist`,
  `Date_hist`,
  `Resp_hist`,
  `Note_hist`,
  `Medium_hist`
from
`Responses` `R`
inner join 
(
  select 
  `Responses`.`InviteID`,
  group_concat(Responses.ID SEPARATOR '|') AS `ID_hist`, 
  group_concat(Date SEPARATOR '|') AS `Date_hist`, 
  group_concat(Response SEPARATOR '|') AS `Resp_hist`, 
  group_concat(IFNULL(Note, "NULL") SEPARATOR '|') AS `Note_hist`,
  group_concat(Media.Medium SEPARATOR '|') AS `Medium_hist`,
  `shultz`.`ID` as ID_last,
  `shultz`.`max_date` AS Date_last
  from
  `Responses` 
  left join 
  (
    select 
    MAX(`Responses`.`ID`) as `ID`,
    `Responses`.`InviteID` as `InviteID`,
    `LatestResponseForInvite`.`max_date` as `max_date`,
    `AllInvites`.`Member` AS `Member`,
    `AllInvites`.`TrainingID` AS `TrainingID` 
    from
    `Responses`
    inner join
    (
      /* This handles the case where there are two identical dates,
        and returns the higher of the indices*/
      select         
      InviteID, 
      MAX(Date) as `max_date`
      from
      `Responses`         
      group by InviteID
    ) AS `LatestResponseForInvite` 
    on 
    (
      `Responses`.`InviteID`= `LatestResponseForInvite`.`InviteID`
      and
      `Responses`.`Date`= `LatestResponseForInvite`.`max_date`
    )
    left join
    (
      select Member, TrainingID,ID 
      from Invitations
    ) as AllInvites
    ON (`Responses`.`InviteID` = `AllInvites`.`ID`)
    group by `InviteID`,`max_date`,`Member`,`TrainingID`
  ) AS shultz
  ON Responses.InviteID=shultz.InviteID
  left join Media 
  on
  Media.ID=Responses.Medium
  group by `Responses`.`InviteID`
  order by shultz.max_date desc, Responses.InviteID desc
) AS ResponseHistoryPerInvite
ON R.ID=ResponseHistoryPerInvite.ID_last
left join `Invitations` `INV`
on
(`R`.`InviteID` = `INV`.`ID`)
/*
group by InviteID
order by R.Date desc, R.ID desc
*/
)


