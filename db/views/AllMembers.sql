CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `AllMembers` AS
(select *, 'Y' as 'OOR' from OutOfRegionMembershipDetails) 
union 
(select *, '0' as 'Archived', 'N' as 'OOR' from MembershipDetails)
