CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `member_extended` AS
(
(select
`AttMemberCourse`.`Member` AS `Member`,
(case when (`AttMemberCourse`.`CourseAbb` = 'ELC') then `AttMemberCourse`.`CourseAbb` end) AS `ELC`,
(case when (`AttMemberCourse`.`CourseAbb` = 'EQU') then `AttMemberCourse`.`CourseAbb` end) AS `EQU`,
(case when (`AttMemberCourse`.`CourseAbb` = 'GMB') then `AttMemberCourse`.`CourseAbb` end) AS `GMB`,
(case when (`AttMemberCourse`.`CourseAbb` = 'HS1') then `AttMemberCourse`.`CourseAbb` end) AS `HS1`,
(case when (`AttMemberCourse`.`CourseAbb` = 'HS2') then `AttMemberCourse`.`CourseAbb` end) AS `HS2`,
(case when (`AttMemberCourse`.`CourseAbb` = 'OFC') then `AttMemberCourse`.`CourseAbb` end) AS `OFC`,
(case when (`AttMemberCourse`.`CourseAbb` = 'WPO1') then `AttMemberCourse`.`CourseAbb` end) AS `WPO1`,
(case when (`AttMemberCourse`.`CourseAbb` = 'WPO2') then `AttMemberCourse`.`CourseAbb` end) AS `WPO2`
from
`AttMemberCourse`)
)
