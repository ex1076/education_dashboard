#!/bin/bash

username=`cat ../php/config.php | grep "^\\\$db_username\s\+=" | cut -d \' -f 2`
password=`cat ../php/config.php | grep "^\\\$db_password\s\+=" | cut -d \' -f 2`
database=`cat ../php/config.php | grep "^\\\$db_name\s\+=" | cut -d \' -f 2`

#Get list of files in schemaChanges directory
#loop through files
#Make sure only to import files that are in the sc.00.00.0000.sql format

for f in schemaChanges/sc\.[0-9][0-9]\.[0-9][0-9]\.[0-9][0-9][0-9][0-9]\.sql
do
  echo $f
  if [ -f $f ]; then
  filename=$(basename "$f")
  filename="${filename%.*}"
  MajorReleaseNumber="${filename%.*.*}"
  MajorReleaseNumber="${MajorReleaseNumber#sc.}"
  MinorReleaseNumber="${filename%.*}"
  MinorReleaseNumber="${MinorReleaseNumber#sc.*.}"
  PointReleaseNumber="${filename#sc.*.*.}"

  count=`mysql -N -s -u $username -p$password $database -e "select count(*) from SchemaChangeLog where ScriptName='$filename'"`
  
  if [ $count = 0 ]; then 
    echo "importing" $f
    #apply schema change
    mysql -u $username -p$password $database < $f
    #if change applied succesfully that add change to SchemaChangeLog
    if [ $? -ne 0 ]; then
      echo "Import failed"
      exit
    else
      echo "import succeeded"
      #mysql -N -s -u $username -p$password $database -e "INSERT INTO SchemaChangeLog(ScriptName) VALUES ('$filename')"
      mysql -N -s -u $username -p$password $database -e "INSERT INTO SchemaChangeLog(MajorReleaseNumber, MinorReleaseNumber, PointReleaseNumber, ScriptName ) VALUES ($MajorReleaseNumber, $MinorReleaseNumber, $PointReleaseNumber, '$filename' )"
    fi
  else
    echo "Already imported" $filename
  fi

  fi
done

echo "ALL DONE!"


