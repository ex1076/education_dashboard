#!/bin/bash

username=`cat ../php/config.php | grep "^\\\$db_username\s\+=" | cut -d \' -f 2`
password=`cat ../php/config.php | grep "^\\\$db_password\s\+=" | cut -d \' -f 2`
database=`cat ../php/config.php | grep "^\\\$db_name\s\+=" | cut -d \' -f 2`

mysql -u $username -p$password -e "SET @views = NULL;SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @views FROM information_schema.views WHERE table_schema = '$database'; SET @views = CONCAT('DROP VIEW ', @views); PREPARE stmt FROM @views; EXECUTE stmt; DEALLOCATE PREPARE stmt; DELETE from mysql.proc where db='$database';"

for d in functions views procedures triggers
do
  for f in $d/*.sql
  do

    echo "importing: "$f
    if [ -f "$f" ]; then
    mysql -u $username -p$password $database < $f
    fi
  done
done

