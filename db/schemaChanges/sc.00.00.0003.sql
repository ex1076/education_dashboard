ALTER TABLE TrainingProgramme MODIFY COLUMN Series varchar(20);
ALTER TABLE TrainingProgramme MODIFY COLUMN Notes text;
ALTER TABLE TrainingProgramme MODIFY COLUMN Archived tinyint(1);
ALTER TABLE TrainingProgramme ADD UNIQUE (Series);
