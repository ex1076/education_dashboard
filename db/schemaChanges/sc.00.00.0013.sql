CREATE TABLE IF NOT EXISTS `ResponseType` (
  `ID` varchar(1) NOT NULL DEFAULT '',
  `ResponseType` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=3141 DEFAULT CHARSET=latin1;

INSERT INTO ResponseType VALUES 
(0,"No"),
(1,"Yes"),
(2,"Full")
;

UPDATE `Responses` SET `Response` = 1 WHERE `Response` = '';
ALTER TABLE `Responses` MODIFY COLUMN `Response` tinyint(1);
