alter table Users add column Level tinyint not null default 100 after USER;
create table Roles(Level TinyInt not null primary key, Role varchar(12) not null);
insert into  Roles Values (1,'Dev'),(2,'SuperUser'),(3,'Admin'),(4,'User');
