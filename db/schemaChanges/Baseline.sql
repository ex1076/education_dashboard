CREATE TABLE IF NOT EXISTS `Attendance` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Member` varchar(7) NOT NULL,
  `TrainingID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=1880 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Courses` (
  `Abbreviation` varchar(4) NOT NULL,
  `Title` tinytext NOT NULL,
  `Prerequisite` varchar(4) NOT NULL,
  `Duration` int(11) DEFAULT NULL,
  `CoreCourseOrder` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`Abbreviation`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Invitations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrainingID` int(11) NOT NULL,
  `Member` varchar(7) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Medium` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `MemTraining` (`Member`,`TrainingID`)
)
ENGINE=InnoDB AUTO_INCREMENT=3141 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Media` (
  `ID` varchar(1) NOT NULL DEFAULT '',
  `Medium` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `MembershipDataImports` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DateOfImport` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NumberOfPostholders` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `MembershipDetails` (
  `Member` varchar(7) NOT NULL,
  `Title` tinytext NOT NULL,
  `FirstName` tinytext NOT NULL,
  `Surname` tinytext NOT NULL,
  `Add1` tinytext NOT NULL,
  `Add2` tinytext,
  `Add3` tinytext,
  `Add4` tinytext,
  `PostCode` varchar(9) DEFAULT NULL,
  `InvalidHomeAddress` varchar(1) DEFAULT NULL,
  `TelNoHome` varchar(15) DEFAULT NULL,
  `TelNoWork` varchar(15) DEFAULT NULL,
  `TelNoMobile` varchar(15) DEFAULT NULL,
  `EmailAddress1` tinytext,
  `OfficerName` tinytext,
  `BranchName` tinytext,
  `BranchCode` tinytext,
  `BSMailName` tinytext,
  `EmployerName` tinytext,
  `Employer` tinytext,
  `OccupationName` tinytext,
  `Department` tinytext,
  `WorkplaceName` tinytext,
  `Workplace` tinytext,
  `Section` varchar(2) DEFAULT NULL,
  `Activist` tinytext,
  `ApparentLeaverDate` date DEFAULT NULL,
  `SuspendedMandate` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`Member`),
  UNIQUE KEY `Member` (`Member`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `PreferredVenues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Member` varchar(7) NOT NULL,
  `Venue` varchar(4) NOT NULL,
  `Archived` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
  /*
  UNIQUE KEY `Member` (`Member`,`Venue`),
  UNIQUE KEY `Member_2` (`Member`,`Venue`)
  --UNIQUE KEY `MemberVenue` (`Member`,`Venue`)
*/
)
ENGINE=InnoDB AUTO_INCREMENT=1524 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Responses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `InviteID` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Medium` varchar(1) NOT NULL,
  `Note` mediumtext,
  `Response` tinytext NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=1951 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `SchemaChangeLog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MajorReleaseNumber` varchar(2) NOT NULL,
  `MinorReleaseNumber` varchar(2) NOT NULL,
  `PointReleaseNumber` varchar(4) NOT NULL,
  `ScriptName` varchar(50) NOT NULL,
  `DateApplied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `TrainingProgramme` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Series` varchar(4) NOT NULL,
  `CourseAbb` varchar(4) NOT NULL,
  `VenueAbb` varchar(3) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `InvitesDone` tinyint(1) NOT NULL,
  `MaterialsDone` tinyint(1) NOT NULL,
  `ExpensesDone` tinyint(1) NOT NULL,
  `Archived` int(11) NOT NULL,
  `Notes` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CourseVenue` (`CourseAbb`,`VenueAbb`)
)
ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailAddress` text NOT NULL,
  `FirstName` text NOT NULL,
  `Surname` text NOT NULL,
  `Password` text NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Venues` (
  `Abbreviation` varchar(4) NOT NULL,
  `Name` tinytext NOT NULL,
  `Add1` tinytext,
  `Add2` tinytext,
  `Add3` tinytext,
  `Add4` tinytext,
  `Add5` tinytext,
  `Post Code` varchar(9) DEFAULT NULL,
  `Phone Number` varchar(16) DEFAULT NULL,
  `CoreVenue` tinyint(1) NOT NULL,
  PRIMARY KEY (`Abbreviation`),
  UNIQUE KEY `Abbreviation` (`Abbreviation`),
  KEY `Abbreviation_2` (`Abbreviation`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Audit_Logins` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Username` varchar(100) DEFAULT NULL,
  `Hostname` varchar(100) DEFAULT NULL,
  `Status`   varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `Audit_Trail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `date_chng` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username`   varchar(100) DEFAULT NULL,
  `status`   varchar(100) DEFAULT NULL,
  `table`     varchar(100) DEFAULT NULL,
  `row_id`     varchar(100) DEFAULT NULL,
  `values`   varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `SchemaChangeLog` (
  `MajorReleaseNumber` ,
  `MinorReleaseNumber` ,
  `PointReleaseNumber` ,
  `ScriptName` )
  Values
  (0,0,0,"Initial Install")

