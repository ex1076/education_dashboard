<?php 
  require('php/config.php');

  session_start();
  $secondsWait=1200;
  $secondsWaitBuffer=300;
  $milisecondsWait=($secondsWait * 1000);
  $milisecondsWaitBuffer=($secondsWaitBuffer * 1000);

  $session_value=(isset($_SESSION['user']))?$_SESSION['user']:'';
  if (isset($_SESSION['LAST_ACTIVITY']) ) {
    if (time() - $_SESSION['LAST_ACTIVITY'] > ($secondsWait + $secondsWaitBuffer) + 10) {
      // last request was more than 30 minutes ago
      session_unset();     // unset $_SESSION variable for the run-time 
      session_destroy();   // destroy session data in storage
    }
  }
  $_SESSION['LAST_ACTIVITY'] = time(); 

  $mysqli = @new mysqli($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "<br>";
    exit;
  }

  if (!($res = $mysqli->query(
    //"UPDATE Users SET LastSeen='FROM_UNIXTIME(" . time() . ")' WHERE user='" . $sessionvalue . "'"
    "UPDATE Users SET LastSeen=FROM_UNIXTIME(" . time() . ") WHERE user='" . $session_value . "'"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br>";
    exit;
  };
?>


<!DOCTYPE html>
<html>
  <head>
    <?php include_once "head.html"; ?>
    <style type="text/css">
      body {
        background-color: #FFFFFF;
      }
      .ui.menu .item img.logo {
        margin-right: 1.5em;
      }
      .main.container {
        margin-top: 2em;
      }
      .wireframe {
        margin-top: 2em;
      }
      .ui.footer.segment {
      }

html, body {
    height: 100%;
}
#content {
    min-height: 100%;
    margin-bottom: -5.5em;
}
#content:after {
  content: "";
  display: block;
}
.site-footer, #content:after {
    height: 5.5em;
}

    </style>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    //user login sessions
    var timer = 0;
    function set_interval() {
      // the interval 'timer' is set as soon as the page loads
      timer = setInterval("auto_logout()", <?php echo $milisecondsWait?>);
    };
    function reset_interval() {
      //resets the timer. The timer is reset on each of the below events:
      // 1. mousemove   2. mouseclick   3. key press 4. scroliing
      //first step: clear the existing timer
      if (timer != 0) {
       clearInterval(timer);
       timer = 0;
       // second step: implement the timer again
       timer = setInterval("auto_logout()", <?php echo $milisecondsWait?>);
       // completed the reset of the timer
      };
    };
    function auto_logout() {
      var session = '<?php echo $session_value?>';
      if (session == false) {
      } else {
        $('.ui.mini.modal') 
          .modal({ 
            closable  : false ,
            onApprove : function() {
              reset_interval();
            }
          })
          .modal('show');
        if (timer != 0) {
         clearInterval(timer);
         timer = 0;
         // second step: implement the timer again
         //timer = setInterval("auto_logout()", <?php echo $milisecondsWait?>);
         timer = setInterval('window.location = "?page=logoutsuccess"', <?php echo $milisecondsWaitBuffer?>);
         //timer = setInterval("auto_logout()", 2000);
         // completed the reset of the timer
        };
        
      }
    };
  </script>

    <?php 
      if (isset($_GET['page'])) { $page = $_GET['page']; }
      if ($page != '') {
        $include = $page . ".php"; 
      }else{
        $include = "welcome.php";
      };
    ?>
    <?php include_once "navbar.php"; ?>
  <body onload="set_interval()" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()" style="height:100%;margin:0 auto;" >
    <div id='content' class="ui container" >
      <?php //include_once "content.html"; ?>
      <?php include $include; ?>
    </div>
  </body>
    <?php include_once "footer.php"; ?>
    <div class="ui mini modal">
  <div class="content">
      You've been inactive for quite a while.<br>
      To continue working press continue.
  </div>
      <div class="actions" id="continueWork">
        <div class="ui positive center labeled icon button">
          Continue
        </div>
      </div>
    </div>
</html>
