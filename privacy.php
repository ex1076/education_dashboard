<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Landing Page</title>
  </head>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">
        Privacy Policy 
      </h1>
      <div class="ui container">
        <div class="ui left aligned segment">
          <?php 
            $content = file_get_contents("http://www.gmb.org.uk/legal/privacy-policy") ;
            $first_step = explode( '<div id="content">' , $content );
            $second_step = explode("<div id=\"content-shares\">" , $first_step[1] );
            echo $second_step[0] ;
          ?>
        </div>
      </div>
    </div>
  </body>
</html>
