<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Update Database</title>
  </head>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">Update Database</h1>
      <div class="ui container">
        <div class="ui basic segment">
          <?php 
  session_start();
            require('php/config.php');
            
    $db_username    = str_ireplace("@gmb.org.uk","",$_SESSION['user']);
    $db_password    = $_SESSION['password'];
    $db_name        = 'education';
    $db_host        = 'localhost';
            $mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);
            
            echo "<p>";
            if ($mysqli->connect_errno) {
              echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            }else{
              echo "Connected to server " . $db_host . " and database " . $db_name;
            }
            echo "</p>";
            
            if (!($res = $mysqli->query(
              " SELECT * from SchemaChangeLog ORDER By ID DESC"
              ))) {
                echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
              }else{
                echo "Schema Update History:";
            }
            echo "<br>";
            
            $dir = 'schemachange';
            $files = scandir($dir);
            
            //This will output a list of changes that have already been installed
            while ($row = mysqli_fetch_array($res)){
              $key = array_search($row['ScriptName'],$files);
              if($key){ unset($files[$key]); }
              echo $row['ScriptName'] . " on ";
              echo $row['DateApplied'] . "<br>";
            };
            array_splice ($files,0,2);
                
            //This will output a list of changes that have already been installed
            //if $files is empty then
            echo "<br>Database up-to-date<br>";
            //else
            echo "<br>The following updates are available:<br>";
            foreach ( $files as $current){
            echo $current . "<br>";
            };
            echo "button to run script that will update;"
               //script 
                //loop through files
                //run sql
                //if error then exit and return error message
          ?>
        </div>
      </div>
    </div>
  </body>

</html>
