<!DOCTYPE html>
<?php
  //GET LIST OF CORE VENUES
  session_start();
  require('php/config.php');
  $db_username    = str_ireplace("@gmb.org.uk","",$_SESSION['user']);
  $db_password    = $_SESSION['password'];
  $db_name        = 'education';
  $db_host        = 'localhost';
  $mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  }

  if (!($res = $mysqli->query(
    //"select Abbreviation AS Course, Member from Courses left join (SELECT * FROM AttMemberCourse WHERE Member='" . $member . "' ) ATT on Courses.Abbreviation=ATT.CourseAbb where CoreCourseOrder is not null Group by Abbreviation order by CoreCourseOrder"
    "select Title AS Course, Member from Courses left join (SELECT * FROM AttMemberCourse WHERE Member='" . $member . "' ) ATT on Courses.Abbreviation=ATT.CourseAbb where CoreCourse=1  Group by Abbreviation order by CoreCourseOrder"
    //"SELECT GROUP_CONCAT(DISTINCT CourseAbb ORDER BY CoreCourseOrder)  
    //FROM AttMemberCourse  
    //LEFT JOIN Courses on AttMemberCourse.CourseAbb=Courses.Abbreviation  
    //WHERE Member='" .  $member  . "'"

    //" SELECT Name FROM Venues WHERE CoreVenue=1"
    //" select Name AS Venue, id from Venues V left join (SELECT * FROM PreferredVenues where Member='" . $member . "') PV on PV.Venue=V.Abbreviation where CoreVenue=1"
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

  //LOOKUP VENUES AND TICK ACCORDINGLY
  echo '<form name="completedCourses" >';
    echo '<div class="ui relaxed list">';
      while ($row = mysqli_fetch_array($res)){ //foreach ($row as $current){
        //echo $current;
        //print_r($row);
        echo ' <div class="item">';
          echo '<div class="ui course checkbox">';
            echo '<label>' . $row['Course'] . '</label>';
              if( empty($row['Member'])) {
                echo '<input type="checkbox" disabled>';
              }else{
                echo '<input type="checkbox" checked disabled>';
              }
          echo '</div>';
        echo '</div>';
      //}};
      };
    echo '</div>';
  echo '</form>';
?>

<script type="text/javascript" language="javascript" class="init">
  $(document).ready(function() {

    this.PreferredVenues.reset();

    $(".ui.checkbox").checkbox() ;

  });
</script>

