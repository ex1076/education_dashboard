<?php
  $courseID=$_GET['courseID'];

  session_start();
  require('php/config.php');
  $db_username    = str_ireplace("@gmb.org.uk","",$_SESSION['user']);
  $db_password    = $_SESSION['password'];
  $db_name        = 'education';
  $db_host        = 'localhost';
  $mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  }

  if (!($res = $mysqli->query(
    " UPDATE TrainingProgramme SET Archived=1 where ID=" . $courseID
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

  echo json_encode (new stdClass);
?>
