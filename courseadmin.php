<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Repslist</title>

    <?php 
      session_start();
      if(empty($_SESSION['user'])) {
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/login.php');
        print '
          <div class="ui main text center aligned container">
          <div class="ui center aligned container">
          This page requires a login
        ';
        include "login.php";
        print "</div></div>";
        exit;
      }
    ?>
  </head>

  <script type="text/javascript" language="javascript" class="init">
    var editoro; // use a global for the submit and return data rendering in the examples
    var editorn; // use a global for the submit and return data rendering in the examples
                
    function filterColumn (i) {
      //$('#example').DataTable().column(i).search(
      //  $('#col'+i+'_filter').val(),true,true
      //).draw(); 
    };    
                
    function empty(data) {
      if(typeof(data) == 'number' || typeof(data) == 'boolean')
      { 
        return false; 
      }
      if(typeof(data) == 'undefined' || data === null)
      {
        return true; 
      }
      if(typeof(data.length) != 'undefined')
      {
        return data.length == 0;
      }
      var count = 0;
      for(var i in data) {
        if(data.hasOwnProperty(i))
        {
          count ++;
        }
      }
      return count == 0;
    }

    $(document).ready(function() {
      $('.column_filter').on('keyup click', function() { filterColumn( $(this).parents('tr').attr('data-column')); } );

	$('.tabular.menu .item').tab();

      $('#BigFucker') .popup({
        on       : 'click',
        inline   : true,
        closable   : false,
        //preserve : true,
        //hoverable: true,
        position : 'bottom left',
        delay: { show: 300, hide: 800 }
      }) ;
      
      // -#This selects which elements are editable-->
      editoro = new $.fn.dataTable.Editor( {
        ajax: "php/courseadmin1.php",
        table: "#tempo",
        fields: [ {
          label: "Abbreviation:",
          fieldInfo: "Mandatory Field",
          name: "Abbreviation"
        }, {
          label: "Title:",
          fieldInfo: "Mandatory Field",
          name: "Title"
        }, {
          label: "Prerequisite:",
          name: "Prerequisite",
          type: "select",
          options: loader("php/courses.php")
        }, {
          label: "Course Duration (Days):", 
          name: "Duration",
          def: "5"
        }, {
          label: "Core Course:",
          name: "CoreCourse",
	    type: "checkbox",
          separator: "|",
          options:   [
            { label: '', value: 1 }
          ],
          unselectedValue: 0,
        }, {
          label: "CoreCourseOrder:",
          name: "CoreCourseOrder",
 	    def: "99"
        } ]
      } );
	editoro
        .on( 'postCreate postRemove', function () {
            // After create or edit, a number of other rows might have been effected -
            // so we need to reload the table, keeping the paging in the current position
            tableo.ajax.reload( null, false );
            tablen.ajax.reload( null, false );
        } )
        .on( 'initCreate', function () {
            // Enable order for create
            editoro.field( 'CoreCourseOrder' ).enable();
        } )
        .on( 'initEdit', function () {
            // Disable for edit (re-ordering is performed by click and drag)
            editoro.field( 'CoreCourseOrder' ).disable();
        } );

      editorn = new $.fn.dataTable.Editor( {
        ajax: "php/courseadmin2.php",
        table: "#tempn",
        fields: [ {
          label: "Abbreviation:",
          fieldInfo: "Mandatory Field",
          name: "Abbreviation"
        }, {
          label: "Title:",
          fieldInfo: "Mandatory Field",
          name: "Title"
        }, {
          label: "Prerequisite:",
          name: "Prerequisite",
          type: "select",
          options: loader("php/courses.php")
        }, {
          label: "Course Duration (Days):", 
          name: "Duration",
          def: "5"
        }, {
          label: "Core Course:",
          name: "CoreCourse",
	    type: "checkbox",
          separator: "|",
          options:   [
            { label: '', value: 1 }
          ],
          unselectedValue: 0,
        }, {
          label: "CoreCourseOrder:",
          name: "CoreCourseOrder",
          def: "99"
        } ]
      } );
	editorn
        .on( 'postCreate postRemove', function () {
            // After create or edit, a number of other rows might have been effected -
            // so we need to reload the table, keeping the paging in the current position
            tableo.ajax.reload( null, false );
            tablen.ajax.reload( null, false );
        } )
        .on( 'initCreate', function () {
            // Enable order for create
            editorn.field( 'CoreCourseOrder' ).enable();
        } )
        .on( 'initEdit', function () {
            // Disable for edit (re-ordering is performed by click and drag)
            editorn.field( 'CoreCourseOrder' ).disable();
        } );

      function loader(phpFile){
        var test= new Array({"label" : "a", "value" : "a"});

        test.splice(0,1);
        console.log(test);
        $.ajax({
          url: phpFile,
          async: false,
          dataType: 'json',
          success: function (json) {
            console.log("json");
            console.log(json);
            for(var a=0;a<json.length;a++){
              //if (phpFile='php/responseRecordAdd.php' ) alert(json[a]);
              //alert(json[a]['label']);
              //alert(json[a]['label']);
              if (json[a]['label'] !== undefined){
                console.log("successtest1");
                obj= { "label" : json[a]['label'], "value" : json[a]['value']};
              }else{
                console.log("successtest2");
                obj= { "label" : json[a][1], "value" : json[a][0]};
              }
              test.push(obj);
            }
          }
        });
        console.log("test");
        console.log(test);
        return test;
      }

      function myFunction () {
        var cars = [
          { label: "1 (sogo)", value: "1" },
          { label: "2",           value: "2" },
          { label: "3",           value: "3" },
          { label: "4",           value: "4" },
          { label: "5 (lowest)",  value: "5" }
        ];
        return cars;
      };
        
      // Activate an inline edit on click of a table cell
      //$('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
	//      editor.inline( this );
      //} );
        
      var tableo = $('#tempo').DataTable( {
        dom: "Bfrtip",
        "fnDrawCallback": function(){
          $('.VD_popup') .popup({
            position : 'top right',
            title    : 'Invited:',
            delay: { show: 900, hide: 0 }
          });
        },
        // dom:'B<ti>p<T>S',
        ajax: "php/courseadmin1.php",
        select: "single",
	  order: [[4, 'asc']],
        columns: [ { 
          "_": "abbreviation",
          data: "Abbreviation",
          visible: true,
          width: '25%', //jfh fix styling in
          //render: function (data,type,row){
          //  return "<div>" + data.Abbreviation + "</div";
          //} 
        } , { 
          "_": "title",
          data:  "Title", 
          visible: true,
          //render: function (data,type,row){
           // return "<div>" + data.Title + "</div";
         // }
        } , { 
          "_": "prerequisite",
          data:  "Prerequisite", 
          visible: true,
          defaultContent: "<i>None</i>"
        } , { 
          "_": "duration",
          data:  "Duration", 
          visible: true,
          //render: function (data,type,row){
          //  return "<div>" + data.Duration + "</div";
          //}
        } , { 
          "_": "corecourseorder",
          data:  "CoreCourseOrder", 
	    className: 'reorder',
          visible: true
        } ],
        columnDefs: [ {
          targets: "abbreviation", className: "ui basic center align",
          searchable: false,
          width: "7%",
	    visible: true, 
	  } , { 
	    //targets: [ 0,1,2,3] ,
          targets: "_all",
	    orderable: false
	  } , { 
	    targets: "corecourseorder",
	    orderable: true
        } , {
          targets: "_all",
          visible: true
        } ],
        rowReorder: {
            dataSrc: 'CoreCourseOrder',
		select: 'tr',
            editor:  editoro
        },
           select: 'single',
        buttons: [
          { extend: "create", editor: editoro },
          { extend: "edit",   editor: editoro },
          { extend: "remove", editor: editoro }
        ]
      } );
      var tablen = $('#tempn').DataTable( {
        dom: "Bfrtip",
        "fnDrawCallback": function(){
          $('.VD_popup') .popup({
            position : 'top right',
            title    : 'Invited:',
            delay: { show: 900, hide: 0 }
          });
        },
        // dom:'B<ti>p<T>S',
        ajax: "php/courseadmin2.php",
        select: "single",
	  order: [[4, 'asc']],
        columns: [ { 
          "_": "abbreviation",
          data: "Abbreviation",
          visible: true,
          width: '25%', //jfh fix styling in
          //render: function (data,type,row){
          //  return "<div>" + data.Abbreviation + "</div";
          //} 
        } , { 
          "_": "title",
          data:  "Title", 
          visible: true,
          //render: function (data,type,row){
           // return "<div>" + data.Title + "</div";
         // }
        } , { 
          "_": "prerequisite",
          data:  "Prerequisite", 
          visible: true,
          defaultContent: "<i>None</i>"
        } , { 
          "_": "duration",
          data:  "Duration", 
          visible: true,
          //render: function (data,type,row){
          //  return "<div>" + data.Duration + "</div";
          //}
        } , { 
          "_": "corecourseorder",
          data:  "CoreCourseOrder", 
	    className: 'reorder',
          visible: true
        } ],
        columnDefs: [ {
          targets: "abbreviation", className: "ui basic center align",
          searchable: false,
          width: "7%",
	    visible: true, 
	  } , { 
	    //targets: [ 0,1,2,3] ,
          targets: "_all",
	    orderable: false
	  } , { 
	    targets: "corecourseorder",
	    orderable: true
        } , {
          targets: "_all",
          visible: true
        } ],
        rowReorder: {
            dataSrc: 'CoreCourseOrder',
		select: 'tr',
            editor:  editorn
        },
           select: 'single',
        buttons: [
          { extend: "create", editor: editorn },
          { extend: "edit",   editor: editorn },
          { extend: "remove", editor: editorn }
        ]
      } );
    } );
  </script>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">Course Administration</h1>
      <div class="ui container">
        <div class="ui basic segment">
	    <div class="ui top attached tabular menu">
		<a class="active item" data-tab="corecourses">Core</a>
		<a class="item" data-tab="bespokecourses">Bespoke</a>
	    </div>
	    <div class="ui bottom attached active tab basic segment" data-tab="corecourses">
		<table id="tempo" class="ui small sortable very compact single line table" >
		  <thead>
		    <tr>
			<!--<th></th>-->
			<th>Abbreviation</th>
			<th>Title</th>
			<th>Prerequisite</th>
			<th>Duration</th>
			<th>CoreCourseOrder</th>
		    </tr>
		  </thead>
		</table>
          </div>
	    <div class="ui bottom attached tab basic segment" data-tab="bespokecourses">
		<table id="tempn" class="ui small sortable very compact single line table" >
		  <thead>
		    <tr>
			<!--<th></th>-->
			<th>Abbreviation</th>
			<th>Title</th>
			<th>Prerequisite</th>
			<th>Duration</th>
			<th>CoreCourseOrder</th>
		    </tr>
		  </thead>
		</table>
          </div>
          <div  class="ui right very close rail">
            <div class="ui basic left aligned segment">
              <div data-hmtl="test" id="search" class="ui icon">
                <i id="BigFucker" class="ui search circular icon link"></i>
                <div class="ui custom popup top left transition hidden">
                  <table class="ui table">
                    <tbody>
                      <tr id="filter_member" data-column="1"> 
                        <td><input type="text" placeholder="Member" class="column_filter" id="col1_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_officer" data-column="2"> 
                        <td><input type="text" placeholder="Officer" class="column_filter" id="col2_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_contact" data-column="3"> 
                        <td><input type="text" placeholder="Contact Details" class="column_filter" id="col3_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_employer" data-column="4"> 
                        <td><input type="text" placeholder="Employer" class="column_filter" id="col4_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_workplace" data-column="5"> 
                        <td><input type="text" placeholder="Workplace" class="column_filter" id="col5_filter" autocomplete="off"> 
                        </td>
                      </tr>
                      <tr id="filter_branch" data-column="6"> 
                        <td><input type="text" placeholder="Branch" class="column_filter" id="col6_filter" autocomplete="off"> 
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>

</html>
