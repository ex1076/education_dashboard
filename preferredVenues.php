<?php
  //session_start();
  $member=$_GET['member'];
  //$member="A204623";
  //include('head.html');
  //GET LIST OF CORE VENUES
  require('php/config.php');
    $db_username    = str_ireplace("@gmb.org.uk","",$_SESSION['user']);
    $db_password    = $_SESSION['password'];
    $db_name        = 'education';
    $db_host        = 'localhost';
  $mysqli = mysqli_connect($db_host, $db_username, $db_password, $db_name);

  if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  }

  $query="select Name AS Venue, Abbreviation, id from Venues V left join (SELECT * FROM PreferredVenues where Member='" . $member . "' AND Archived=0) PV on PV.Venue=V.Abbreviation where CoreVenue=1 GROUP BY Abbreviation ORDER BY Abbreviation";
  if (!($res = $mysqli->query($query
  ))) {
    echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }

  //LOOKUP VENUES AND TICK ACCORDINGLY
  echo '<form name="PreferredVenues" >';

  echo '<div class="ui relaxed list">';
  while ($row = mysqli_fetch_array($res)){ //foreach ($row as $current){
    echo ' <div class="ui disabled item">';
    echo '   <div class="ui venue checkbox">';
    echo '     <label>' . $row['Venue'] . '</label>';
    $checkb =  '     <input type="checkbox" value="'. $row['Abbreviation'] .'"';
    if( !empty($row['id'])) { $checkb =  $checkb . ' checked '; }
    //$checkb =  $checkb . ' disabled ';
    $checkb =  $checkb . '>';
    echo $checkb;
    echo '   </div>';
    echo ' </div>';
  //}};
  };
  echo '</div>';
  echo '</form>';
?>

<script type="text/javascript" language="javascript" class="init">
  $(document).ready(function() {

    this.PreferredVenues.reset();

    $(".ui.venue.checkbox").checkbox() ;
    $(".ui.venue.checkbox").click(function() { 
      var checkbox = $(this).find(":checkbox");   // keep reference of checkbox
      checked = checkbox.is(":checked");      // check checkbox status

      if (!checked) {
        $.ajax({
          url:'addPreferredVenue.php',
          data:{member:"<?=$member?>",venue:checkbox.val()}
        });
      } else {
        $.ajax({
          url:'removePreferredVenue.php',
            data:{member:"<?=$member?>",venue:checkbox.val()}
        });
      };

    });

  });
</script>
