<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Education Dashboard - Landing Page</title>
  </head>

  <body>
    <div class="ui main text center aligned container">
      <h1 class="ui header">
        Education Dashboard
      <div class="sub header">London Region</div>
      </h1>
      <div class="ui container">
        <div class="ui basic segment">
          <p>  
            The Education Dashboard provides staff with up-to-date information on members' postholder status, training history, and upcoming courses including information on invitation and response lists.  
          </p>
          <p>
            This dashboard is currently in development so if you have any difficulities in using the site or have feedback you would like to offerthen please <a href="mailto:john.healy@gmb.org.uk">email the developer</a>.
          </p>  
        </div>
        <a href="/education/?page=repslist">
          <div class="ui huge center labelled icon button">
            <p>Training<br>Records</p>
            <i class="student large icon"></i>
          </div>
        </a>
        <a href="/education/?page=courses">
          <div class="ui huge center labelled icon button" data-tooltip"TEST">
            <p>Course<br>Schedule</p>
            <i class="calendar large icon"></i>
          </div>
        </a> 
      </div>
    </div>
  </body>
</html>
