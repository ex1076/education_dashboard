<style>
.ui.menu {
	margin: 0rem 0;
	border-radius: 0rem;
}
</style>
<script type="text/javascript" language="javascript" class="init">
  $(document).ready(function() {
    $('.simple.item').not('.simple.dropdown.item') .popup({
      on       : 'hover',
      inline   : true,
      closable   : false,
      position : 'bottom center',
      delay: { show: 800, hide: 800 }
    });
    $('#profile') .popup({
      on       : 'click',
      inline   : true,
      closable   : false,
      position : 'bottom center',
      delay: { show: 800, hide: 800 }
    });
  }) ;
</script>

<div class="ui inverted menu">
  <div class="ui container">
    <div href="#" class="ui  item">
      <img class="logo" src="/education/media/images/logo.png">
      Education Dashboard
    </div>
    <a id="home" href="/education/" class="ui simple item">
      <i class="home fitted icon"></i>
    </a>
    <div class="ui custom popup transition hidden">Home</div>
    <a id="student" href="/education/?page=repslist" class="ui simple item">
      <i class="student icon"></i>
    </a>
    <div class="ui custom popup transition hidden">Training Records</div>
    <a id "calendar" href="/education/?page=courses" class="ui simple item">
      <i class="calendar icon"></i>
    </a>
    <div class="ui custom popup transition hidden">Course Schedule</div>
    <div href="#" class="ui right inverted menu">
      <?php 
        if  (!empty($_SESSION['user']) && $_SESSION['level']<=3){
          print '
            <a href="#" class="ui simple dropdown item">
              <i class="setting icon"></i>
              <div class="menu">
                <div class="item" onclick="window.location=\'/education/?page=courseadmin\';">
                  Courses <i class="calender icon"></i>
                </div>
                <div class="item" onclick="window.location=\'/education/?page=venueadmin\';">Venues</div>
                <div class="item" onclick="window.location=\'/education/?page=users\';">Users</div>
              </div>
            </a>
           ';
        };
      ?>
      <a href="#" class="ui right simple dropdown item">
        SuperUser <i class="dropdown icon"></i>
        <div class="menu">
          <div class="item" onclick="window.location='/education/?page=profile';">Profile</div>
          <div class="item" onclick="window.location='/education/?page=newuser';">Add User</div>
          <div class="item" onclick="window.location='/education/?page=activity';">User Logins</div>
          <div class="item" onclick="window.location='/education/?page=trail';">User Activity</div>
        </div>
      </a>
      <div id="profile" class="header right item link">
        <i class="user icon"></i>
          <?php 
            session_start(); 
            if(!isset($_SESSION['user']) ){ 
              print "Login"; 
            } else {
              print $_SESSION['firstname'] . " "  . $_SESSION['surname'];
            }
          ?>
      </div> 
      <div class="ui custom popup top right transition hidden">
        <?php 
          if(!isset($_SESSION['user']) ){ 
            include "login.php"; 
            $page = $_SERVER["REQUEST_URI"]; 
            $_SESSION['page'] = $page; 
          } else { 
            include "logout.php"; 
          }
        ?>
      </div> 
    </div>
  </div> 
</div>

